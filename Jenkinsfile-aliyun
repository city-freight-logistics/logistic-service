pipeline {
  agent {
    node {
      label 'maven'
    }
  }

  parameters {
    string(name:'PROJECT_VERSION',defaultValue: 'v0.0Beta',description:'')
    choice(name:'PROJECT_LOCATION',
    choices: [
        'logistics-gateway/gateway-provide',
        'logistics-admin',
        'logistics-base-message/base-message-provider',
        'logistics-base-taskplatform/base-taskplatform-provider',
        'logistics-driver-order/driver-order-provide',
        'logistics-driver-publish/driver-publish-provide',
        'logistics-driver-transport/driver-transport-provide',
        'logistics-driver-vehicle/driver-vehicle-provide',
        'logistics-grabbing-dispatching/grabbing-dispatching-provide',
        'logistics-order/order-provide',
        'logistics-owner-find/owner-find-provide',
        'logistics-owner-manage/owner-manage-provide',
        'logistics-owner-publish/owner-publish-provide',
        'logistics-payment/payment-provide',
        'logistics-third-party/third-party-provide',
        'logistics-user/user-provide'
        ],description:'')
    choice(name:'PROJECT_NAME',
    choices: [
        'logistics-gateway',
        'logistics-admin',
        'logistics-base-message',
        'logistics-base-taskplatform',
        'logistics-driver-order',
        'logistics-driver-publish',
        'logistics-driver-transport',
        'logistics-driver-vehicle',
        'logistics-grabbing-dispatching',
        'logistics-order',
        'logistics-owner-find',
        'logistics-owner-manage',
        'logistics-owner-publish',
        'logistics-payment',
        'logistics-third-party',
        'logistics-user'
        ],description:'你需要选择哪个模块进行构建 ?')
  }

  environment {
    LOCATION = ''
    DOCKER_CREDENTIAL_ID = 'docker-aliyun-id'
    GITEE_CREDENTIAL_ID = 'gitee'
    KUBECONFIG_CREDENTIAL_ID = 'kubeconfig'
    REGISTRY = 'registry.cn-beijing.aliyuncs.com'
    DOCKERHUB_NAMESPACE = 'gulimall-demo'
    GITEE_ACCOUNT = 'wuye111'
    APP_NAME = 'logistic-service'
    BRANCH_NAME = 'master'
  }

  stages {
    stage('拉取代码') {
      steps {
        git(credentialsId: 'git', branch: 'master', url: 'https://gitee.com/city-freight-logistics/logistic-service.git', changelog: true, poll: false)
      }
    }

    stage('代码编译') {
      steps {
        container('maven') {
          sh 'mvn clean install -Dmaven.test.skip=true -gs `pwd`/mvn-settings.xml'
        }
      }
    }

    stage ('构建镜像-推送镜像') {
      steps {
        container ('maven') {
          sh 'cd $PROJECT_LOCATION && docker build --no-cache -f Dockerfile -t $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER .'
          withCredentials([usernamePassword(passwordVariable : 'DOCKER_PASSWORD' ,usernameVariable : 'DOCKER_USERNAME' ,credentialsId : "$DOCKER_CREDENTIAL_ID" ,)]) {
              sh 'echo "$DOCKER_PASSWORD" | docker login $REGISTRY -u "$DOCKER_USERNAME" --password-stdin'
              sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER'
          }
        }
      }
    }

    stage('推送最新镜像'){
      when{
        branch 'master'
      }
      steps{
        container ('maven') {
          sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT_NAME:latest '
          sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT_NAME:latest '
        }
      }
    }

    stage('部署到k8s') {
      steps {
        kubernetesDeploy(configs: "$PROJECT_LOCATION/deploy/**", enableConfigSubstitution: true, kubeconfigId: "$KUBECONFIG_CREDENTIAL_ID")
      }
    }

  }

}