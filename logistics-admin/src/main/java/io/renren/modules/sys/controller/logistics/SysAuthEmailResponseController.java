package io.renren.modules.sys.controller.logistics;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import io.renren.modules.sys.dao.SysUserDao;
import io.renren.modules.sys.entity.SysUserEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-28 18:03
 */
@RestController
@RequestMapping("/sys/auth")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class SysAuthEmailResponseController {

    private final SysUserDao userDao;

    @GetMapping("/refuse/{email}")
    public ApiResponse<?> refuse(@PathVariable String email) {
        userDao.update(null , new UpdateWrapper<SysUserEntity>().set("status", 3).eq("email", email));
        return new ApiResponse<>(HeaderStatus.SUCCESS);
    }

    @GetMapping("/activation/{email}")
    public ApiResponse<?> activation(@PathVariable String email) {
        userDao.update(null , new UpdateWrapper<SysUserEntity>().set("status", 1).eq("email", email));
        return new ApiResponse<>(HeaderStatus.SUCCESS);
    }
}
