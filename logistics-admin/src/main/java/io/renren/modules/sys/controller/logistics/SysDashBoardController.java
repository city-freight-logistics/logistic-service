package io.renren.modules.sys.controller.logistics;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.api.client.util.Lists;
import com.google.api.client.util.Maps;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.DateUtils;
import com.logistics.micro.order.api.OrderAdminService;
import com.logistics.micro.order.dto.AdminOrderChartDto;
import com.logistics.micro.user.api.UserAdminService;
import io.renren.modules.sys.dao.SysHrEmailSendMapper;
import io.renren.modules.sys.entity.SysHrEmailSendEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-29 13:29
 */
@RestController
@RequestMapping("/sys/dashboard")
@Slf4j
public class SysDashBoardController {

    @Reference
    private OrderAdminService orderAdminService;
    @Autowired
    private SysHrEmailSendMapper hrEmailSendMapper;
    @Reference
    private UserAdminService adminService;

    @GetMapping("/order")
    public ApiResponse<?> order() {
        ApiResponse<AdminOrderChartDto> adminOrderChartDtoApiResponse = orderAdminService.dashBoardChart();
        ApiResponse<AdminOrderChartDto> adminOrderChartDtoApiResponse1 = orderAdminService.dashBoardChartL();
        Map<String, Object> orderMap = Maps.newHashMap();
        List<String> dateList = Lists.newArrayList();
        for(int i = -6; i <= 0; i++) {
            log.info("SysDashBoardController -> order date={}", DateUtils.dataFormatAdd(i));
            dateList.add(DateUtils.dataFormatAdd(i));
        }
        orderMap.put("expectedDataH", adminOrderChartDtoApiResponse.getBody().getAmount());
        orderMap.put("actualDataL", adminOrderChartDtoApiResponse1.getBody().getAmount());
        orderMap.put("orderMonCount", adminOrderChartDtoApiResponse.getBody().getSum());
        orderMap.put("dateList", dateList);
        orderMap.put("orderNumCount", adminOrderChartDtoApiResponse1.getBody().getNum());
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DAY_OF_YEAR, -7);
        long zero = instance.getTimeInMillis();
        orderMap.put("emailCount", hrEmailSendMapper.selectCount(new QueryWrapper<SysHrEmailSendEntity>().gt("create_time", zero / 1000)));
        orderMap.put("newUserCount", adminService.getNewUser());
    return new ApiResponse<>(orderMap);
    }
}
