package io.renren.modules.sys.controller.logistics;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-30 15:40
 */
@RestController
@RequestMapping("/sys/discount")
public class SysDiscountController {
}
