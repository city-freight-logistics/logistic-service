package io.renren.modules.sys.controller.logistics;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.user.api.UserAdminService;
import com.logistics.micro.user.dto.DriverAdminQueryDto;
import io.renren.common.annotation.SysLog;
import io.renren.modules.sys.dto.DriverDto;
import io.renren.modules.sys.service.SysDriverService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-25 10:48
 */
@RestController
@RequestMapping("/sys/driver")
public class SysDriverController {

    @Autowired
    private SysDriverService driverService;
    @Reference
    private UserAdminService userAdminService;

    /**
     * 车辆界面获取接口
     * @param query
     * @return
     */
    @PostMapping("/query/vehicle")
    public ApiResponse<?> queryVehicle(@RequestParam("query") String query) {
        DriverDto driverDto = GsonUtils.fromJson(query, DriverDto.class);
        return driverService.getVehicle(GsonUtils.strToMap(GsonUtils.toJSON(driverDto)));
    }

    /**
     * 司机界面获取接口
     * @param query
     * @return
     */
    @PostMapping("/query/list")
    public ApiResponse<?> queryList(@RequestParam("query") String query) {
        DriverAdminQueryDto driverDto = GsonUtils.fromJson(query, DriverAdminQueryDto.class);
        return userAdminService.getAdminDriver(driverDto);
    }

    /**
     * 司机审核接口
     *
     * @param query
     * @return
     */
    @PostMapping("/d/examine")
    @SysLog("司机审核")
    public ApiResponse<?> examineDriver(@RequestParam("examine") String query) {
        return userAdminService.examineDriver(query);
    }

    /**
     * 司机状态禁用解封接口
     *
     * @param query
     * @return
     */
    @PostMapping("/d/change/disable")
    @SysLog("司机状态更改")
    public ApiResponse<?> disableOrRe(@RequestParam("examine") String query) {
        return userAdminService.disableOrRe(query);
    }

    @PostMapping("/examine")
    @SysLog("车辆审核")
    public ApiResponse<?> examine(@RequestParam("states") String states, @RequestParam("query") String query) {
        DriverDto driverDto = GsonUtils.fromJson(query, DriverDto.class);
        return driverService.vehicleExamine(states, GsonUtils.strToMap(GsonUtils.toJSON(driverDto)));
    }

    @PostMapping("/disable")
    @SysLog("车辆禁用")
    public ApiResponse<?> disable(@RequestParam("states") String states, @RequestParam("query") String query) {
        DriverDto driverDto = GsonUtils.fromJson(query, DriverDto.class);
        return driverService.vehicleDisable(states, GsonUtils.strToMap(GsonUtils.toJSON(driverDto)));
    }

    @PostMapping("/unseal")
    @SysLog("车辆解封")
    public ApiResponse<?> unseal(@RequestParam("states") String states, @RequestParam("query") String query) {
        DriverDto driverDto = GsonUtils.fromJson(query, DriverDto.class);
        return driverService.vehicleUnseal(states, GsonUtils.strToMap(GsonUtils.toJSON(driverDto)));
    }
}
