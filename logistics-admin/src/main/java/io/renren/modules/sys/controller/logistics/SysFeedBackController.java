package io.renren.modules.sys.controller.logistics;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.order.api.FeedBackService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-26 17:34
 */
@RestController
@RequestMapping("/sys/feed/back")
public class SysFeedBackController {

    @Reference
    private FeedBackService feedBackService;

    @PostMapping("/query")
    public ApiResponse<?> getList(@RequestParam("query") String query) {
        return feedBackService.getFeedBackAdmin(query);
    }

    @PostMapping("/add")
    public ApiResponse<?> addFeedBack(@RequestParam("feedBack") String feedBackJson) {
        return feedBackService.addFeedBack(feedBackJson);
    }
}
