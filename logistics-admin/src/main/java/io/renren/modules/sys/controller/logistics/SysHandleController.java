package io.renren.modules.sys.controller.logistics;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.order.api.HandleService;
import com.logistics.micro.order.dto.AddHandleDto;
import com.logistics.micro.order.dto.HandleDto;
import io.renren.common.annotation.SysLog;
import io.renren.common.utils.MinioUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-29 23:20
 */
@RestController
@RequestMapping("/sys/handle")
public class SysHandleController {

    @Reference
    private HandleService handleService;

    /**
     * 获取申诉单 - 根绝dto条件获取
     *
     * @param query
     * @return
     */
    @PostMapping("/query/handle")
    public ApiResponse<?> queryVehicle(@RequestParam("query") String query) {
        HandleDto handleDto = GsonUtils.fromJson(query, HandleDto.class);
        return handleService.queryHandleList(handleDto);
    }

    @PostMapping("/examine")
    @SysLog("申诉单审核")
    public ApiResponse<?> examine(@RequestParam("query") String query) {
        return handleService.handleExamine(query);
    }

    @PostMapping("/apply")
    public ApiResponse<?> apply(@RequestParam("apply") String apply) {
        AddHandleDto addHandleDto = GsonUtils.fromJson(apply, AddHandleDto.class);
        return handleService.applyHandle(addHandleDto);
    }

    @PostMapping("/verify")
    public ApiResponse<?> verify(@RequestParam("orderCode") String orderCode, @RequestParam("userId") String userId) {
        return handleService.verifyHandelApply(orderCode, userId);
    }

    @PostMapping("/user/list")
    public ApiResponse<?> getUserHandleList(@RequestParam("userId") String userId) {
        return handleService.getUserHandleList(userId);
    }
}
