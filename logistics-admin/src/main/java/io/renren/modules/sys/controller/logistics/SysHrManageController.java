package io.renren.modules.sys.controller.logistics;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import io.renren.common.annotation.SysLog;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.sys.dto.HrListQueryDto;
import io.renren.modules.sys.dto.InviteDto;
import io.renren.modules.sys.service.SysHrEmailSendService;
import io.renren.modules.sys.service.SysRoleService;
import io.renren.modules.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 人事系统
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-17 22:53
 */
@RestController
@RequestMapping("sys/hr")
public class SysHrManageController extends AbstractController {

    @Autowired
    private SysHrEmailSendService hrEmailSendService;
    @Autowired
    private SysUserService userService;
    @Autowired
    private SysRoleService roleService;

    /**
     * 加入邀请
     *
     * @return
     */
    @PostMapping("/invite")
    @SysLog("入职邮件发送")
    public ApiResponse<?> invite(@RequestParam("invite") String inviteDto) {
        InviteDto inviteDto1 = GsonUtils.fromJson(inviteDto, InviteDto.class);
        return hrEmailSendService.sendEntryEmail(inviteDto1);
    }

    /**
     * list查询
     * @param query
     * @return
     */
    @PostMapping("/list")
    public ApiResponse<?> list(@RequestParam("query") String query) {
        HrListQueryDto hrListQueryDto = GsonUtils.fromJson(query, HrListQueryDto.class);
        return userService.queryByQueryList(hrListQueryDto);
    }

    @GetMapping("/role/list")
    public ApiResponse<?> roleList() {
        return roleService.getRoleList();
    }

    /**
     * 封禁、解封、状态更改通用接口
     * @param userId
     * @param status
     * @return
     */
    @PostMapping("/disable")
    public ApiResponse<?> disable(@RequestParam("user_id") String userId, @RequestParam("status") String status) {
        return userService.disable(userId, status);
    }

    @PostMapping("/verify/email")
    public ApiResponse<?> verifyEmail(@RequestParam("email") String email) {
        return userService.verifyEmail(email);
    }

    @PostMapping("/verify/phone")
    public ApiResponse<?> verifyPhone(@RequestParam("phone") String phone) {
        return userService.verifyPhone(phone);
    }
}
