package io.renren.modules.sys.controller.logistics;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.order.api.OrderAdminService;
import com.logistics.micro.order.dto.OrderAdminQueryDto;
import io.renren.common.annotation.SysLog;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-29 19:53
 */
@RestController
@RequestMapping("/sys/order")
public class SysOrderController {

    @Reference
    private OrderAdminService orderAdminService;

    @PostMapping("/query/order")
    public ApiResponse<?> queryOrder(@RequestParam("query") String query) {
        OrderAdminQueryDto orderAdminQueryDto = GsonUtils.fromJson(query, OrderAdminQueryDto.class);
        return orderAdminService.queryOrder(orderAdminQueryDto);
    }

    @PostMapping("/interrupt")
    @SysLog("异常订单强制终止")
    public ApiResponse<?> interrupt(@RequestParam("order") String orderAdminPo) {
        return orderAdminService.interruptOrder(orderAdminPo);
    }
}
