package io.renren.modules.sys.controller.logistics;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.user.api.UserAdminService;
import com.logistics.micro.user.dto.UserQueryListDto;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 23:42
 */
@RestController
@RequestMapping("/sys/admin")
public class SysUserManageController {

    @Reference
    private UserAdminService adminService;

    @PostMapping("/query")
    public ApiResponse<?> query(@RequestParam("query") String query) {
        return adminService.queryUserList(GsonUtils.fromJson(query, UserQueryListDto.class));
    }
}
