package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.SysHrEmailSendEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 人事系统DAO
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-17 23:08
 */
@Mapper
public interface SysHrEmailSendMapper extends BaseMapper<SysHrEmailSendEntity> {
}
