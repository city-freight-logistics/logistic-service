package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.SysUsernameCountEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 21:07
 */
@Mapper
public interface SysUsernameCountMapper extends BaseMapper<SysUsernameCountEntity> {
}
