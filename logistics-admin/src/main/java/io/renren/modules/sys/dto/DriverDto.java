package io.renren.modules.sys.dto;

import lombok.*;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-26 22:24
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DriverDto {
    private String page;
    private String limit;
    private String carClass;
    private String driverName;
    private String carCode;
    private String carState;
}
