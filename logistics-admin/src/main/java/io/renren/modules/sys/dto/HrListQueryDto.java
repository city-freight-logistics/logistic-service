package io.renren.modules.sys.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * hr列表页查询dto
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-30 15:35
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HrListQueryDto implements Serializable {
    private String page;
    private String limit;
    private String userId;
    private String userName;
    private String role;
    private String status;
}
