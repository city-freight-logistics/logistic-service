package io.renren.modules.sys.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * hr邮箱发送dto对象
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-30 15:34
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InviteDto implements Serializable {
    private String username;
    private String gender;
    private String email;
    private String mobile;
    private String role;
}
