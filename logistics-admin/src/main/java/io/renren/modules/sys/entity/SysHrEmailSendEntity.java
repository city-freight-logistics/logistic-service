package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 人事邮件记录表实体类
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-17 23:06
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_hr_email_send")
public class SysHrEmailSendEntity {
    private Integer id;
    private String uuid;
    private String sendTo;
    private String org;
    private Integer userId;
    private String name;
    private Integer operator;
    private Integer status;
    private Integer createTime;
}
