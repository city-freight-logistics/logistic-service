package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户名拼音记录表
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 21:06
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_user_name_count")
public class SysUsernameCountEntity implements Serializable {
    private Integer id;
    private String username;
    private Integer count;
}
