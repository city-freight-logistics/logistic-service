package io.renren.modules.sys.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 9:49
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HrListPo implements Serializable {
    private Integer userId;
    private String username;
    private Integer gender;
    private String email;
    private String mobile;
    private Integer status;
    private String role;
}
