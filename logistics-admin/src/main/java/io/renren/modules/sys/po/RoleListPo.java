package io.renren.modules.sys.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 10:35
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleListPo implements Serializable {
    private Integer key;
    private String  displayName;
}
