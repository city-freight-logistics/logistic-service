package io.renren.modules.sys.service;

import com.logistics.micro.common.model.ApiResponse;
import org.apache.dubbo.config.annotation.Reference;

import java.util.Map;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-25 10:43
 */
public interface SysDriverService {

    /**
     * 获取车辆列表统一接口
     *
     * @param param
     * @return
     */
    ApiResponse<?> getVehicle(Map<String, Object> param);

    /**
     * 司机车辆审核状态回调接口<br>
     * 幂等接口
     * @param driverStateDto
     * @return
     */
    ApiResponse<?> vehicleExamine(String driverStateDto, Map<String, Object> map);

    /**
     * 司机车辆禁用状态回调接口<br>
     * 幂等接口
     * @param driverStateDto
     * @return
     */
    ApiResponse<?> vehicleDisable(String driverStateDto, Map<String, Object> map);

    /**
     * 司机车辆解封状态回调接口<br>
     * 幂等接口
     * @param driverStateDto
     * @return
     */
    ApiResponse<?> vehicleUnseal(String driverStateDto, Map<String, Object> map);
}
