package io.renren.modules.sys.service;

import com.logistics.micro.common.model.ApiResponse;
import io.renren.modules.sys.dto.InviteDto;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-17 23:09
 */
public interface SysHrEmailSendService {

    /**
     * 入职邮件发送
     * @param inviteDto
     * @return
     */
    ApiResponse<?> sendEntryEmail(InviteDto inviteDto);
}
