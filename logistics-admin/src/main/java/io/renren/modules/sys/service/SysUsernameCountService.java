package io.renren.modules.sys.service;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 21:07
 */
public interface SysUsernameCountService {

    /**
     * 用户名计数
     *
     * @param username
     * @return
     */
    Integer getCount(String username);
}
