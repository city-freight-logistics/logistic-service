package io.renren.modules.sys.service.impl;

import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.mircro.driver.vehicle.api.VehicleStatesService;
import com.logistics.mircro.driver.vehicle.api.VehicledubboService;
import io.renren.modules.sys.service.SysDriverService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;

import java.util.Map;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-25 10:43
 */
@Service
public class SysDriverServiceImpl implements SysDriverService {

    @Reference
    private VehicleStatesService vehicleStatesService;

    @Override
    public ApiResponse<?> getVehicle(Map<String, Object> param) {
        Map<String, Object> map = vehicleStatesService.queryPage(param);
        return new ApiResponse<>(HeaderStatus.SUCCESS, map);
    }

    @Override
    public ApiResponse<?> vehicleExamine(String driverStateDto, Map<String, Object> map) {
        return vehicleStatesService.vehicleExamine(driverStateDto, map);
    }

    @Override
    public ApiResponse<?> vehicleDisable(String driverStateDto, Map<String, Object> map) {
        return vehicleStatesService.vehicleDisable(driverStateDto, map);
    }

    @Override
    public ApiResponse<?> vehicleUnseal(String driverStateDto, Map<String, Object> map) {
        return vehicleStatesService.vehicleUnseal(driverStateDto, map);
    }
}
