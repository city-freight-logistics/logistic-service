package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.AsyncMessageEnum;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.dto.HrMailDTO;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.message.api.AsyncMessageService;
import io.renren.common.utils.PinYinUtil;
import io.renren.modules.sys.dao.SysHrEmailSendMapper;
import io.renren.modules.sys.dao.SysUserDao;
import io.renren.modules.sys.dao.SysUserRoleDao;
import io.renren.modules.sys.dto.InviteDto;
import io.renren.modules.sys.entity.SysHrEmailSendEntity;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.entity.SysUserRoleEntity;
import io.renren.modules.sys.service.SysHrEmailSendService;
import io.renren.modules.sys.service.SysUsernameCountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-17 23:09
 */
@Service
@Slf4j
public class SysHrEmailSendServiceImpl extends ServiceImpl<SysHrEmailSendMapper, SysHrEmailSendEntity> implements SysHrEmailSendService {

    @Autowired
    private SysUserDao userDao;
    @Autowired
    private SysUserRoleDao roleDao;
    @Reference
    private AsyncMessageService messageService;
    @Autowired
    private SysUsernameCountService countService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse<?> sendEntryEmail(InviteDto inviteDto) {
        try {
            String name = inviteDto.getUsername();
            String pinyin = PinYinUtil.getPinyin(name);
            // 用户名后缀数字计算
            Integer count = countService.getCount(pinyin);
            log.info("SysHrEmailSendServiceImpl -> sendEntryEmail() ret={} count={}",inviteDto.toString(), count + 1);
            inviteDto.setUsername(pinyin + (count + 1));
            // 1:进行user表 及 权限表插入
            log.info("SysHrEmailSendServiceImpl -> sendEntryEmail() ret={} action={}",inviteDto.toString(), "进行user表插入");
            SysUserEntity userEntity = SysUserEntity.builder()
                    .email(inviteDto.getEmail())
                    .gender(Integer.parseInt(inviteDto.getGender()))
                    .username(inviteDto.getUsername())
                    .mobile(inviteDto.getMobile())
                    .status(2)
                    .password("9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d")
                    .salt("YzcmCZNvbXocrsz9dm8e")
                    .createUserId(1L)
                    .createTime(new Date())
                    .build();
            int insert = userDao.insert(userEntity);
            if (insert > 0) {
                log.info("SysHrEmailSendServiceImpl -> sendEntryEmail() ret={} action={}",inviteDto.toString(), "进行权限表插入");
                int insert1 = roleDao.insert(SysUserRoleEntity.builder()
                        .roleId(Long.parseLong((inviteDto.getRole())))
                        .userId(userEntity.getUserId())
                        .build());
                if (insert1 > 0) {
                    // 2:利用返回的user_id插入email表
                    log.info("SysHrEmailSendServiceImpl -> sendEntryEmail() ret={} action={}",inviteDto.toString(), "利用返回的user_id插入email表");
                    String uuid = UUID.randomUUID().toString();
                    boolean save = this.save(SysHrEmailSendEntity.builder()
                            .uuid(uuid)
                            .sendTo(inviteDto.getEmail())
                            .org("同城货运物流信息平台")
                            .userId(Integer.parseInt(userEntity.getUserId().toString()))
                            .name(inviteDto.getUsername())
                            .operator(1)
                            .status(1)
                            .createTime(Integer.parseInt(String.valueOf(System.currentTimeMillis() / 1000)))
                            .build());
                    if (save) {
                        // 3:组装email发送实体
                        log.info("SysHrEmailSendServiceImpl -> sendEntryEmail() ret={} action={}",inviteDto.toString(), "组装email发送实体");
                        HrMailDTO mailDTO = HrMailDTO.builder()
                                .uuid(uuid)
                                .sendTo(inviteDto.getEmail())
                                .org("同城货运物流信息平台")
                                .name(name)
                                .build();
                        // 4：将消息推送至消息队列，异步完成发送
                        log.info("SysHrEmailSendServiceImpl -> sendEntryEmail() ret={} action={}",inviteDto.toString(), "将消息推送至消息队列，异步完成发送");
                        ApiResponse<?> apiResponse = messageService.providerSendMessageInfo(AsyncMessageEnum.EMAIL_MESSAGE_TYPE, GsonUtils.toJSON(mailDTO));
                        if (HeaderStatus.SUCCESS.getCode().equals(apiResponse.getHeader().getCode())) {
                            return new ApiResponse<>(HeaderStatus.SUCCESS);
                        }
                    }
                }
            }
        } catch (NumberFormatException e) {
            log.error("SysHrEmailSendServiceImpl -> sendEntryEmail() ret={} exception={}",inviteDto.toString(), e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }

        return new ApiResponse<>();
    }
}
