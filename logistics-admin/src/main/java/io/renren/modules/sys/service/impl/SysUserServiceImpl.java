/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.order.dto.HandleInfoDto;
import io.renren.common.exception.RRException;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.SysUserDao;
import io.renren.modules.sys.dto.HrListQueryDto;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.po.HrListPo;
import io.renren.modules.sys.service.SysRoleService;
import io.renren.modules.sys.service.SysUserRoleService;
import io.renren.modules.sys.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
 * 系统用户
 *
 * @author Mark sunlightcs@gmail.com
 */
@Service("sysUserService")
@Slf4j
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements SysUserService {
	@Autowired
	private SysUserRoleService sysUserRoleService;
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysUserDao userDao;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String username = (String)params.get("username");
		Long createUserId = (Long)params.get("createUserId");

		IPage<SysUserEntity> page = this.page(
			new Query<SysUserEntity>().getPage(params),
			new QueryWrapper<SysUserEntity>()
				.like(StringUtils.isNotBlank(username),"username", username)
				.eq(createUserId != null,"create_user_id", createUserId)
		);

		return new PageUtils(page);
	}

	@Override
	public List<String> queryAllPerms(Long userId) {
		return baseMapper.queryAllPerms(userId);
	}

	@Override
	public List<Long> queryAllMenuId(Long userId) {
		return baseMapper.queryAllMenuId(userId);
	}

	@Override
	public SysUserEntity queryByUserName(String username) {
		return baseMapper.queryByUserName(username);
	}

	@Override
	@Transactional
	public void saveUser(SysUserEntity user) {
		user.setCreateTime(new Date());
		//sha256加密
		String salt = RandomStringUtils.randomAlphanumeric(20);
		user.setPassword(new Sha256Hash(user.getPassword(), salt).toHex());
		user.setSalt(salt);
		this.save(user);
		
		//检查角色是否越权
		checkRole(user);
		
		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
	}

	@Override
	@Transactional
	public void update(SysUserEntity user) {
		if(StringUtils.isBlank(user.getPassword())){
			user.setPassword(null);
		}else{
			user.setPassword(new Sha256Hash(user.getPassword(), user.getSalt()).toHex());
		}
		this.updateById(user);
		
		//检查角色是否越权
		checkRole(user);
		
		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
	}

	@Override
	public void deleteBatch(Long[] userId) {
		this.removeByIds(Arrays.asList(userId));
	}

	@Override
	public boolean updatePassword(Long userId, String password, String newPassword) {
		SysUserEntity userEntity = new SysUserEntity();
		userEntity.setPassword(newPassword);
		return this.update(userEntity,
				new QueryWrapper<SysUserEntity>().eq("user_id", userId).eq("password", password));
	}

	@Override
	public ApiResponse<?> queryByQueryList(HrListQueryDto queryDto) {
		try {
			if (Objects.isNull(queryDto)) {
				return new ApiResponse<>(HeaderStatus.INPUT_DATA_NULL);
			}
			Map<String, Object> params = GsonUtils.strToMap(GsonUtils.toJSON(queryDto));
//            IPage<HandlePo> page = getPageFromDB(params);
			IPage<HrListPo> query = userDao.queryByQueryList((Page<?>) new com.logistics.micro.common.utils.Query<HrListPo>().getPage(params), queryDto);
			final com.logistics.micro.common.utils.PageUtils pageUtils = new com.logistics.micro.common.utils.PageUtils(query);
			if (pageUtils.getTotalCount() == 0){
				return new ApiResponse<>(HeaderStatus.PAGE_CONTEXT_NULL_INFO);
			}
			return new ApiResponse<>(GsonUtils.beanToMap(pageUtils));
		} catch (Exception e) {
			log.info("SysUserServiceImpl -> queryByQueryList() ret={} exception={}",queryDto.toString(), e.getMessage());
			return new ApiResponse<>(HeaderStatus.EXCPEITON);
		}
	}

	@Override
	public ApiResponse<?> disable(String userId, String status) {
		return this.update(new UpdateWrapper<SysUserEntity>().set("status", status).eq("user_id", userId)) ? new ApiResponse<>(HeaderStatus.SUCCESS) : new ApiResponse<>();
	}

	@Override
	public ApiResponse<?> verifyEmail(String email) {
		return this.count(new QueryWrapper<SysUserEntity>().eq("email", email)) > 0 ? new ApiResponse<>(HeaderStatus.QUERY_TARGET_EXIST) : new ApiResponse<>(HeaderStatus.SUCCESS);
	}

	@Override
	public ApiResponse<?> verifyPhone(String phone) {
		return this.count(new QueryWrapper<SysUserEntity>().eq("mobile", phone)) > 0 ? new ApiResponse<>(HeaderStatus.QUERY_TARGET_EXIST) : new ApiResponse<>(HeaderStatus.SUCCESS);
	}

	/**
	 * 检查角色是否越权
	 */
	private void checkRole(SysUserEntity user){
		if(user.getRoleIdList() == null || user.getRoleIdList().size() == 0){
			return;
		}
		//如果不是超级管理员，则需要判断用户的角色是否自己创建
		if(user.getCreateUserId() == Constant.SUPER_ADMIN){
			return ;
		}
		
		//查询用户创建的角色列表
		List<Long> roleIdList = sysRoleService.queryRoleIdList(user.getCreateUserId());

		//判断是否越权
		if(!roleIdList.containsAll(user.getRoleIdList())){
			throw new RRException("新增用户所选角色，不是本人创建");
		}
	}
}