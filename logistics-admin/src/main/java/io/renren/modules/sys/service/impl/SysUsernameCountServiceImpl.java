package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.sys.dao.SysUsernameCountMapper;
import io.renren.modules.sys.entity.SysUsernameCountEntity;
import io.renren.modules.sys.service.SysUsernameCountService;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 21:08
 */
@Service
public class SysUsernameCountServiceImpl extends ServiceImpl<SysUsernameCountMapper, SysUsernameCountEntity> implements SysUsernameCountService {

    @Override
    public Integer getCount(String username) {
        SysUsernameCountEntity username1 = this.getOne(new QueryWrapper<SysUsernameCountEntity>().eq("username", username));
        if (Objects.isNull(username1)) {
            this.save(SysUsernameCountEntity.builder()
                    .count(1)
                    .username(username)
                    .build());
            return 0;
        } else {
            this.update(new UpdateWrapper<SysUsernameCountEntity>().set("count", username1.getCount() + 1).eq("username", username));
            return username1.getCount();
        }
    }
}
