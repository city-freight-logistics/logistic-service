package com.logistics.micro.message.api;

import com.logistics.micro.common.enums.AsyncMessageEnum;
import com.logistics.micro.common.model.ApiResponse;

/**
 * 异步消息发送接口
 *      现包含：短信服务，邮件服务
 *      策略模式实现
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-10 17:16
 */
public interface AsyncMessageService {

    /**
     * 提交短信发送请求到消息队列
     *
     * @param msgJson
     * @return
     */
    ApiResponse<?> providerSendMessageInfo(AsyncMessageEnum messageEnum, String msgJson);
}
