package com.logistics.micro.message.api;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.rabbit.RabbitMessage;

/**
 * 订单消息队列服务
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-09 17:19
 */
public interface OrderService {

    /**
     * 司机抢单派单
     *
     * @param rabbitMessage
     * @return
     */
    ApiResponse<?> providerSendOrderInfo(RabbitMessage rabbitMessage);

    ApiResponse<?> providerSendOrderStatus(RabbitMessage rabbitMessage);
}
