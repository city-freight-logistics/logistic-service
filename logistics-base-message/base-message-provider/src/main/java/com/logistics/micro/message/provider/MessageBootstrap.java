package com.logistics.micro.message.provider;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-06 22:48
 */
@SpringBootApplication
@EnableRabbit
public class MessageBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(MessageBootstrap.class, args);
    }
}
