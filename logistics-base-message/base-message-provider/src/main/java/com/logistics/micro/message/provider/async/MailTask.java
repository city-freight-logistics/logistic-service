package com.logistics.micro.message.provider.async;

import com.logistics.micro.message.provider.config.ServerConfig;
import com.logistics.micro.message.provider.model.Mail;
import com.logistics.micro.message.provider.service.IMailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;

import java.time.LocalDateTime;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-17 22:15
 */
@Component
@Slf4j
public class MailTask {
    @Autowired
    private ServerConfig serverConfig;
    @Autowired
    private IMailService mailService;

    /**
     * 激活邮件异步请求方法体
     *
     * @param uuid   标识号
     * @param sendTo 发送目标邮箱
     */
    @Async
    public void activeMailTask(String uuid, String sendTo, String org, String name) {
        //邮箱验证发送整体逻辑
        Context context = new Context();
        context.setVariable("company", org);
        context.setVariable("userId", sendTo);
        context.setVariable("url", "http://103.205.254.217:31001");
        context.setVariable("name", name);
        context.setVariable("email", sendTo);
        context.setVariable("createTime", LocalDateTime.now().plusDays(7).toString());
        Mail mail = mailService.prepareMail(context, sendTo);
        log.info("发送往" + sendTo + "的邮件正在发送=======>");
        mailService.sendActiveMail(mail);
        log.info("发送往" + sendTo + "的邮件--已送达--");
    }
}
