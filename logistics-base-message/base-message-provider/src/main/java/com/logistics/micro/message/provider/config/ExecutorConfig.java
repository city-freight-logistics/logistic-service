package com.logistics.micro.message.provider.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.*;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/25 13:50
 * @description
 */

@Configuration
public class ExecutorConfig {
    private static final Logger logger = LoggerFactory.getLogger(ExecutorConfig.class);
    private static int count = 0;
    /**
     * 核心线程数
     */
//    private int corePoolSize = Runtime.getRuntime().availableProcessors();
    private int corePoolSize = 10;
    /**
     * 最大线程数
     */
    private int maxPoolSize = 20;
    /**
     * 线程销毁时间
     */
    private Long keepAliveTime = 5L;
    /**
     * 任务队列
     */
    private BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(5);
    /**
     * 线程名称
     */
    private ThreadFactory namedThreadFactory = r -> new Thread(r,"message-thread-" + ++count);

    @Bean
    public ExecutorService createThreadPool() {
        logger.info("线程池创建===>开始");
        /**
         * 创建线程池
         * Runtime.getRuntime().availableProcessors()
         */
        ExecutorService threadPool = new ThreadPoolExecutor(corePoolSize,
                maxPoolSize,
                keepAliveTime,
                TimeUnit.SECONDS,
                queue,
                namedThreadFactory);
        logger.info("线程池创建===>结束");
        return threadPool;
    }
}
