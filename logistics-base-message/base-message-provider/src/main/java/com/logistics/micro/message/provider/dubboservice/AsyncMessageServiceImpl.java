package com.logistics.micro.message.provider.dubboservice;

import com.logistics.micro.common.enums.AsyncMessageEnum;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.dto.HrMailDTO;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.message.api.AsyncMessageService;
import com.logistics.micro.message.provider.enums.ExchangeEnum;
import com.logistics.micro.message.provider.enums.RouteKeyEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-10 17:17
 */
@Service
@Slf4j
@Component
public class AsyncMessageServiceImpl implements AsyncMessageService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public ApiResponse<?> providerSendMessageInfo(AsyncMessageEnum messageEnum, String msgJson) {
        try {
            String uuid = UUID.randomUUID().toString();
            log.info("PhoneMessageServiceImpl -> providerSendPhoneMessageInfo message uuid:{}", uuid);
            // 把消息对象放入路由对应的队列当中去
            RabbitMessage<HrMailDTO> hrMailDTORabbitMessage = new RabbitMessage<>();
            HrMailDTO mailDTO = GsonUtils.fromJson(msgJson, HrMailDTO.class);
            hrMailDTORabbitMessage.setBody(mailDTO);
            hrMailDTORabbitMessage.setDesc("email");
            hrMailDTORabbitMessage.setCode(200);
            rabbitTemplate.convertAndSend(
                    ExchangeEnum.ASYNC_MAEESAGE_SEND_EXCHANGE.getExchangeName(),
                    RouteKeyEnum.EMAIL_MESSAGE_ROUTE_KEY.getRouteKeyName(),
                    hrMailDTORabbitMessage,
                    new CorrelationData(uuid));
            return new ApiResponse<>(HeaderStatus.SUCCESS, ImmutablePair.of("msg_uuid", uuid));
        } catch (AmqpException e) {
            log.error("PhoneMessageServiceImpl -> providerSendPhoneMessageInfo exception:{}", e.getMessage());
            return new ApiResponse<>(HeaderStatus.MASSAGE_SEND_ERROR);
        }
    }
}
