package com.logistics.micro.message.provider.dubboservice;

import com.google.gson.Gson;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.message.api.OrderService;
import com.logistics.micro.message.provider.enums.ExchangeEnum;
import com.logistics.micro.message.provider.enums.RouteKeyEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Dubbo服务实现类
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-09 17:18
 */
@Service
@Slf4j
@Component
public class OrderServiceImpl implements OrderService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public ApiResponse<?> providerSendOrderInfo(RabbitMessage rabbitMessage) {
        try {
            String uuid = UUID.randomUUID().toString();
            log.info("OrderServiceImpl -> providerSendOrderInfo message uuid:{}", uuid);
            // 把消息对象放入路由对应的队列当中去
            rabbitTemplate.convertAndSend(
                    ExchangeEnum.ORDER_EXCHANGE.getExchangeName(),
                    RouteKeyEnum.ORDER_ROUTE_KEY.getRouteKeyName(),
                    rabbitMessage,
                    new CorrelationData(uuid));
            return new ApiResponse<>(HeaderStatus.SUCCESS, ImmutablePair.of("msg_uuid", uuid));
        } catch (AmqpException e) {
            log.error("OrderServiceImpl -> providerSendOrderInfo exception:{}", e.getMessage());
            return new ApiResponse<>(HeaderStatus.MASSAGE_SEND_ERROR);
        }
    }

    @Override
    public ApiResponse<?> providerSendOrderStatus(RabbitMessage rabbitMessage) {
        try {
            String uuid = UUID.randomUUID().toString();
            log.info("OrderServiceImpl -> providerSendOrderInfo message uuid:{}", uuid);
            // 把消息对象放入路由对应的队列当中去
            rabbitTemplate.convertAndSend(
                    ExchangeEnum.ORDER_EXCHANGE.getExchangeName(),
                    RouteKeyEnum.ORDER_STATUS_ROUTE_KEY.getRouteKeyName(),
                    rabbitMessage,
                    new CorrelationData(uuid));
            return new ApiResponse<>(HeaderStatus.SUCCESS, ImmutablePair.of("msg_uuid", uuid));
        } catch (AmqpException e) {
            log.error("OrderServiceImpl -> providerSendOrderInfo exception:{}", e.getMessage());
            return new ApiResponse<>(HeaderStatus.MASSAGE_SEND_ERROR);
        }
    }
}
