package com.logistics.micro.message.provider.enums;

import lombok.Getter;

/**
 * 交换机枚举类
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-09 16:47
 */
@Getter
public enum ExchangeEnum {

    ORDER_EXCHANGE(1, "order-exchange", "订单交换机"),
    ASYNC_MAEESAGE_SEND_EXCHANGE(2, "async-message-send-exchange", "异步消息交换机")
    ;

    private final Integer exchangeID;
    private final String exchangeName;
    private final String describe;

    ExchangeEnum(Integer exchangeID, String exchangeName, String describe) {
        this.exchangeID = exchangeID;
        this.exchangeName = exchangeName;
        this.describe = describe;
    }
}
