package com.logistics.micro.message.provider.enums;

import lombok.Getter;

/**
 * 队列枚举类
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-09 16:55
 */
@Getter
public enum QueueEnum {

    ORDER_QUEUE(1, "order-queue", "听单、接单队列"),
    PHONE_MESSAGE_QUEUE(2, "phone-message-queue", "短信消息队列"),
    EMAIL_MESSAGE_QUEUE(3, "email-message-queue", "邮件消息队列")
    ;

    private final Integer queueID;
    private final String queueName;
    private final String describe;

    QueueEnum(Integer queueID, String queueName, String describe) {
        this.queueID = queueID;
        this.queueName = queueName;
        this.describe = describe;
    }
}
