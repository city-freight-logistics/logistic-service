package com.logistics.micro.message.provider.enums;

import lombok.Getter;

/**
 * 路由件枚举类
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-09 17:00
 */
@Getter
public enum RouteKeyEnum {

    ORDER_ROUTE_KEY(1, "order-queue", "听单、接单路由件"),
    PHONE_MESSAGE_ROUTE_KEY(2, "phone-message", "短信服务路由件"),
    EMAIL_MESSAGE_ROUTE_KEY(3, "email-message", "邮件服务路由件"),
    ORDER_STATUS_ROUTE_KEY(4, "order-status-queue","传递订单状态路由件")
    ;

    private final Integer routeKeyID;
    private final String routeKeyName;
    private final String describe;

    RouteKeyEnum(Integer routeKeyID, String routeKeyName, String describe) {
        this.routeKeyID = routeKeyID;
        this.routeKeyName = routeKeyName;
        this.describe = describe;
    }
}
