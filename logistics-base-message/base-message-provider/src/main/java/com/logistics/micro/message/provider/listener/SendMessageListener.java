package com.logistics.micro.message.provider.listener;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.logistics.micro.common.model.dto.HrMailDTO;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.message.provider.async.MailTask;
import com.logistics.micro.message.provider.model.OrderStatusTempDto;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 *
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-06 23:54
 */
@Component
@Slf4j
public class SendMessageListener {

    @Autowired
    private MailTask mailTask;

//    @RabbitListener(queues = {"phone-message-queue"})
//    public void sendPhoneMessage(Message message, String messageStr, Channel channel) throws IOException {
//        RabbitMessage<?> phoneMessage = GsonUtils.fromJson(messageStr, RabbitMessage.class);
//        String code = String.valueOf(message.getBody());
//        log.info("SendMessageListener -> sendMessage() body={}", code);
//        log.info("SendMessageListener -> sendMessage() code={}", phoneMessage.getCode());
//        log.info("SendMessageListener -> sendMessage() code={}", phoneMessage.getDesc());
//        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
//    }

    @RabbitListener(queues = {"email-message-queue"})
    public void sendEmailMessage(Message message, String messageStr, Channel channel) throws IOException {
        RabbitMessage<HrMailDTO> rabbitMessage = new Gson().fromJson(messageStr, new TypeToken<RabbitMessage<HrMailDTO>>() {}.getType());
        HrMailDTO hrMailDTO = rabbitMessage.getBody();
        log.info("SendMessageListener -> sendEmailMessage send email message={}", messageStr);
        mailTask.activeMailTask(hrMailDTO.getUuid(), hrMailDTO.getSendTo(), hrMailDTO.getOrg(), hrMailDTO.getName());
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
