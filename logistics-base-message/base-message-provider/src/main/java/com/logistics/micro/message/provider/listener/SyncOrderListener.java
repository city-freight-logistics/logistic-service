package com.logistics.micro.message.provider.listener;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.order.OrderStatusDto;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.message.provider.model.OrderStatusTempDto;
import com.logistics.micro.order.api.OrderStateService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

/**
 * 订单同步状态监听
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-27 14:51
 */
@Component
@Slf4j
public class SyncOrderListener {

    private static Channel local_channel = null;
    @Resource(name = "createThreadPool")
    private ExecutorService threadPool;
    @Reference
    private OrderStateService orderStateService;

    @RabbitListener(queues = {"order-status-queue"})
    public void syncOrderContentListener(Message message, String content, Channel channel) throws IOException {
        log.info("SyncOrderListener -> syncOrderContentListener ret={}", content);
        if ( local_channel == null) {
            log.info("local_channel is already");
            local_channel = channel;
        }
        threadPool.execute(() -> {
            try {
                log.info("syncOrderContentJob is running");
                RabbitMessage<OrderStatusTempDto> rabbitMessage = new Gson().fromJson(content, new TypeToken<RabbitMessage<OrderStatusTempDto>>() {}.getType());
                OrderStatusTempDto body = rabbitMessage.getBody();
                OrderStatusDto orderDto = OrderStatusDto.builder()
                        .orderName(body.getOrderName())
                        .orderState(body.getOrderState())
                        .orderCode(body.getOrderCode())
                        .beginDate(null)
                        .completeDate((Objects.nonNull(body.getCompleteDate()) && body.getCompleteDate() > 0) ? new Date(body.getCompleteDate()) : null)
                        .build();
//                Object body = GsonUtils.fromJson(content, RabbitMessage<>.class).getBody();
//                GsonUtils.mapToBean((Map<String, Object>) GsonUtils.fromJson(content, RabbitMessage.class).getBody(),orderDto);

                ApiResponse<?> response = orderStateService.syncDriverOrderState(orderDto);

                log.info("syncOrderContentJob sync result={}", response);

                if (Objects.isNull(response)) {
                    throw new Exception("syncOrderContentJob sync response is null");
                } else if (response.getHeader().getCode() == 200) {
                    channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
                } else if (response.getHeader().getCode() == 701) {
                    throw new Exception("syncOrderContentJob sync data not exist throw data=" + orderDto);
                }
                channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,true);
            } catch (Exception e) {
                log.error("syncOrderContentJob got exception ret={} exception={}", content, e.getMessage());
                try {
                    channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,true);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
            try {
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
