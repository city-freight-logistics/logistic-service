package com.logistics.micro.message.provider.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 邮件系统通用对象
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-17 22:11
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mail {
    /**
     * 邮件发送方
     */
    private String from;
    /**
     * 邮件接收方
     */
    private String to;
    /**
     * 邮件主题
     */
    private String subject;
    /**
     * 邮件内容
     */
    private String mailContent;
}
