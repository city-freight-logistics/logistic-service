package com.logistics.micro.message.provider.service;

import com.logistics.micro.message.provider.model.Mail;
import org.thymeleaf.context.Context;

/**
 * 占位
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-10 17:42
 */
public interface IMailService {
    Mail prepareMail(Context context, String sendTo);

    /**
     * 发送激活邮件验证
     *
     * @param mail 发送邮件需要的基本参数，具体参照Mail定义
     */
    void sendActiveMail(Mail mail);
}
