package com.logistics.micro.message.provider.service.impl;

import com.logistics.micro.message.provider.model.Mail;
import com.logistics.micro.message.provider.service.IMailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-17 22:27
 */
@Service
@Slf4j
public class MailServiceImpl implements IMailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine templateEngine;

    /**
     * 发送邮箱准备接口
     * @param context 邮箱内部数据
     * @param sendTo 发送对象
     * @return
     */
    @Override
    public Mail prepareMail(Context context, String sendTo) {
        String emailContent = templateEngine.process("activeTemplate", context);
        Mail mail = Mail.builder()
                .from("thulium0601@163.com")
                .to(sendTo)
                .subject("Please active account click under emial...")
                .mailContent(emailContent)
                .build();
        return mail;
    }

    /**
     * 发送邮件核心方法
     *
     * @param mail 发送邮件需要的基本参数，具体参照Mail定义
     */
    @Override
    public void sendActiveMail(Mail mail) {
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(mail.getFrom());
            helper.setTo(mail.getTo());
            helper.setSubject(mail.getSubject());
            helper.setText(mail.getMailContent(), true);
            javaMailSender.send(message);
            log.info("激活验证邮件发送成功...");
        } catch (MessagingException e) {
            log.error("验证邮件发送失败...", e);
        }
    }
}
