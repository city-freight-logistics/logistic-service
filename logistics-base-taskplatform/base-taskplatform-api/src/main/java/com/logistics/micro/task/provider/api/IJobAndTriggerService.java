package com.logistics.micro.task.provider.api;

import com.logistics.micro.common.model.ApiResponse;

import java.util.Map;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-28 15:00
 */
public interface IJobAndTriggerService {

    /**
     * 定时任务分页查询，后续应添加对应的条件筛选
     *
     * @param param
     * @return
     */
    ApiResponse<?> getJobAndTriggerDetailsByPage(Map<String, Object> param);

    /**
     * 添加定时任务方法
     *
     * @param jsonStr NewJobVo类JSON格式
     * @return
     */
    ApiResponse<?> addNewJob(String jsonStr);

    /**
     * 定时任务暂停方法
     *
     * @param jobClassName Job类名
     * @param jobGroupName Job组名
     * @return
     */
    ApiResponse<?> pauseJob(String jobClassName, String jobGroupName);

    /**
     * 定时任务恢复
     *
     * @param jobClassName Job类名
     * @param jobGroupName Job组名
     * @return
     */
    ApiResponse<?> resumeJob(String jobClassName, String jobGroupName);

    /**
     * 定时任务更新方法
     *
     * @param jsonStr NewJobVo类JSON格式
     * @return
     */
    ApiResponse<?> rescheduleJob(String jsonStr);

    /**
     * 删除定时任务方法
     *
     * @param jobClassName Job类名
     * @param jobGroupName Job组名
     * @return
     */
    ApiResponse<?> deleteJob(String jobClassName, String jobGroupName);
}
