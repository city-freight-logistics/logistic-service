package com.logistics.micro.task.provider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-29 23:10
 */
@SpringBootApplication
@MapperScan("com.logistics.micro.task.provider.mapper")
public class TaskPlatformBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(TaskPlatformBootstrap.class, args);
    }
}
