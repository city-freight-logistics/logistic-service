package com.logistics.micro.task.provider.controller;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.task.provider.api.IJobAndTriggerService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-28 15:03
 */
@RestController
@Slf4j
@RequestMapping("job")
public class JobController {

    @Autowired
    private IJobAndTriggerService iJobAndTriggerService;

    //加入Qulifier注解，通过名称注入bean
    @Autowired @Qualifier("Scheduler")
    private Scheduler scheduler;

    /**
     * 添加一个定时任务
     *
     * @param jsonStr NewJobVo类JSON格式
     * @return
     */
    @PostMapping(value="/addjob")
    public ApiResponse<?> addJob(@RequestParam("jsonStr") String jsonStr) {
        log.info("JobController --> addJob() ret={}", jsonStr);
        return iJobAndTriggerService.addNewJob(jsonStr);
    }

    /**
     * 定时任务暂停方法
     *
     * @param jobClassName 定时任务类名
     * @param jobGroupName 定时任务组名
     * @return
     * @throws Exception
     */
    @PostMapping(value="/pausejob")
    public ApiResponse<?> pauseJob(@RequestParam(value="jobClassName")String jobClassName,
                                   @RequestParam(value="jobGroupName")String jobGroupName) {
        log.info("JobController --> pauseJob() ret=jobClassName: {}, jobGroupName: {}", jobClassName, jobGroupName);
        return iJobAndTriggerService.pauseJob(jobClassName, jobGroupName);
    }

    /**
     * 定时任务恢复
     *
     * @param jobClassName Job类名
     * @param jobGroupName Job组名
     * @return
     */
    @PostMapping(value="/resumejob")
    public ApiResponse<?> resumeJob(@RequestParam(value="jobClassName")String jobClassName,
                                    @RequestParam(value="jobGroupName")String jobGroupName) {
        log.info("JobController --> resumeJob() ret=jobClassName: {}, jobGroupName: {}", jobClassName, jobGroupName);
        return iJobAndTriggerService.resumeJob(jobClassName, jobGroupName);
    }

    /**
     * 定时任务更新方法
     *
     * @param jsonStr NewJobVo类JSON格式
     * @return
     */
    @PostMapping(value="/reschedulejob")
    public ApiResponse<?> rescheduleJob(@RequestParam String jsonStr) {
        log.info("JobController --> rescheduleJob() ret={}", jsonStr);
        return iJobAndTriggerService.rescheduleJob(jsonStr);
    }

    /**
     * 删除定时任务方法
     *
     * @param jobClassName Job类名
     * @param jobGroupName Job组名
     * @return
     */
    @PostMapping(value="/deletejob")
    public ApiResponse<?> deleteJob(@RequestParam(value="jobClassName")String jobClassName, @RequestParam(value="jobGroupName")String jobGroupName) {
        log.info("JobController --> deleteJob() ret=jobClassName: {}, jobGroupName: {}", jobClassName, jobGroupName);
        return iJobAndTriggerService.deleteJob(jobClassName, jobGroupName);
    }

    /**
     * 分页查询任务列表
     * TODO 后续添加按条件筛选
     *
     * @param param
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    @PostMapping(value="/queryjob")
    public ApiResponse<?> queryJob(@RequestParam Map<String, Object> param) {
        log.info("JobController --> queryJob() ret={}", param);
        return iJobAndTriggerService.getJobAndTriggerDetailsByPage(param);
    }

}
