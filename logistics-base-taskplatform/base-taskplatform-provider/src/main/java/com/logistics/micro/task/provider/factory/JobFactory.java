package com.logistics.micro.task.provider.factory;

import com.logistics.micro.task.provider.job.impl.HelloJob;
import com.logistics.micro.task.provider.job.impl.MqOrderJob;
import com.logistics.micro.task.provider.job.impl.NewJob;
import com.logistics.micro.task.provider.job.impl.TempOrderJob;

/**
 * Job实力创建工厂
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-02 22:55
 */
public class JobFactory {

    private JobFactory() {}

    public static Class<?> getJobByName(String jobName) {
        Class<?> c = null;
        switch (jobName) {
            case "Hello" :
                c = HelloJob.class;
                break;
            case "New" :
                c = NewJob.class;
                break;
            case "MqOrder" :
                c = MqOrderJob.class;
                break;
            case "TempOrder" :
                c = TempOrderJob.class;
        }
        return c;
    }

}
