package com.logistics.micro.task.provider.job;

import org.quartz.Job;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-28 14:28
 */
public interface BaseJob extends Job {
}
