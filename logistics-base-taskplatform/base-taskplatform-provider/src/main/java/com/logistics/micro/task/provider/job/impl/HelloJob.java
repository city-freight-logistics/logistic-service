package com.logistics.micro.task.provider.job.impl;

import com.logistics.micro.task.provider.job.BaseJob;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-28 14:30
 */
@Slf4j
public class HelloJob implements BaseJob {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("HelloJob start at {}", new Date());
        try {
            // 模拟定时任务执行
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error("HelloJob got exception: {} at {}", e.toString(), new Date());
        }
        log.info("HelloJob finish at {}", new Date());
    }
}
