package com.logistics.micro.task.provider.job.impl;

import com.google.common.collect.Maps;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.grabbing.dispatching.api.MQAckDubboService;
import com.logistics.micro.order.api.TempOrderService;
import com.logistics.micro.task.provider.enums.CachekeyEnum;
import com.logistics.micro.task.provider.job.BaseJob;
import com.logistics.micro.task.provider.model.po.message.MessageAckDto;
import com.logistics.micro.task.provider.model.po.tempOrder.TempOrderPO;
import com.logistics.micro.task.provider.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 定时取redis中的消息缓存，检查是否有被消费
 *
 * @author xionghw0982
 * @date 2021-04-15 22:12
 */
@Slf4j
public class MqOrderJob implements BaseJob {
    private final String ORDER_CACHE_KEY = "order-message";

    @Autowired
    private RedisUtil redisUtil;
    @Reference
    private TempOrderService tempOrderService;
    @Reference
    private MQAckDubboService mqAckDubboService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        // 从DB的临时订单表中获取订单信息
        log.info("从DB的临时订单表中获取订单信息");
        final String all = tempOrderService.getAll();
        final List<TempOrderPO> list = GsonUtils.gsonToList(all, TempOrderPO.class);

        list.forEach( tempOrderPO -> {
            String ackDto = String.valueOf(redisUtil.hget(CachekeyEnum.ORDER_MESSAGE_CACHE_KEY.getKey(),tempOrderPO.getOrderCode()));
            if (!"null".equals(ackDto)){
                MessageAckDto messageAckDto = GsonUtils.fromJson(ackDto, MessageAckDto.class);
                if (check(messageAckDto)) {
                    mqAckDubboService.noticeMQAck(messageAckDto.getDeliveryTag());
                }
            }
        });

    }

    /**
     *
     * @param messageAckDto
     * @return true : 消费了  false ： 未消费
     */
    private Boolean check(MessageAckDto messageAckDto) {
        // 如果还没有消费
        if (messageAckDto.getMessageState() == 0) {
            // 通知消息队列，重新入队
            log.info("消息重新入队,订单号为{}",messageAckDto.getOrderCode());
            Map<String,String> map = new HashMap<>(1);
            map.put("message",GsonUtils.toJSON(messageAckDto));
            mqAckDubboService.reQueueMQ(map);
            return false;
        }
        return true;
    }
}
