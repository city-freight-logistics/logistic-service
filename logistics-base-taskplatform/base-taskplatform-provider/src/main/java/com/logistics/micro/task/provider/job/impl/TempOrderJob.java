package com.logistics.micro.task.provider.job.impl;

import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.order.api.DriverOrderDataService;
import com.logistics.micro.order.api.TempOrderService;
import com.logistics.micro.task.provider.enums.CachekeyEnum;
import com.logistics.micro.task.provider.job.BaseJob;
import com.logistics.micro.task.provider.model.po.message.MessageAckDto;
import com.logistics.micro.task.provider.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 对于订单的临时信息进行检查，如果没用了就删除缓存
 *
 * @author xionghw0982
 * @date 2021-04-18 15:02
 */
@Slf4j
public class TempOrderJob implements BaseJob {
    @Autowired
    private RedisUtil redisUtil;
    @Reference
    private TempOrderService tempOrderService;
    @Reference
    private DriverOrderDataService driverOrderDataService;


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Map<Object, Object> map = redisUtil.hmget("order-message");
        List<String> deleteIDs = new ArrayList<>(map.size());
        map.forEach((key,value) -> {
            MessageAckDto messageAckDto = GsonUtils.fromJson(String.valueOf(value),MessageAckDto.class);
            // 已经被消费了
            if (messageAckDto.getOrderState() >= 2 && messageAckDto.getMessageState() == 1) {
                // 删除订单消息缓存
                redisUtil.hdel(CachekeyEnum.ORDER_MESSAGE_CACHE_KEY.getKey(),key);
                // 删除订单临时信息缓存
                redisUtil.hdel(CachekeyEnum.ORDER_TEMP_CACHE_KEY.getKey(),key);

                // 删除订单接单人数缓存
                redisUtil.hdel(CachekeyEnum.ORDER_ACCEPT.getKey(),key);
                // 删除订单拒绝人数缓存
                final String hget = String.valueOf(redisUtil.hget(CachekeyEnum.DRIVER_REJECT_CACHE_KEY.getKey(), String.valueOf(key)));
                if (!"null".equals(hget)){
                    final List<String> rejectList = GsonUtils.gsonToList(hget, String.class);
                    redisUtil.hdel(CachekeyEnum.DRIVER_REJECT_CACHE_KEY.getKey(),key);
                    deleteIDs.add(String.valueOf(key));
                }
            }
        });
        if (deleteIDs.size() > 0) {
            driverOrderDataService.calculateRate();
            tempOrderService.deleteBatch(deleteIDs);
        }
    }
}
