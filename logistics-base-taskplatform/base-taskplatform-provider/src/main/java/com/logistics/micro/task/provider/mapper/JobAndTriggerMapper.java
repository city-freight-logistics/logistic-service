package com.logistics.micro.task.provider.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.logistics.micro.task.provider.model.vo.JobAndTrigger;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-28 14:57
 */
public interface JobAndTriggerMapper {

    @Results(id = "jobAndTrigger",
            value = {
                    @Result(property = "jobName", column = "JOB_NAME"),
                    @Result(property = "jobGroup", column = "JOB_GROUP"),
                    @Result(property = "jobClassName", column = "JOB_CLASS_NAME"),
                    @Result(property = "triggerName", column = "TRIGGER_NAME"),
                    @Result(property = "triggerGroup", column = "TRIGGER_GROUP"),
                    @Result(property = "cronExpression", column = "CRON_EXPRESSION"),
                    @Result(property = "timeZoneId", column = "TIME_ZONE_ID")
            }
    )
    @Select("SELECT\n" +
            "\tQRTZ_JOB_DETAILS.JOB_NAME,\n" +
            "\tQRTZ_JOB_DETAILS.JOB_GROUP,\n" +
            "\tQRTZ_JOB_DETAILS.JOB_CLASS_NAME,\n" +
            "\tQRTZ_TRIGGERS.TRIGGER_NAME,\n" +
            "\tQRTZ_TRIGGERS.TRIGGER_GROUP,\n" +
            "\tQRTZ_CRON_TRIGGERS.CRON_EXPRESSION,\n" +
            "\tQRTZ_CRON_TRIGGERS.TIME_ZONE_ID \n" +
            "FROM\n" +
            "\tQRTZ_JOB_DETAILS\n" +
            "\tJOIN QRTZ_TRIGGERS\n" +
            "\tJOIN QRTZ_CRON_TRIGGERS ON QRTZ_JOB_DETAILS.JOB_NAME = QRTZ_TRIGGERS.JOB_NAME \n" +
            "\tAND QRTZ_TRIGGERS.TRIGGER_NAME = QRTZ_CRON_TRIGGERS.TRIGGER_NAME \n" +
            "\tAND QRTZ_TRIGGERS.TRIGGER_GROUP = QRTZ_CRON_TRIGGERS.TRIGGER_GROUP")
    Page<JobAndTrigger> getJobAndTiggerByPage(IPage<?> page);
}
