package com.logistics.micro.task.provider.model.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author Yue Wu
 * @date 2021-03-28 14:17:43
 */
@Data
@TableName("QRTZ_LOCKS")
public class LocksPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String schedName;
	/**
	 * 
	 */
	private String lockName;

}
