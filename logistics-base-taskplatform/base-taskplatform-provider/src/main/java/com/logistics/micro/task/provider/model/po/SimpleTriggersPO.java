package com.logistics.micro.task.provider.model.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author Yue Wu
 * @date 2021-03-28 14:17:44
 */
@Data
@TableName("QRTZ_SIMPLE_TRIGGERS")
public class SimpleTriggersPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String schedName;
	/**
	 * 
	 */
	private String triggerName;
	/**
	 * 
	 */
	private String triggerGroup;
	/**
	 * 
	 */
	private Long repeatCount;
	/**
	 * 
	 */
	private Long repeatInterval;
	/**
	 * 
	 */
	private Long timesTriggered;

}
