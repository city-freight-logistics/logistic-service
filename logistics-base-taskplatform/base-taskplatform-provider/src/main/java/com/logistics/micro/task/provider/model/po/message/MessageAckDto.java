package com.logistics.micro.task.provider.model.po.message;

import lombok.Data;

import java.io.Serializable;

/**
 * redis中存放订单消息的状态信息
 *
 * @author xionghw0982
 * @date 2021-04-13 23:31
 */
@Data
public class MessageAckDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 每个消息的传输标签（唯一）
     */
    private Long deliveryTag;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 司机编号
     */
    private String driverCode;
    /**
     * 车牌
     */
    private String carCode;
    /**
     * 订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）
     */
    private Integer orderState;
    /**
     * 该消息的状态 0：未消费  1：消费
     */
    private Integer messageState;

}
