package com.logistics.micro.task.provider.model.po.tempOrder;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户下单但还未接单表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Data
public class TempOrderPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private Integer id;
	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 修改时间
	 */
	private Date modifyDate;
	/**
	 * 是否完成（0：否，1：是）
	 */
	private Integer isDelete;

}
