package com.logistics.micro.task.provider.model.vo;

import lombok.Data;

import java.math.BigInteger;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-28 14:59
 */
@Data
public class JobAndTrigger {
    private String jobName;
    private String jobGroup;
    private String jobClassName;
    private String triggerName;
    private String triggerGroup;
    private BigInteger repeatInterval;
    private BigInteger timesTriggered;
    private String cronExpression;
    private String timeZoneId;
}
