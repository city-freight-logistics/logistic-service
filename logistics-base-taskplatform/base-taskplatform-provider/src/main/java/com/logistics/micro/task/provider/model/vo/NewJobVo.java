package com.logistics.micro.task.provider.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 新建任务Vo类
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-02 16:52
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewJobVo {
    private String jobClassName;
    private String jobGroupName;
    private String cronExpression;
}
