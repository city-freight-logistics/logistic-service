package com.logistics.micro.task.provider.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.task.provider.api.IJobAndTriggerService;
import com.logistics.micro.task.provider.factory.JobFactory;
import com.logistics.micro.task.provider.job.BaseJob;
import com.logistics.micro.task.provider.mapper.JobAndTriggerMapper;
import com.logistics.micro.task.provider.model.vo.JobAndTrigger;
import com.logistics.micro.task.provider.model.vo.NewJobVo;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-28 15:02
 */
@org.apache.dubbo.config.annotation.Service
@Service
@Slf4j
public class JobAndTriggerImpl implements IJobAndTriggerService {

    @Autowired
    private JobAndTriggerMapper jobAndTriggerMapper;
    //加入Qulifier注解，通过名称注入bean
    @Autowired @Qualifier("Scheduler")
    private Scheduler scheduler;

    /**
     * 定时任务分页查询，后续应添加对应的条件筛选
     *
     * @param param
     * @return
     */
    @Override
    public ApiResponse<?> getJobAndTriggerDetailsByPage(Map<String, Object> param) {
        Page<JobAndTrigger> jobAndTiggerByPage = null;
        try {
            log.info("JobAndTriggerImpl --> getJobAndTriggerDetailsByPage() query page start ret={}", param);
            jobAndTiggerByPage = jobAndTriggerMapper.getJobAndTiggerByPage(new Query<>().getPage(param));
            log.info("JobAndTriggerImpl --> getJobAndTriggerDetailsByPage() query result={} ret={}",jobAndTiggerByPage.getRecords(), param);
        } catch (Exception ex) {
            log.error("JobAndTriggerImpl --> getJobAndTriggerDetailsByPage() error {}", ex.getMessage());
            return new ApiResponse<>(HeaderStatus.ERROR, ex.getMessage());
        }
        log.info("JobAndTriggerImpl --> getJobAndTriggerDetailsByPage() success");
        return new ApiResponse<>(new PageUtils(jobAndTiggerByPage));
    }

    /**
     * 添加定时任务方法
     *
     * @param jsonStr NewJobVo类JSON格式
     * @return
     */
    @Override
    public ApiResponse<?> addNewJob(String jsonStr) {
        try {
            log.info("JobAndTriggerImpl --> addNewJob() fromJson ret={}", jsonStr);
            NewJobVo newJobVo = GsonUtils.fromJson(jsonStr, NewJobVo.class);
            log.info("JobAndTriggerImpl --> addNewJob() 启动调度器 ret={}", jsonStr);
            scheduler.start();
            log.info("JobAndTriggerImpl --> addNewJob() 构建job信息 ret={}", jsonStr);
            BaseJob baseJob = (BaseJob) JobFactory.getJobByName(newJobVo.getJobClassName()).newInstance();
            JobDetail jobDetail = JobBuilder.newJob(baseJob.getClass()).withIdentity(newJobVo.getJobClassName(), newJobVo.getJobGroupName()).build();
            log.info("JobAndTriggerImpl --> addNewJob() 表达式调度构建器(即任务执行的时间) ret={}", jsonStr);
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(newJobVo.getCronExpression());
            log.info("JobAndTriggerImpl --> addNewJob() 按新的cronExpression表达式构建一个新的trigger ret={}", jsonStr);//按新的cronExpression表达式构建一个新的trigger
            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(newJobVo.getJobClassName(), newJobVo.getJobGroupName())
                    .withSchedule(scheduleBuilder).build();
            log.info("JobAndTriggerImpl --> addNewJob() 添加任务 ret={}", jsonStr);
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (Exception ex) {
            log.error("JobAndTriggerImpl --> addNewJob() exception={} ret={}", ex.getMessage(), jsonStr);
            return new ApiResponse<>(HeaderStatus.ERROR, ex.getMessage());
        }
        return new ApiResponse<>(HeaderStatus.SUCCESS);
    }

    /**
     * 定时任务暂停方法
     *
     * @param jobClassName Job类名
     * @param jobGroupName Job组名
     * @return
     */
    @Override
    public ApiResponse<?> pauseJob(String jobClassName, String jobGroupName) {

        try {
            log.info("JobAndTriggerImpl --> pauseJob() pauseJob ret= jobClassName: {} , jobGroupName: {}", jobClassName, jobGroupName);
            scheduler.pauseJob(JobKey.jobKey(jobClassName, jobGroupName));
        } catch (SchedulerException ex) {
            log.error("JobAndTriggerImpl --> pauseJob() exception={} ret= jobClassName: {} , jobGroupName: {}", ex.getMessage(), jobClassName, jobGroupName);
            return new ApiResponse<>(HeaderStatus.ERROR, ex.getMessage());
        }
        return new ApiResponse<>(HeaderStatus.ERROR);
    }

    /**
     * 定时任务恢复
     *
     * @param jobClassName Job类名
     * @param jobGroupName Job组名
     * @return
     */
    @Override
    public ApiResponse<?> resumeJob(String jobClassName, String jobGroupName) {

        try {
            log.info("JobAndTriggerImpl --> resumeJob() resumeJob ret= jobClassName: {} , jobGroupName: {}", jobClassName, jobGroupName);
            scheduler.resumeJob(JobKey.jobKey(jobClassName, jobGroupName));
        } catch (SchedulerException ex) {
            log.error("JobAndTriggerImpl --> resumeJob() exception={} ret= jobClassName: {} , jobGroupName: {}", ex.getMessage(), jobClassName, jobGroupName);
            return new ApiResponse<>(HeaderStatus.ERROR, ex.getMessage());
        }
        return new ApiResponse<>(HeaderStatus.SUCCESS);
    }

    /**
     * 定时任务更新方法
     *
     * @param jsonStr NewJobVo类JSON格式
     * @return
     */
    @Override
    public ApiResponse<?> rescheduleJob(String jsonStr) {

        try {
            log.info("JobAndTriggerImpl --> rescheduleJob() start ret={}", jsonStr);
            NewJobVo newJobVo = GsonUtils.fromJson(jsonStr, NewJobVo.class);
            TriggerKey triggerKey = TriggerKey.triggerKey(newJobVo.getJobClassName(), newJobVo.getJobGroupName());
            log.info("JobAndTriggerImpl --> rescheduleJob() 表达式调度构建器 ret={}", jsonStr);
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(newJobVo.getCronExpression());
            CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
            log.info("JobAndTriggerImpl --> rescheduleJob() 按新的trigger重新设置job执行 ret={}", jsonStr);
            scheduler.rescheduleJob(triggerKey, trigger);
        } catch (SchedulerException ex) {
            log.error("JobAndTriggerImpl --> addNewJob() exception={} ret={}", ex.getMessage(), jsonStr);
            return new ApiResponse<>(HeaderStatus.ERROR, ex.getMessage());
        }
        return new ApiResponse<>(HeaderStatus.SUCCESS);
    }

    /**
     * 删除定时任务方法
     *
     * @param jobClassName Job类名
     * @param jobGroupName Job组名
     * @return
     */
    @Override
    public ApiResponse<?> deleteJob(String jobClassName, String jobGroupName) {

        try {
            log.info("JobAndTriggerImpl --> deleteJob() start ret= jobClassName: {} , jobGroupName: {}", jobClassName, jobGroupName);
            scheduler.pauseTrigger(TriggerKey.triggerKey(jobClassName, jobGroupName));
            scheduler.unscheduleJob(TriggerKey.triggerKey(jobClassName, jobGroupName));
            log.info("JobAndTriggerImpl --> deleteJob() deleteJob ret= jobClassName: {} , jobGroupName: {}", jobClassName, jobGroupName);
            scheduler.deleteJob(JobKey.jobKey(jobClassName, jobGroupName));
        } catch (SchedulerException ex) {
            log.error("JobAndTriggerImpl --> addNewJob() exception={} ret= jobClassName: {} , jobGroupName: {}", ex.getMessage(), jobClassName, jobGroupName);
            return new ApiResponse<>(HeaderStatus.ERROR, ex.getMessage());
        }
        return new ApiResponse<>(HeaderStatus.SUCCESS);
    }

}
