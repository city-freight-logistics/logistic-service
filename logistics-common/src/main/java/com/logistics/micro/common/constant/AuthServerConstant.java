package com.logistics.micro.common.constant;

/**
 * redis的名
 *
 * @author ju.huang02@hand-china.com 2021/04/13 13:36
 */
public class AuthServerConstant {

	public static final String SMS_CODE_CACHE_PREFIX = "sms:code:";
	public static final String SMS_CODE_FORGET_CODE="forget:code:";
	public static final String JWT_TOKEN="Jwt_token:";

}
