package com.logistics.micro.common.enums;

import lombok.Getter;

import java.io.Serializable;

/**
 * 异步消息通用枚举类
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-10 18:02
 */
@Getter
public enum AsyncMessageEnum implements Serializable {

    PHONE_MESSAGE_TYPE(9001, "手机短信消息"),
    EMAIL_MESSAGE_TYPE(9002, "邮件消息")
    ;

    private final Integer code;
    private final String decs;

    AsyncMessageEnum(Integer code, String decs) {
        this.code = code;
        this.decs = decs;
    }
}
