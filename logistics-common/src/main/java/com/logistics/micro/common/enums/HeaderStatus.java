package com.logistics.micro.common.enums;

/**
 * 返回头枚举类型，用于进行返回引用
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-25 21:53
 */
public enum HeaderStatus {

	SUCCESS(200, "ok"),
	SUCCESS_IDEMPOTENT(200, "触发接口幂等返回"),
	ERROR(500, "error"),
	EXCPEITON(501, "系统内部异常"),
	PERMISSION_DIED(501, "您没有权限进行访问，请联系管理员"),
	INPUT_DATA_NULL(502, "传入数据为空"),
	DUBBO_SERVER_EXCEPTION(503, "dubbo服务调用异常"),


	/**
	 * 600开头为消息队列相关错误返回头信息
	 */
	MASSAGE_SEND_ERROR(601, "发送消息至消息队列出错"),
	MESSAGE_TYPE_ERROR(602, "请输入正确的消息类型"),
	SMS_CODE_EXCEPTION(603, "验证码频率过高"),

	/**
	 * 700开头为数据处理异常
	 */
	QUERY_EMPTY(701, "操作对象不存在"),
	QUERY_UPDATE_STATUS_EXCEPTION(702, "操作对象前置状态异常"),
	QUERY_TARGET_EXIST(703, "已被占用"),

	/**
	 * 800开头为分页返回提示
	 */
	PAGE_IS_NOT_NUMBER_ERROR(801, "page字段应为数字"),
	PAGE_CONTEXT_NULL_INFO(802, "当前条件无数据哦"),

	/**
	 * 900开头为操作异常通用返回
	 */
	UPDATE_PERMISSION_DEAD(901, "当前状态不支持更改"),
	PASSWORD_ERROR(902, "密码错误请重新输入"),
	NO_USER_PHONE(903, "手机号不存在"),

	/**
	 * 950开头为数据校验相关
	 */
	HANDLE_ALREADY_APPLY(951, "该订单已被申诉")

	;
	private static final long serialVersionUID = 1L;
	private final Integer code;
	private final String msg;

	HeaderStatus(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public Integer getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}
}
