package com.logistics.micro.common.enums;

/**
 * service层错误异常枚举
 *
 * @author xionghw0982
 * @date 2021-04-15 10:50
 */
public enum ServiceExceptionEnum {
	PARAMS_ERROR("参数有误"),

	DATA_ZERO("没有数据"),

	PAGE_ERROR("分页参数异常"),

	INSERT_ERROR("插入异常"),

	UPDATE_ERROR("更新异常"),

	DELETE_ERROR("删除异常"),

	GRABBING_FAIL("来晚一步，订单已经被人先抢了..."),

	DUPLICATE_REJECT("重复拒绝"),

    ORDER_STATUS_ERROR("错误，订单状态不是当前过程的状态"),

    INSERT_DB_ERROR("插入数据库失败"),

	NO_MESSAGE_DATA("消息丢失"),

	INSERT_CACHE_ERROR("插入缓存失败"),

    DUBBO_SEVICE_ERROR("dubbo服务调用异常"),

    NO_ORDER_CACHE_DATA("没有订单号为%s的缓存"),

	NO_LISTENING_DRIVER_DATA("没有正在听单的司机"),

	NO_ERROR_PASSWORD("密码错误"),

	NO_USER_PHONE("手机号未注册");


	private String message;

	ServiceExceptionEnum(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
