package com.logistics.micro.common.exception;

import com.logistics.micro.common.enums.ServiceExceptionEnum;
import lombok.Data;

/**
 * service层产生的异常
 *
 * @author xionghw0982
 * @date 2021-04-15 09:49
 */
@Data
public class ServiceException extends RuntimeException{

    private String message;

    public ServiceException(String message) {
        this.message = message;
    }

    public ServiceException(ServiceExceptionEnum message) {
        this.message = message.getMessage();
    }

    public ServiceException(ServiceExceptionEnum message, String param) {
        this.message = String.format(message.getMessage(),param);
    }

}
