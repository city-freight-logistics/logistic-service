package com.logistics.micro.common.model;

import com.logistics.micro.common.enums.HeaderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 控制器全局返回对象
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-25 21:49
 */
@Data
public class ApiResponse<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Header header;
    private T body;

    public ApiResponse(HeaderStatus headerStatus) {
        this.header = new Header(headerStatus);
    }

    public ApiResponse(T body) {
        this.header = new Header(HeaderStatus.SUCCESS);
        this.body = body;
    }

    public ApiResponse(HeaderStatus headerStatus, T body) {
        this.header = new Header(headerStatus);
        this.body = body;
    }

    public ApiResponse() {
        this.header = new Header(HeaderStatus.ERROR);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
