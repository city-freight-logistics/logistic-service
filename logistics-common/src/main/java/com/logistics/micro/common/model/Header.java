package com.logistics.micro.common.model;

import com.logistics.micro.common.enums.HeaderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 公共返回类头部信息
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-25 21:50
 * 对应枚举类 @link com.logistics.micro.common.enums.HeaderStatus
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Header implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer code;
    private String msg;

    public Header(HeaderStatus headerStatus) {
        this.code = headerStatus.getCode();
        this.msg = headerStatus.getMsg();
    }
}
