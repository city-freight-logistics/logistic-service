package com.logistics.micro.common.model.VO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 短信消息实体 - 消息队列使用
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-07 00:01
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhoneMessageVO {
    /**
     * 缓存中键值
     */
    private String messageKey;
    /**
     * 六位操作验证码
     */
    private String messageCode;
}
