package com.logistics.micro.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 人事系统邮件通用对象
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-17 22:41
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HrMailDTO {
    /**
     * 邮件唯一标识
     */
    private String uuid;
    /**
     * 发送接收对象
     */
    private String sendTo;
    /**
     * 公司名称
     */
    private String org;
    /**
     * 录用者姓名
     */
    private String name;
}
