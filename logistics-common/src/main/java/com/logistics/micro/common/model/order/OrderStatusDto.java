package com.logistics.micro.common.model.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单状态消息传输类
 *
 * @author xionghw0982
 * @date 2021-04-28 14:26
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderStatusDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 订单编号
     */
    private String orderCode;

    /**
     * 订单名称
     */
    private String orderName;

    /**
     * 订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）
     */
    private Integer orderState;

    /**
     * 订单开始日期
     */
    private Date beginDate;

    /**
     * 订单结束日期
     */
    private Date completeDate;

}
