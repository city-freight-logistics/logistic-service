package com.logistics.micro.common.model.rabbit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 消息队发送实体类
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-04-09 17:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RabbitMessage<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    // 消息描述
    private String desc;
    // 消息数据体
    private T body;
    // 消息状态码 ==> 保留字段，特殊业务逻辑可使用
    private Integer code = 200;
}
