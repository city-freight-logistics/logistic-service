package com.logistics.micro.common.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 非spring容器类获取bean工具类
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-15 23:44
 */
@Component
public final class BeanUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext; // Spring应用上下文环境

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        BeanUtils.applicationContext = applicationContext;
    }

    public static <T> T getBean(String beanName) throws BeansException {
        return (T) applicationContext.getBean(beanName);
    }

    public static <T> T getBean(Class<?> beanClass) throws BeansException {
        return (T) applicationContext.getBean(beanClass);
    }
}
