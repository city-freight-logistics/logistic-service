package com.logistics.micro.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * DATE日期工具类，提供日球相关格式转换
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-26 21:42
 */
public class DateUtils {
    private DateUtils() {}

    /** 年-月-日 时:分:秒 显示格式 */
    // 备注:如果使用大写HH标识使用24小时显示格式,如果使用小写hh就表示使用12小时制格式。
    public static String DATE_TO_STRING_DETAIL_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /** 年-月-日 显示格式 */
    public static String DATE_TO_STRING_SHORT_PATTERN = "yyyy-MM-dd";

    private static SimpleDateFormat simpleDateFormat;
    private static final Calendar calendar;

    static {
        calendar = Calendar.getInstance();
    }

    /**
     * Date类型转为指定格式的String类型
     *
     * @param source
     * @param pattern
     * @return
     */
    public static String DateToString(Date source, String pattern) {
        simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(source);
    }

    public static String DateToString(Date date) {
        return DateToString(date, DATE_TO_STRING_SHORT_PATTERN);
    }

    /**
     * 获取相对于当前日期多少天的日期格式化
     * @param param
     * @return
     */
    public static String dataFormatAdd(Integer param) {
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DAY_OF_MONTH, param);
        Date date = instance.getTime();
        return DateToString(date);
    }

    /**
     * unix时间戳转为指定格式的String类型
     *
     * @param source
     * @param format
     * @return
     */
    public static String timeStampToString(long source, String format) {
        simpleDateFormat = new SimpleDateFormat(format);
        Date date = new Date(source);
        return simpleDateFormat.format(date);
    }

    /**
     * unix时间戳转为指定格式的Date类型
     *
     * @param source
     * @return
     */
    public static Date timeStampToDate(long source) {
        return new Date(source);
    }

    /**
     * 将日期转换为时间戳(unix时间戳,单位毫秒)
     *
     * @return
     */
    public static long dateToTimeStamp() {
        return System.currentTimeMillis();
    }

    /**
     * 获取当前年份
     * @return
     */
    public static Integer getCurrentYear() {
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 获取当前月份
     * @return
     */
    public static Integer getCurrentMonth() {
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 获取当前日
     * @return
     */
    public static Integer getCurrentDay() {
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

}
