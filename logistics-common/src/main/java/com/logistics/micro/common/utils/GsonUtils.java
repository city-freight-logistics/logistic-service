package com.logistics.micro.common.utils;

import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.springframework.cglib.beans.BeanMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JSON工具类，用于JSON相关功能处理
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-26 21:19
 */
public class GsonUtils {
    private static Gson gson = null;

    static {
        // 当使用GsonBuilder方式时属性为空的时候输出来的json字符串是有键值key的,显示形式是"key":null，而直接new出来的就没有"key":null的
        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
    }

    private GsonUtils() {}

    /**
     * 转JSON字符串
     *
     * @param src
     * @return
     */
    public static String toJSON(Object src) {
        return gson.toJson(src);
    }

    /**
     * 将JSON字符串转换为Java对象
     *
     * @param json
     * @param classOfT
     * @param <T>
     * @return
     */
    public static <T> T fromJson(String json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }

    /**
     * json字符串转成list
     *
     * @param gsonString
     * @param cls
     * @return
     */
    public static <T> List<T> gsonToList(String gsonString, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        if (gson != null) {

            JsonParser parser = new JsonParser();
            JsonArray jsonarray = parser.parse(gsonString).getAsJsonArray();
            for (JsonElement element : jsonarray) {
                list.add(gson.fromJson(element, cls));
            }
        }
        return list;
    }

    /**
     * 将对象装换为map
     *
     * @param bean
     * @return
     */
    public static <T> Map<String, Object> beanToMap(T bean) {
        HashMap<String, Object> map = Maps.newHashMap();
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                map.put(String.valueOf(key), beanMap.get(key));
            }
        }
        return map;
    }

    /**
     * 将map装换为javabean对象
     *
     * @param map
     * @param bean
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map, T bean) {
        BeanMap beanMap = BeanMap.create(bean);
        beanMap.putAll(map);
        return bean;
    }

    public static Map<String, Object> strToMap(String jsonStr) {
        return new Gson().fromJson(jsonStr, new TypeToken<HashMap<String, Object>>() {}.getType());
    }

    public static String mapToStr(Map<?, ?> map) {
        StringBuilder sb = new StringBuilder().append('{');
        boolean first = true;
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (!first) {
                sb.append(", ");
            }
            first = false;
            sb.append(entry.getKey()).append(':').append(entry.getValue());
        }
        return sb.append('}').toString();
    }
}
