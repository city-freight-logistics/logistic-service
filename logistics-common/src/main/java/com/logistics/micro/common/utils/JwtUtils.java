package com.logistics.micro.common.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/05/06 23:04
 */
public class JwtUtils {

	//token过期时间
	public static final long EXPIRE = 1000 * 60 * 60 * 24 * 365;
	//秘钥
	public static final String APP_SECRET = "logisticsHjWyXhhWxx530ZXCVBNM";

	//生成token字符串的方法
	public static String getJwtToken(Integer id) {

		String JwtToken = Jwts.builder()
				.setHeaderParam("typ", "JWT")
				.setHeaderParam("alg", "HS256")
				.setSubject("logistics-user")
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRE))
				.claim("user_id", id)
				.signWith(SignatureAlgorithm.HS256, APP_SECRET)
				.compact();

		return JwtToken;
	}

	/**
	 * 判断token是否存在与有效
	 *
	 * @param jwtToken
	 * @return
	 */
	public static boolean checkToken(String jwtToken) {
		if (StringUtils.isEmpty(jwtToken)) {
			return false;
		}
		try {
			Jwts.parser().setSigningKey(APP_SECRET).parseClaimsJws(jwtToken);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 根据token获取用户id
	 *
	 * @param jwtToken
	 * @return
	 */
	public static String getMemberIdByJwtToken(String jwtToken) {

		if (StringUtils.isEmpty(jwtToken)) {
			return null;
		}
		Jws<Claims> claimsJws = Jwts.parser().setSigningKey(APP_SECRET).parseClaimsJws(jwtToken);
		Claims claims = claimsJws.getBody();
		Integer id = (Integer) claims.get("user_id");
		String id1 = String.valueOf(id);
		return id1;
	}
}
