package com.logistics.micro.common.xss;

import org.apache.commons.lang.StringUtils;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/26 15:36
 * @description
 */
public class SQLFilter {
    /**
     * SQL注入过滤
     * @param str  待验证的字符串
     */
    public static String sqlInject(String str) {
        if(StringUtils.isBlank(str)){
            return null;
        }
        //去掉'|"|;|\字符
        str = StringUtils.replace(str, "'", "");
        str = StringUtils.replace(str, "\"", "");
        str = StringUtils.replace(str, ";", "");
        str = StringUtils.replace(str, "\\", "");

        //转换成小写
        str = str.toLowerCase();

        //非法字符
        String[] keywords = {"master", "truncate", "insert", "select", "delete", "update", "declare", "alter", "drop"};

        //判断是否包含非法字符
        for(String keyword : keywords){
            if(str.indexOf(keyword) != -1){
                // TODO SQL注入非法字段异常
                // throw new Exception("包含非法字符");
            }
        }

        return str;
    }
}
