package com.logistics.micro.driver.order.provide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/26 16:00
 * @description
 */
@SpringBootApplication
@EnableDiscoveryClient
public class DriverOrderServiceBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(DriverOrderServiceBootstrap.class, args);
    }
}
