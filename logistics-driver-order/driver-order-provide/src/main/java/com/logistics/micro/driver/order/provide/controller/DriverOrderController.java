package com.logistics.micro.driver.order.provide.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.logistics.micro.common.enums.ServiceExceptionEnum;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.driver.order.provide.entity.pojo.DriverOrderPO;
import com.logistics.micro.order.api.DriverOrderService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import com.logistics.micro.common.model.ApiResponse;


/**
 * 司机方订单表
 *
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@RestController
@RequestMapping("provide/driver-order")
public class DriverOrderController {

    @Reference
    DriverOrderService driverOrderService;

    /**
     * 列表
     */
    @GetMapping("/list")
    //@RequiresPermissions("provide:driverorder:list")
    public ApiResponse<Map<String,Object>> list(@RequestParam Map<String, Object> params){
        if (!isNumeric(String.valueOf(params.get("page")))) {
            throw new ServiceException(ServiceExceptionEnum.PAGE_ERROR);
        }
        System.out.println("请求");
        Map<String,Object> page = driverOrderService.queryPage(params);

        return new ApiResponse<>(page);
    }

    @GetMapping("/list-finish")
    public ApiResponse<Map<String,Object>> listFinish(@RequestParam Map<String, Object> params) {
        if (!isNumeric(String.valueOf(params.get("page")))) {
            throw new ServiceException(ServiceExceptionEnum.PAGE_ERROR);
        }
        final Map<String, Object> page = driverOrderService.queryPageFinish(params);
        return new ApiResponse<>(page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    //@RequiresPermissions("provide:driverorder:info")
    public ApiResponse<DriverOrderPO> info(@PathVariable("id") Integer id){
        final ApiResponse apiResponse = driverOrderService.getOne(String.valueOf(id));

        return apiResponse;
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    //@RequiresPermissions("provide:driverorder:save")
    public ApiResponse<String> save(@RequestParam String driverOrder){
        final ApiResponse apiResponse = driverOrderService.addOne(driverOrder);

        return apiResponse;
    }

    @GetMapping("/search/{keyWord}/{driverName}")
    public ApiResponse<List> searchOrder(@PathVariable("keyWord") String keyWord,@PathVariable("driverName") String driverName) {
        return driverOrderService.searchOrder(keyWord,driverName);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@RequiresPermissions("provide:driverorder:update")
    public ApiResponse<String> update(@RequestParam String driverOrder){
        final ApiResponse apiResponse = driverOrderService.updateOne(driverOrder);

        return apiResponse;
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    //@RequiresPermissions("provide:driverorder:delete")
    public ApiResponse<String> delete(@RequestBody Integer[] ids){
		driverOrderService.deleteBatch(Arrays.asList(ids));

        return new ApiResponse<>("删除成功");
    }

    private boolean isNumeric(String s) {
        if (s != null && !"".equals(s.trim())){
            return s.matches("^[0-9]*$");
        }
        return false;

    }

}
