package com.logistics.micro.driver.order.provide.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.driver.order.provide.entity.pojo.DriverOrderDataPO;
import com.logistics.micro.order.api.DriverOrderDataService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.model.ApiResponse;


/**
 * 司机-订单模型表
 *
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@RestController
@RequestMapping("provide/driver-order-data")
public class DriverOrderDataController {
    @Reference
    DriverOrderDataService driverOrderDataService;

    /**
     * 列表
     */
    @GetMapping("/list")
    //@RequiresPermissions("provide:driverorderdata:list")
    public ApiResponse<Map<String,Object>> list(@RequestParam Map<String, Object> params){
        Map<String,Object> page = driverOrderDataService.queryPage(params);

        return new ApiResponse<>(page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    //@RequiresPermissions("provide:driverorderdata:info")
    public ApiResponse<DriverOrderDataPO> info(@PathVariable("id") Integer id){
        final ApiResponse apiResponse = driverOrderDataService.getOne(String.valueOf(id));

        return apiResponse;
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    //@RequiresPermissions("provide:driverorderdata:save")
    public ApiResponse<String> save(@RequestParam DriverOrderDataPO driverOrderData){
        final ApiResponse apiResponse = driverOrderDataService.addOne(GsonUtils.toJSON(driverOrderData));

        return apiResponse;
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@RequiresPermissions("provide:driverorderdata:update")
    public ApiResponse<String> update(@RequestParam DriverOrderDataPO driverOrderData){
        final ApiResponse apiResponse = driverOrderDataService.updateOne(GsonUtils.toJSON(driverOrderData));

        return apiResponse;
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    //@RequiresPermissions("provide:driverorderdata:delete")
    public ApiResponse<String> delete(@RequestParam Integer[] ids){
        final ApiResponse apiResponse = driverOrderDataService.deleteBatch(Arrays.asList(ids));

        return apiResponse;
    }

}
