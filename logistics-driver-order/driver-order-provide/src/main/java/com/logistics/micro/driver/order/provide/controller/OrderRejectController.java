package com.logistics.micro.driver.order.provide.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.driver.order.provide.entity.pojo.OrderRejectPO;
import com.logistics.micro.order.api.OrderRejectService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.model.ApiResponse;


/**
 * 司机拒绝订单表
 *
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@RestController
@RequestMapping("provide/order-reject")
public class OrderRejectController {
    @Reference
    OrderRejectService orderRejectService;

    /**
     * 列表
     */
    @GetMapping("/list")
    //@RequiresPermissions("provide:orderreject:list")
    public ApiResponse<Map<String,Object>> list(@RequestParam Map<String, Object> params){
        Map<String,Object> page = orderRejectService.queryPage(params);

        return new ApiResponse<>(page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    //@RequiresPermissions("provide:orderreject:info")
    public ApiResponse<OrderRejectPO> info(@PathVariable("id") Integer id){
        final ApiResponse apiResponse = orderRejectService.getOne(String.valueOf(id));

        return apiResponse;
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    //@RequiresPermissions("provide:orderreject:save")
    public ApiResponse<String> save(@RequestParam OrderRejectPO orderReject){
        final ApiResponse apiResponse = orderRejectService.addOne(GsonUtils.toJSON(orderReject));

        return apiResponse;
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@RequiresPermissions("provide:orderreject:update")
    public ApiResponse<String> update(@RequestParam OrderRejectPO orderReject){
        final ApiResponse apiResponse = orderRejectService.updateOne(GsonUtils.toJSON(orderReject));

        return apiResponse;
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    //@RequiresPermissions("provide:orderreject:delete")
    public ApiResponse<String> delete(@RequestParam Integer[] ids){
        final ApiResponse apiResponse = orderRejectService.deleteBatch(Arrays.asList(ids));

        return apiResponse;
    }

}
