package com.logistics.micro.driver.order.provide.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.logistics.micro.order.api.TempOrderService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logistics.micro.driver.order.provide.entity.pojo.TempOrderPO;
import com.logistics.micro.common.model.ApiResponse;


/**
 * 用户下单但还未接单表
 *
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@RestController
@RequestMapping("provide/temp-order")
public class TempOrderController {
    @Reference
    private TempOrderService tempOrderService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("provide:temporder:list")
    public ApiResponse<Map<String,Object>> list(@RequestParam Map<String, Object> params){
        Map<String,Object> page = tempOrderService.queryPage(params);

        return new ApiResponse<>(page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("provide:temporder:info")
    public ApiResponse<TempOrderPO> info(@PathVariable("id") String id){
        final ApiResponse one = tempOrderService.getOne(id);

        return one;
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("provide:temporder:save")
    public ApiResponse<String> save(@RequestParam String tempOrder){
		tempOrderService.addOne(tempOrder);

        return new ApiResponse<>("保存成功");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("provide:temporder:update")
    public ApiResponse<String> update(@RequestParam String tempOrder){
		tempOrderService.updateOne(tempOrder);

        return new ApiResponse<>("更新成功");
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("provide:temporder:delete")
    public ApiResponse<String> delete(@RequestParam String[] ids){
		tempOrderService.deleteBatch(Arrays.asList(ids));

        return new ApiResponse<>("删除成功");
    }

}
