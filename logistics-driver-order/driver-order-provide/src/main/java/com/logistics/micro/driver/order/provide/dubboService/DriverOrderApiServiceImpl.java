package com.logistics.micro.driver.order.provide.dubboService;


import lombok.extern.slf4j.Slf4j;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/26 20:32
 * @description
 */
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class DriverOrderApiServiceImpl {
}
