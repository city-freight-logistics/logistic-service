package com.logistics.micro.driver.order.provide.entity.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 司机-订单模型表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Data
public class DriverOrderDataPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private Integer id;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 车牌号
	 */
	private String carCode;
	/**
	 * 接驾里程
	 */
	private Double mileage;
	/**
	 * 起点到终点的距离[0,1)
	 */
	private Integer distanceOrder01;
	/**
	 * 起点到终点的距离[1,3)
	 */
	private Integer distanceOrder13;
	/**
	 * 起点到终点的距离[3,5)
	 */
	private Integer distanceOrder35;
	/**
	 * 起点到终点的距离[5,10)
	 */
	private Integer distanceOrder510;
	/**
	 * 起点到终点的距离[10,+)
	 */
	private Integer distanceOrder10More;
	/**
	 * 订单与司机的距离[0,0.5)
	 */
	private Integer distanceDriver005;
	/**
	 * 订单与司机的距离[0.5,2)
	 */
	private Integer distanceDriver052;
	/**
	 * 订单与司机的距离[2,5)
	 */
	private Integer distanceDriver25;
	/**
	 * 订单与司机的距离[5,10)
	 */
	private Integer distanceDriver510;
	/**
	 * 订单与司机的距离[10,+)
	 */
	private Integer distanceDriver10More;
	/**
	 * 历史接单数
	 */
	private Integer countOrder;
	/**
	 * 历史接单率
	 */
	private Double historyRate;
	/**
	 * 运输的货物类型
	 */
	private Integer goodsType;
	/**
	 * 创建时间
	 */
	private Date createData;
	/**
	 * 修改时间
	 */
	private Date modifyDate;

}
