package com.logistics.micro.driver.order.provide.entity.pojo;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 司机拒绝订单表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Data
public class OrderRejectPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private Integer id;
	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 订单名称
	 */
	private String orderName;
	/**
	 * 司机姓名
	 */
	private String driverName;
	/**
	 * 拒绝原因
	 */
	private String reason;
	/**
	 * 拒绝时间
	 */
	private Date createDate;

}
