package com.logistics.mircro.driver.publish.api;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.mircro.driver.publish.entity.OrderPO;

import java.util.Map;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/27 21:32
 * @description
 */
public interface DriverPublishApiService {
    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询司机运力发布
     */
    Map<String,Object> queryPage(Map<String, Object> params);

    /**
     * 通过订单编号获取司机找单具体信息
     * @param orderCode
     * @return
     */
    ApiResponse<?> getOne(String orderCode);
    /**
     * 通过id获取发布订单信息
     * @return
     */
    ApiResponse<?> getOneById(Integer id);
    /**
     * 司机运力订单状态更改
     * @param id
     * @param state
     * @return
     */
    ApiResponse updateOrderStateById(Integer id,Integer state);
}
