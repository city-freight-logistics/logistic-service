package com.logistics.mircro.driver.publish.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 司机找单表
 * 
 * @author xionghw
 * @date 2021-03-27 21:06:13
 */
@Data
@TableName("PMS_ORDER")
public class OrderPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;

	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 司机姓名
	 */
	private String driverName;
	/**
	 * 车牌号
	 */
	private String carCode;
	/**
	 * 找单标题
	 */
	private String orderTitle;
	/**
	 * 期望回报
	 */
	private String exception;
	/**
	 * 司机电话
	 */
	private String driverTel;
	/**
	 * 找单状态（0：正在找单，1：结束找单，2：已取消   3：已被接单）
	 */
	private Integer state;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifyDate;
	/**
	 * 逻辑删除（0：否，1：是）
	 */
	@TableLogic
	private Integer isDalete;

}
