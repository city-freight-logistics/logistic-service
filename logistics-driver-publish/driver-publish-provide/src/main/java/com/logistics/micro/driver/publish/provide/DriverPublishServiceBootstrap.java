package com.logistics.micro.driver.publish.provide;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/27 21:26
 * @description
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.logistics.micro.driver.publish.provide.dao")
public class DriverPublishServiceBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(DriverPublishServiceBootstrap.class,args);
    }
}
