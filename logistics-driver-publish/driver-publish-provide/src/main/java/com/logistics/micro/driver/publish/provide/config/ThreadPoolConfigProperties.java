package com.logistics.micro.driver.publish.provide.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Controller
 *
 * @author xionghw0982
 * @date 2021-04-07 00:38
 */

@ConfigurationProperties(prefix = "publish-order.thread")
@Component
@Data
public class ThreadPoolConfigProperties {
    private Integer core;
    private Integer maxSize;
    private Integer keepAliveTime;
}
