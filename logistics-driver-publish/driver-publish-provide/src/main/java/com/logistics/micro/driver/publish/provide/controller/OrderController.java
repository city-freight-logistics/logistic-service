package com.logistics.micro.driver.publish.provide.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.mircro.driver.vehicle.api.VehicledubboService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.logistics.micro.driver.publish.provide.entity.pojo.OrderPO;
import com.logistics.micro.driver.publish.provide.service.OrderService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.model.ApiResponse;


/**
 * 司机找单表
 *
 * @author xionghw
 * @date 2021-03-27 21:06:13
 */
@RestController
@RequestMapping("provide/publish-order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * 列表
     */
    @GetMapping("/list")
    //@RequiresPermissions("provide:order:list")
    public ApiResponse<Map<String,Object>> listForDriver(@RequestParam Map<String, Object> params){
        Map<String,Object> page = orderService.queryPageForDriver(params);

        return new ApiResponse<>(page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    //@RequiresPermissions("provide:order:info")
    public ApiResponse<OrderPO> info(@PathVariable("id") Integer id){
		OrderPO order = orderService.getById(id);

        return new ApiResponse<>(order);
    }

    @GetMapping("list-vehicle")
    public ApiResponse<Map<String,Object>> listVehicle(@RequestParam Map<String, Object> params){
        final Map<String, Object> map = orderService.listVehicle(params);
        return new ApiResponse<>(map);
    }

    @GetMapping("/infoVehicle/{driverCode}")
    //@RequiresPermissions("provide:order:info")
    public ApiResponse<List> infoVehicle(@PathVariable("driverCode") String driverCode){
        return orderService.infoVehicle(driverCode);
    }

    /**
     * 获取已完成的找单信息
     * @param params
     * @return
     */
    @GetMapping("/list-finished")
    public ApiResponse<PageUtils> listFinished(@RequestParam Map<String, Object> params) {
        final PageUtils pageUtils = orderService.queryPageForDriverFinished(params);
        return new ApiResponse<>(pageUtils);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    //@RequiresPermissions("provide:order:save")
    public ApiResponse<String> save(@RequestParam String order){
        final OrderPO orderPO = GsonUtils.fromJson(order, OrderPO.class);
        orderService.saveOne(orderPO);

        return new ApiResponse<>("保存成功");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@RequiresPermissions("provide:order:update")
    public ApiResponse<String> update(@RequestParam String order){
        final OrderPO orderPO = GsonUtils.fromJson(order, OrderPO.class);
        orderService.updateOne(orderPO);

        return new ApiResponse<>("更新成功");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    //@RequiresPermissions("provide:order:delete")
    public ApiResponse<String> delete(@RequestBody Integer id){
		orderService.deleteOrder(id);

        return new ApiResponse<>("删除成功");
    }

}
