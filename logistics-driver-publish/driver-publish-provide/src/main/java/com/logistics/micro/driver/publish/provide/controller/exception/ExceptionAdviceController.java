package com.logistics.micro.driver.publish.provide.controller.exception;

import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.common.model.ApiResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/30 22:46
 * @description
 */
@RestControllerAdvice
public class ExceptionAdviceController {


    /**
     * 捕捉数据缺失异常
     * @param ex
     * @return
     */
    @ExceptionHandler(ServiceException.class)
    public ApiResponse<String> serviceException(ServiceException ex) {
        return new ApiResponse<>(HeaderStatus.ERROR,ex.getMessage());
    }

    /**
     * 捕捉其他所有异常
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ApiResponse<String> globalException(HttpServletRequest request, Throwable ex) {
        return new ApiResponse<>(HeaderStatus.ERROR,ex.getMessage());
    }
}
