package com.logistics.micro.driver.publish.provide.dao;

import com.logistics.micro.driver.publish.provide.entity.pojo.OrderPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 司机找单表
 * 
 * @author xionghw
 * @date 2021-03-27 21:06:13
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderPO> {
	
}
