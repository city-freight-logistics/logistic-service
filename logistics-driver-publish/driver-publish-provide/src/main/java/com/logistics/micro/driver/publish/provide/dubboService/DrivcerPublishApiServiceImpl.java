package com.logistics.micro.driver.publish.provide.dubboService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.driver.publish.provide.dao.OrderDao;
import com.logistics.micro.driver.publish.provide.entity.pojo.OrderPO;
import com.logistics.mircro.driver.publish.api.DriverPublishApiService;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/27 21:32
 * @description
 */
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class DrivcerPublishApiServiceImpl extends ServiceImpl<OrderDao, OrderPO> implements DriverPublishApiService {
    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询司机运力发布
     * @param params
     */
    @Override
    public Map<String, Object> queryPage(Map<String, Object> params) {
        final QueryWrapper<OrderPO> condition = new QueryWrapper<>();
        condition.eq("state",0);
        IPage<OrderPO> page = this.page(
                new Query<OrderPO>().getPage(params),
                condition
        );
        final Map<String, Object> map = GsonUtils.beanToMap(new PageUtils(page));
        return map;
    }

    /**
     * 通过id获取司机找单具体信息
     *
     * @param orderCode
     * @return
     */
    @Override
    public ApiResponse<?> getOne(String orderCode) {
        final QueryWrapper<OrderPO> condition = new QueryWrapper<>();
        condition.eq("order_code",orderCode);
        final OrderPO orderPO = this.getOne(condition);
        return new ApiResponse<OrderPO>(orderPO);
    }

    @Override
    public ApiResponse<?> getOneById(Integer id) {
        OrderPO orderPO = this.getById(id);
        return new ApiResponse<>(orderPO);
    }

    /**
     * 司机运力订单状态更改
     * @param id
     * @param state
     * @return
     */
    @Override
    public ApiResponse updateOrderStateById(Integer id,Integer state) {
        OrderPO po = new OrderPO();
        po.setId(id);
        po.setState(state);
        boolean b = this.updateById(po);
        return new  ApiResponse(b);
    }
}
