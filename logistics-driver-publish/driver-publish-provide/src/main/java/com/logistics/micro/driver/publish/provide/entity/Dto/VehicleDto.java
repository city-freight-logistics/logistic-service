package com.logistics.micro.driver.publish.provide.entity.Dto;

import lombok.Data;

import java.util.Date;

/**
 * Controller
 *
 * @author xionghw0982
 * @date 2021-04-06 23:29
 */
@Data
public class VehicleDto {
    /**
     *
     */
    private Integer id;
    /**
     * 司机编号
     */
    private String driverCode;
    /**
     * 司机姓名
     */
    private String driverName;
    /**
     * 车牌
     */
    private String carCode;
    /**
     * 车款
     */
    private String carName;
    /**
     * 车辆类型
     */
    private String carType;
    /**
     * 司机的车的图片
     */
    private String carUrl;
    /**
     * 载重量
     */
    private Double loadCapacity;
    /**
     * 容量体积
     */
    private Double volumeCapacity;
    /**
     * 车辆状态(整型 0：禁用 1：锁定  2：审核中  3：正在使用 4：空闲)
     */
    private Integer state;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date modifyDate;
    /**
     * 逻辑删除（0：否，1：是）
     */
    private Integer isDelete;
}
