package com.logistics.micro.driver.publish.provide.entity.vo;

import lombok.Data;

import java.util.Date;

/**
 * Controller
 *
 * @author xionghw0982
 * @date 2021-04-06 23:23
 */
@Data
public class OrderVO {
    /**
     *
     */
    private Integer id;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 司机编号
     */
    private String driverCode;
    /**
     * 司机姓名
     */
    private String driverName;
    /**
     * 车牌号
     */
    private String carCode;
    /**
     * 找单标题
     */
    private String orderTitle;
    /**
     * 期望回报
     */
    private String exception;
    /**
     * 找单状态（0：正在找单，1：结束找单，2：已取消   3：已被接单）
     */
    private Integer state;
    /**
     * 司机电话
     */
    private String driverTel;
    /**
     * 订单发起人/发货人姓名
     */
    private String customerName;
    /**
     * 发起人联系电话
     */
    private String customerTel;
    /**
     * 收货人姓名
     */
    private String consigneeName;
    /**
     * 收货人联系电话
     */
    private String consigneeTel;
    /**
     * 货物分类名称
     */
    private String typeName;
    /**
     * 备注
     */
    private String orderRemarks;
    /**
     * 司机的车的图片
     */
    private String carUrl;
    /**
     * 载重量
     */
    private Double loadCapacity;
    /**
     * 容量体积
     */
    private Double volumeCapacity;
    /**
     * 车辆状态(整型 0：禁用 1：锁定  2：审核中  3：正在使用 4：空闲)
     */
    private Integer stateVehicle;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date modifyDate;
    /**
     * 逻辑删除（0：否，1：是）
     */
    private Integer isDalete;
}
