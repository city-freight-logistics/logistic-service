package com.logistics.micro.driver.publish.provide.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.driver.publish.provide.entity.pojo.OrderPO;

import java.util.List;
import java.util.Map;

/**
 * 司机找单表
 *
 * @author xionghw
 * @date 2021-03-27 21:06:13
 */
public interface OrderService extends IService<OrderPO> {
    /**
     * 分页查询
     * @param params
     * @return
     */
    Map<String,Object> queryPageForDriver(Map<String, Object> params);

    /**
     * 获取司机已经结束的找单信息
     * @param params
     * @return
     */
    PageUtils queryPageForDriverFinished(Map<String, Object> params);



    /**
     * dubbo服务查询车辆信息
     * @param params
     * @return
     */
    Map<String, Object> listVehicle(Map<String, Object> params);

    /**
     * dubbo 服务通过司机姓名查询车辆信息
     * @param driverCode
     * @return
     */
    ApiResponse<List> infoVehicle(String driverCode);

    /**
     * 保存
     * @param orderPO
     */
    void saveOne(OrderPO orderPO);

    /**
     * 更新
     * @param orderPO
     */
    void updateOne(OrderPO orderPO);

    /**
     * 删除
     * @param id
     */
    void deleteOrder(Integer id);
}

