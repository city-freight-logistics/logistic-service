package com.logistics.micro.driver.publish.provide.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.enums.ServiceExceptionEnum;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.order.OrderStatusDto;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.CodeUtils;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.driver.publish.provide.entity.vo.OrderVO;
import com.logistics.micro.driver.publish.provide.utils.RedisUtil;
import com.logistics.mircro.driver.vehicle.api.VehicledubboService;
import lombok.extern.slf4j.Slf4j;
import onwer.find.api.OwnerFindService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.logistics.micro.driver.publish.provide.dao.OrderDao;
import com.logistics.micro.driver.publish.provide.entity.pojo.OrderPO;
import com.logistics.micro.driver.publish.provide.service.OrderService;

import javax.annotation.Resource;


@Service("orderService")
@Slf4j
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderPO> implements OrderService {

    @Reference
    VehicledubboService vehicledubboService;
    @Reference
    OwnerFindService ownerFindService;
    @Reference
    com.logistics.micro.message.api.OrderService orderService;

    @Autowired
    ThreadPoolExecutor executor;
    @Resource
    private RedisUtil redisUtil;

    private final String PUBLISH_DRIVER_CACHE_KEY = "publish-page-driver";
    /**
     * 分页查询
     * 因为指定了查询的司机姓名，也就是说只会有一个用户在一个时间内查询
     * @param params
     * @return
     */
    @Override
    public Map<String,Object> queryPageForDriver(Map<String, Object> params) {
        log.info("普通未接单查询");
        String itemKey = String.valueOf(params.get("page"));
        if (!isNumeric(itemKey)) {
            throw new ServiceException(ServiceExceptionEnum.PAGE_ERROR);
        }
        final String driverCode = String.valueOf(params.get("driverCode"));

//        // 先查缓存
//        final String beanMap = String.valueOf(redisUtil.hget(PUBLISH_DRIVER_CACHE_KEY + "-" + params.get("state") + "-" + driverCode,
//                itemKey));
//        if ("null".equals(beanMap)){
//            Map<String, Object> map = getPageFromDB(params);
//            final String totalCount = map.get("totalCount").toString();
//            if (Integer.parseInt(totalCount) == 0) {
//                throw new ServiceException(ServiceExceptionEnum.DATA_ZERO);
//            }
//            // 查完了后放入redis
//            redisUtil.hset(PUBLISH_DRIVER_CACHE_KEY + "-" + params.get("state") + "-" + driverCode,
//                    itemKey, GsonUtils.toJSON(map),5*60);
//            return map;
//        }
        Map<String, Object> map = getPageFromDB(params);
//        final String totalCount = map.get("totalCount").toString();
        log.info("命中，直接返回");
//        final Map<String, Object> map = GsonUtils.strToMap(beanMap);
        return map;
    }

    /**
     * 获取司机已经结束的找单信息
     *
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPageForDriverFinished(Map<String, Object> params) {
        log.info("获取已被接单的运力订单信息");
        String page = String.valueOf(params.get("page"));
        if (!isNumeric(page)) {
            throw new ServiceException(ServiceExceptionEnum.PAGE_ERROR);
        }
        final String driverCode = String.valueOf(params.get("driverCode"));
        final Integer state = Integer.parseInt((String) params.get("state"));
        final QueryWrapper<OrderPO> condition = new QueryWrapper<>();
        if (!"null".equals(driverCode)) {
            condition.eq("driver_code",driverCode);
        }
        condition.eq("state",state);
        IPage<OrderPO> pageList = this.page(
                new Query<OrderPO>().getPage(params,"create_date",false),
                condition
        );
        final List<OrderPO> records = pageList.getRecords();
        if (records.size() == 0) {
            throw new ServiceException(ServiceExceptionEnum.DATA_ZERO);
        }
        final List<OrderVO> orderVORecords = new ArrayList<>(records.size());
        for(OrderPO orderPO : records) {
            final String orderInfo = ownerFindService.getOrderInfo(orderPO.getOrderCode());
            final OrderVO orderVO = GsonUtils.fromJson(orderInfo, OrderVO.class);
            BeanUtils.copyProperties(orderPO,orderVO);
            orderVORecords.add(orderVO);
        }
        final PageUtils pageUtils = new PageUtils(pageList);
        pageUtils.setList(orderVORecords);
        return pageUtils;
    }

    private Map<String, Object> getPageFromDB(Map<String, Object> params) {
        // 没有命中，查db
        log.info("没有命中，查db");
        Map<String,Object> map = new HashMap<>();
        final String driverCode = String.valueOf(params.get("driverCode"));
        // 先查运力发布表
        CompletableFuture<IPage<OrderPO>> publishFuture = CompletableFuture.supplyAsync(() -> {
            final QueryWrapper<OrderPO> condition = new QueryWrapper<>();
            if (!"null".equals(driverCode)) {
                condition.eq("driver_code",driverCode);
            }
            condition.eq("state",Integer.parseInt((String) params.get("state")));
            IPage<OrderPO> page = this.page(
                    new Query<OrderPO>().getPage(params,"create_date",false),
                    condition
            );
            return page;
        }, executor);
        // 同时可以请求dubbo服务
        CompletableFuture<List<HashMap<String,Object>>> vehicleFuture = CompletableFuture.supplyAsync(() -> {
            final ApiResponse<List> response = this.infoVehicle(driverCode);
            final List<HashMap<String,Object>> list = response.getBody();
            return list;
        }, executor);

        // 两个任务都执行完了最后执行这个任务
        final CompletableFuture<Void> complete = publishFuture.thenAcceptBothAsync(vehicleFuture, (f1, f2) -> {
            final List<OrderPO> records = f1.getRecords();
            List<OrderVO> pageVO = new ArrayList<>(records.size());
            for (OrderPO order: records) {
                final OrderVO orderVO = new OrderVO();
                orderVO.setId(order.getId());
                orderVO.setDriverCode(order.getDriverCode());
                orderVO.setDriverName(order.getDriverName());
                orderVO.setCarCode(order.getCarCode());
                orderVO.setOrderTitle(order.getOrderTitle());
                orderVO.setException(order.getException());
                orderVO.setState(order.getState());
                orderVO.setDriverTel(order.getDriverTel());
                orderVO.setCreateDate(order.getCreateDate());
                orderVO.setModifyDate(order.getModifyDate());
                orderVO.setIsDalete(order.getIsDalete());

                for (HashMap<String,Object> v : f2) {
                    if (v.get("carCode").equals(order.getCarCode())){
                        orderVO.setCarUrl(String.valueOf(v.get("carUrl")));
                        orderVO.setVolumeCapacity(Double.parseDouble(String.valueOf(v.get("volumeCapacity"))));
                        orderVO.setLoadCapacity(Double.parseDouble(String.valueOf(v.get("loadCapacity"))));
                        orderVO.setStateVehicle(Integer.parseInt(String.valueOf(v.get("state"))));
                    }
                }

                pageVO.add(orderVO);
            }
            map.put("list",pageVO);
            map.put("totalCount",f1.getTotal());
            map.put("pageSize",f1.getSize());
            map.put("currPage",f1.getCurrent());
            map.put("totalPage",f1.getPages());
        });
        try {
            CompletableFuture.allOf(publishFuture,vehicleFuture,complete).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * dubbo服务查询车辆信息
     * @param params
     * @return
     */
    @Override
    public Map<String, Object> listVehicle(Map<String, Object> params) {
        final Map<String, Object> map = vehicledubboService.queryPage(params);
        return map;
    }

    /**
     * dubbo 服务通过司机编号查询车辆信息
     *
     * @param driverCode
     * @return
     */
    @Override
    public ApiResponse<List> infoVehicle(String driverCode) {
        final ApiResponse list = vehicledubboService.getListByDriverName(driverCode);
        return list;
    }

    /**
     * 保存
     *
     * @param orderPO
     */
    @Override
    public void saveOne(OrderPO orderPO) {
        final Serializable uuid = CodeUtils.uuid(8);
        orderPO.setOrderCode(uuid.toString());
        final boolean save = this.save(orderPO);
        if (!save) {
            throw new ServiceException(ServiceExceptionEnum.INSERT_ERROR);
        }

    }

    /**
     * 更新
     *
     * @param orderPO
     */
    @Override
    public void updateOne(OrderPO orderPO) {
        final boolean update = this.updateById(orderPO);
        if (!update) {
            throw new ServiceException(ServiceExceptionEnum.UPDATE_ERROR);
        }
        if (orderPO.getState() == 1) {
            final OrderPO one = this.getById(orderPO.getId());
            final OrderStatusDto statusDto = new OrderStatusDto();
            statusDto.setOrderCode(one.getOrderCode());
            statusDto.setOrderName(one.getOrderTitle());
            statusDto.setOrderState(4);
            final RabbitMessage<OrderStatusDto> orderMessage = new RabbitMessage<>();
            orderMessage.setDesc("订单编号为" + statusDto.getOrderCode() + "的订单状态信息");
            orderMessage.setBody(statusDto);
            final ApiResponse<?> messageRes = orderService.providerSendOrderStatus(orderMessage);
            if (messageRes.getHeader().equals(HeaderStatus.MASSAGE_SEND_ERROR)) {
                log.info("消息丢失： {}",orderMessage.toString());
                throw new ServiceException(ServiceExceptionEnum.NO_MESSAGE_DATA);
            }
            log.info("发送订单状态消息到消息队列");
        }
        //删除缓存
        redisUtil.del(PUBLISH_DRIVER_CACHE_KEY + "-" + orderPO.getState() + "-" + orderPO.getDriverCode());
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void deleteOrder(Integer id) {
        final OrderPO orderPO = this.getById(id);
        if (orderPO.getState() == 3) {
            throw new ServiceException("该运力发布的订单已被人接单了，不能删除");
        }
        final boolean remove = this.removeById(id);
        if (!remove) {
            throw new ServiceException(ServiceExceptionEnum.DELETE_ERROR);
        }
        //删除缓存
        redisUtil.del(PUBLISH_DRIVER_CACHE_KEY + "-" + orderPO.getState() + "-" + orderPO.getDriverCode());
    }

    private boolean isNumeric(String s) {
        if (s != null && !"".equals(s.trim())){
            return s.matches("^[0-9]*$");
        }
        return false;

    }

}