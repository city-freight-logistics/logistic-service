package com.logistics.micro.driver.transport.provide;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/29 13:59
 * @description
 */
@SpringBootApplication
@MapperScan("com.logistics.micro.driver.transport.provide.dao")
public class DriverTransportServiceBootsrap {
    public static void main(String[] args) {
        SpringApplication.run(DriverTransportServiceBootsrap.class,args);
    }
}
