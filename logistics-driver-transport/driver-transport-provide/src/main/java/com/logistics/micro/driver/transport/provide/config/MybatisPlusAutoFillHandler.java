package com.logistics.micro.driver.transport.provide.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/26 16:28
 * @description
 */
@Component
public class MybatisPlusAutoFillHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createDate", () -> new Date(), Date.class); // 起始版本 3.3.3(推荐)
        this.strictInsertFill(metaObject, "modifyDate", () -> new Date(), Date.class); // 起始版本 3.3.3(推荐)
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "modifyDate", () -> new Date(), Date.class); // 起始版本 3.3.3(推荐)
    }
}
