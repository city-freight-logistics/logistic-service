package com.logistics.micro.driver.transport.provide.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.driver.transport.provide.entity.po.DriveLogPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.logistics.micro.driver.transport.provide.service.DriveLogService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.enums.HeaderStatus;



/**
 * 司机开车的日志
 *
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
@RestController
@RequestMapping("provide/driverlog")
public class DriveLogController {
    @Autowired
    private DriveLogService driveLogService;

    /**
     * 列表
     */
    @GetMapping("/list")
    //@RequiresPermissions("provide:drivelog:list")
    public ApiResponse<PageUtils> list(@RequestParam Map<String, Object> params){
        PageUtils page = driveLogService.queryPage(params);

        return new ApiResponse<>(page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    //@RequiresPermissions("provide:drivelog:info")
    public ApiResponse<DriveLogPO> info(@PathVariable("id") Integer id){
		DriveLogPO driveLog = driveLogService.getById(id);

        return new ApiResponse<>(driveLog);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    //@RequiresPermissions("provide:drivelog:save")
    public ApiResponse<String> save(@RequestParam String driveLog){
        final DriveLogPO driveLogPO = GsonUtils.fromJson(driveLog, DriveLogPO.class);
        final boolean save = driveLogService.save(driveLogPO);
        if (save){
            return new ApiResponse<>("保存成功");
        }

        return new ApiResponse<>(HeaderStatus.ERROR,"保存失败");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@RequiresPermissions("provide:drivelog:update")
    public ApiResponse<String> update(@RequestParam String driveLog){
        final DriveLogPO driveLogPO = GsonUtils.fromJson(driveLog, DriveLogPO.class);
        final boolean update = driveLogService.updateById(driveLogPO);
        if (update){
            return new ApiResponse<>("更新成功");
        }

        return new ApiResponse<>(HeaderStatus.ERROR,"更新失败");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    //@RequiresPermissions("provide:drivelog:delete")
    public ApiResponse<String> delete(@RequestBody Integer[] ids){
		driveLogService.removeByIds(Arrays.asList(ids));

        return new ApiResponse<>("删除成功");
    }

}
