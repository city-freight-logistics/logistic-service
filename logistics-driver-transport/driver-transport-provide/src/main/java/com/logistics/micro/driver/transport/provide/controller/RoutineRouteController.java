package com.logistics.micro.driver.transport.provide.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.driver.transport.provide.entity.po.RoutineRoutePO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.logistics.micro.driver.transport.provide.service.RoutineRouteService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.enums.HeaderStatus;



/**
 * 司机常用路线
 *
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
@RestController
@RequestMapping("provide/routineroute")
public class RoutineRouteController {
    @Autowired
    private RoutineRouteService routineRouteService;

    /**
     * 列表
     */
    @GetMapping("/list")
    //@RequiresPermissions("provide:routineroute:list")
    public ApiResponse<PageUtils> list(@RequestParam Map<String, Object> params){
        PageUtils page = routineRouteService.queryPage(params);

        return new ApiResponse<>(page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    //@RequiresPermissions("provide:routineroute:info")
    public ApiResponse<RoutineRoutePO> info(@PathVariable("id") Integer id){
		RoutineRoutePO routineRoute = routineRouteService.getById(id);

        return new ApiResponse<>(routineRoute);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    //@RequiresPermissions("provide:routineroute:save")
    public ApiResponse<String> save(@RequestParam String routineRoute){
        final RoutineRoutePO routineRoutePO = GsonUtils.fromJson(routineRoute, RoutineRoutePO.class);
        final boolean save = routineRouteService.save(routineRoutePO);
        if (save){
            return new ApiResponse<>("保存成功");
        }

        return new ApiResponse<>(HeaderStatus.ERROR,"保存失败");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@RequiresPermissions("provide:routineroute:update")
    public ApiResponse<String> update(@RequestParam String routineRoute){
        final RoutineRoutePO routineRoutePO = GsonUtils.fromJson(routineRoute, RoutineRoutePO.class);
        final boolean update = routineRouteService.updateById(routineRoutePO);
        if (update){
            return new ApiResponse<>("更新成功");
        }

        return new ApiResponse<>(HeaderStatus.ERROR,"更新失败");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    //@RequiresPermissions("provide:routineroute:delete")
    public ApiResponse<String> delete(@RequestBody Integer[] ids){
		routineRouteService.removeByIds(Arrays.asList(ids));

        return new ApiResponse<>("删除成功");
    }

}
