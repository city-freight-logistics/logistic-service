package com.logistics.micro.driver.transport.provide.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.driver.transport.provide.entity.po.TransprotPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.logistics.micro.driver.transport.provide.service.TransportService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.enums.HeaderStatus;



/**
 * 司机运输表
 *
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
@RestController
@RequestMapping("provide/transport")
public class TransprotController {
    @Autowired
    private TransportService transportService;

    /**
     * 列表
     */
    @GetMapping("/list")
    //@RequiresPermissions("provide:transprot:list")
    public ApiResponse<PageUtils> list(@RequestParam Map<String, Object> params){
        PageUtils page = transportService.queryPage(params);

        return new ApiResponse<>(page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    //@RequiresPermissions("provide:transprot:info")
    public ApiResponse<TransprotPO> info(@PathVariable("id") Integer id){
		TransprotPO transprot = transportService.getById(id);

        return new ApiResponse<>(transprot);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    //@RequiresPermissions("provide:transprot:save")
    public ApiResponse<String> save(@RequestParam String transprot){
        final boolean save = transportService.saveOne(transprot);
        if (save){
            return new ApiResponse<>("保存成功");
        }

        return new ApiResponse<>(HeaderStatus.ERROR,"保存失败");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@RequiresPermissions("provide:transprot:update")
    public ApiResponse<String> update(@RequestParam String transprot){
        final boolean update = transportService.updateOne(transprot);
        if (update){
            return new ApiResponse<>("更新成功");
        }

        return new ApiResponse<>(HeaderStatus.ERROR,"更新失败");
    }

    @PostMapping("/updateLocation")
    public ApiResponse<String> updateLoaction(@RequestParam String driverLocation) {
        final Boolean location = transportService.updateDriverLocation(driverLocation);
        if (location){
            return new ApiResponse<>("更新成功");
        }

        return new ApiResponse<>(HeaderStatus.ERROR,"更新失败");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    //@RequiresPermissions("provide:transprot:delete")
    public ApiResponse<String> delete(@RequestBody Integer[] ids){
        transportService.removeByIds(Arrays.asList(ids));

        return new ApiResponse<>("删除成功");
    }

}
