package com.logistics.micro.driver.transport.provide.dao;

import com.logistics.micro.driver.transport.provide.entity.po.DriveLogPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 司机开车的日志
 * 
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
@Mapper
public interface DriveLogDao extends BaseMapper<DriveLogPO> {
	
}
