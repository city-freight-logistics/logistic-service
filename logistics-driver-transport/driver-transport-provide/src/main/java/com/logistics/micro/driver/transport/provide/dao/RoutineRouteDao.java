package com.logistics.micro.driver.transport.provide.dao;

import com.logistics.micro.driver.transport.provide.entity.po.RoutineRoutePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 司机常用路线
 * 
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
@Mapper
public interface RoutineRouteDao extends BaseMapper<RoutineRoutePO> {
	
}
