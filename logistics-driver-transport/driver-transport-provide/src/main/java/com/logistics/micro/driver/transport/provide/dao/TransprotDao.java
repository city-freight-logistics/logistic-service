package com.logistics.micro.driver.transport.provide.dao;

import com.logistics.micro.driver.transport.provide.entity.po.TransprotPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 司机运输表
 * 
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
@Mapper
public interface TransprotDao extends BaseMapper<TransprotPO> {
	
}
