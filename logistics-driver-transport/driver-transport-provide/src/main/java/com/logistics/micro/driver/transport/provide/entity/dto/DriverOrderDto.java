package com.logistics.micro.driver.transport.provide.entity.dto;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 司机方订单表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DriverOrderDto implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）
	 */
	private Integer orderState;

	/**
	 * 订单结束日期
	 */
	private Date completeDate;


}
