package com.logistics.micro.driver.transport.provide.entity.dto;

import lombok.Data;

import java.util.Date;

/**
 * redis中存放的订单信息
 *
 * @author xionghw0982
 * @date 2021-04-14 20:20
 */
@Data
public class OrderDto {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private Integer id;

    /**
     * 订单编号
     */
    private String orderCode;

    /**
     * 订单名称
     */
    private String orderName;
    /**
     * 货物分类名称
     */
    private String typeName;
    /**
     * 订单发起人（发货人）
     */
    private String customerName;

    /**
     * 位置纬度
     */
    private  String latitude;

    /**
     * 位置经度
     */
    private  String longitude;

    /**
     * 收获与发货距离
     */
    private  String orderDistance;
    /**
     * 订单到司机的距离
     */
    private String distanceDriver;
    /**
     * 发起人联系电话
     */
    private String customerTel;

    /**
     * 收货人
     */
    private String consigneeName;

    /**
     * 收货人联系电话
     */
    private String consigneeTel;

    /**
     * 用户身份
     */
    private String customerType;
    /**
     * 支付方式【1->支付宝；2->微信】
     */
    private Integer payType;

    /**
     * 订单执行人
     */
    private String driverName;
    /**
     * 车牌
     */
    private String carCode;

    /**
     * 订单开始日期
     */
    private Date beginDate;

    /**
     * 订单结束日期
     */
    private Date completeDate;

    /**
     * 订单备注
     */
    private String orderRemarks;

    /**
     * 订单价格
     */
    private Double freightAmount;

    /**
     * 优惠券
     */
    private String coupon;

    /**
     * 订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）
     */
    private Integer orderState;

}
