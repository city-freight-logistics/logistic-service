package com.logistics.micro.driver.transport.provide.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 司机开车的日志
 * 
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
@Data
@TableName("TMS_DRIVE_LOG")
public class DriveLogPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 车牌号
	 */
	private String carCode;
	/**
	 * 起始地点
	 */
	private String startPlace;
	/**
	 * 终点地点
	 */
	private String endPlace;
	/**
	 * 备注
	 */
	private String memo;
	/**
	 * 创建日期
	 */
	private Date createDate;
	/**
	 * 修改日期
	 */
	private Date modifyDate;

}
