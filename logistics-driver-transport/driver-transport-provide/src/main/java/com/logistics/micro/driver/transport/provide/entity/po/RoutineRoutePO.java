package com.logistics.micro.driver.transport.provide.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 司机常用路线
 * 
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
@Data
@TableName("TMS_ROUTINE_ROUTE")
public class RoutineRoutePO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 司机姓名
	 */
	private String driverName;
	/**
	 * 路线（地点名以，隔开）
	 */
	private String route;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 修改时间
	 */
	private Date modifyDate;

}
