package com.logistics.micro.driver.transport.provide.entity.po;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 司机运输表
 * 
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
@Data
@TableName("TMS_TRANSPROT")
public class TransprotPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 运输司机姓名
	 */
	private String driverName;
	/**
	 * 客户姓名
	 */
	private String customerName;
	/**
	 * 开始运输前图片地址
	 */
	private String beginPic;
	/**
	 * 结束运输后图片地址
	 */
	private String completePic;
	/**
	 * 订单状态（2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束）
	 */
	private Integer orderState;
	/**
	 * 创建日期
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 修改日期
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifyDate;

}
