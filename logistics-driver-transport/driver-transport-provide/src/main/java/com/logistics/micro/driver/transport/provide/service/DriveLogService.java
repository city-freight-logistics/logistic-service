package com.logistics.micro.driver.transport.provide.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.driver.transport.provide.entity.po.DriveLogPO;

import java.util.Map;

/**
 * 司机开车的日志
 *
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
public interface DriveLogService extends IService<DriveLogPO> {

    PageUtils queryPage(Map<String, Object> params);
}

