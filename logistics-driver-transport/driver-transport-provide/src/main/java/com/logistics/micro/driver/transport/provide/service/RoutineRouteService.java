package com.logistics.micro.driver.transport.provide.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.driver.transport.provide.entity.po.RoutineRoutePO;

import java.util.Map;

/**
 * 司机常用路线
 *
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
public interface RoutineRouteService extends IService<RoutineRoutePO> {

    PageUtils queryPage(Map<String, Object> params);
}

