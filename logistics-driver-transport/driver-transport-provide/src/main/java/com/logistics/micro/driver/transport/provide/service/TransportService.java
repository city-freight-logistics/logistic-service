package com.logistics.micro.driver.transport.provide.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.driver.transport.provide.entity.po.TransprotPO;

import java.util.Map;

/**
 * 司机运输表
 *
 * @author xionghw
 * @date 2021-03-29 13:53:01
 */
public interface TransportService extends IService<TransprotPO> {

    /**
     * 分页查询
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存
     * @param transprotPO
     * @return
     */
    Boolean saveOne(String transprotPO);

    /**
     * 更新
     * @param transprotPO
     * @return
     */
    Boolean updateOne(String transprotPO);

    /**
     * 更新司机的位置信息
     */
    Boolean updateDriverLocation(String drvierInfo);
}

