package com.logistics.micro.driver.transport.provide.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;

import com.logistics.micro.driver.transport.provide.dao.DriveLogDao;
import com.logistics.micro.driver.transport.provide.entity.po.DriveLogPO;
import com.logistics.micro.driver.transport.provide.service.DriveLogService;


@Service("driveLogService")
public class DriveLogServiceImpl extends ServiceImpl<DriveLogDao, DriveLogPO> implements DriveLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DriveLogPO> page = this.page(
                new Query<DriveLogPO>().getPage(params),
                new QueryWrapper<DriveLogPO>()
        );

        return new PageUtils(page);
    }

}