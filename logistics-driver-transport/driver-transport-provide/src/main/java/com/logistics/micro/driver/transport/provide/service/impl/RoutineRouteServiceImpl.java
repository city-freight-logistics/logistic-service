package com.logistics.micro.driver.transport.provide.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;

import com.logistics.micro.driver.transport.provide.dao.RoutineRouteDao;
import com.logistics.micro.driver.transport.provide.entity.po.RoutineRoutePO;
import com.logistics.micro.driver.transport.provide.service.RoutineRouteService;


@Service("routineRouteService")
public class RoutineRouteServiceImpl extends ServiceImpl<RoutineRouteDao, RoutineRoutePO> implements RoutineRouteService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RoutineRoutePO> page = this.page(
                new Query<RoutineRoutePO>().getPage(params),
                new QueryWrapper<RoutineRoutePO>()
        );

        return new PageUtils(page);
    }

}