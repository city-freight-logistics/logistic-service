package com.logistics.micro.driver.transport.provide.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.enums.ServiceExceptionEnum;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.order.OrderStatusDto;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.driver.transport.provide.entity.dto.DriverLactionDto;
import com.logistics.micro.driver.transport.provide.entity.dto.DriverOrderDto;
import com.logistics.micro.driver.transport.provide.entity.dto.OrderDto;
import com.logistics.micro.driver.transport.provide.enums.CachekeyEnum;
import com.logistics.micro.driver.transport.provide.utils.RedisUtil;
import com.logistics.micro.message.api.OrderService;
import com.logistics.micro.order.api.DriverOrderService;
import com.logistics.micro.payment.api.PaymentDubboService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;

import com.logistics.micro.driver.transport.provide.dao.TransprotDao;
import com.logistics.micro.driver.transport.provide.entity.po.TransprotPO;
import com.logistics.micro.driver.transport.provide.service.TransportService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service("transprotService")
@Slf4j
public class TransportServiceImpl extends ServiceImpl<TransprotDao, TransprotPO> implements TransportService {
    @Resource
    RedisUtil redisUtil;
    @Reference
    DriverOrderService driverOrderService;
    @Reference
    OrderService orderService;
    @Reference
    PaymentDubboService paymentDubboService;

    /**
     * 分页查询
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TransprotPO> page = this.page(
                new Query<TransprotPO>().getPage(params),
                new QueryWrapper<TransprotPO>()
        );

        return new PageUtils(page);
    }
    /**
     * 保存
     * @param transprotPO
     * @return
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public Boolean saveOne(String transprotPO) {
        final TransprotPO transprotPO1 = GsonUtils.fromJson(transprotPO, TransprotPO.class);
        log.info("接受到的订单状态为{}",transprotPO1.getOrderState());
        final String orderTempStr = String.valueOf(redisUtil.hget(CachekeyEnum.ORDER_TEMP_CACHE_KEY.getKey(),
                transprotPO1.getOrderCode()));
        final OrderDto orderDto = GsonUtils.fromJson(orderTempStr, OrderDto.class);
        log.info("从缓存中拿到的订单状态为{}",orderDto.getOrderState());
        // 如果当前订单的状态不是2：已处理
        if (orderDto.getOrderState() != 2) {
            log.info("当前订单状态{} 异常",orderDto.getOrderState());
            throw new ServiceException(ServiceExceptionEnum.ORDER_STATUS_ERROR);
        }
        orderDto.setOrderState(3);
        final boolean save = this.save(transprotPO1);
        if (!save){
            log.info("保存司机运输表异常");
            throw new ServiceException(ServiceExceptionEnum.INSERT_DB_ERROR);
        }
        final Boolean response = driverOrderService.updateOrderState(orderDto.getOrderCode(), 3);
        if (!response) {
            log.info("司机开始运输时，更新状态信息dubbo调用异常");
            throw new ServiceException(ServiceExceptionEnum.DUBBO_SEVICE_ERROR);
        }
        redisUtil.hset(CachekeyEnum.ORDER_TEMP_CACHE_KEY.getKey(), transprotPO1.getOrderCode(),GsonUtils.toJSON(orderDto));

        final OrderStatusDto statusDto = new OrderStatusDto();
        statusDto.setOrderCode(orderDto.getOrderCode());
        statusDto.setOrderName(orderDto.getOrderName());
        statusDto.setOrderState(3);
        final RabbitMessage<OrderStatusDto> orderMessage = new RabbitMessage<>();
        orderMessage.setDesc("订单编号为" + statusDto.getOrderCode() + "的订单状态信息");
        orderMessage.setBody(statusDto);
        final ApiResponse<?> messageRes = orderService.providerSendOrderStatus(orderMessage);
        if (messageRes.getHeader().equals(HeaderStatus.MASSAGE_SEND_ERROR)) {
            log.info("消息丢失： {}",orderMessage.toString());
            throw new ServiceException(ServiceExceptionEnum.NO_MESSAGE_DATA);
        }
        log.info("发送订单状态消息到消息队列");
        return true;
    }

    /**
     * 运输完更新
     *
     * @param transprotPO
     * @return
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public Boolean updateOne(String transprotPO) {
        final TransprotPO transprotPO1 = GsonUtils.fromJson(transprotPO, TransprotPO.class);
        log.info("接受到的订单状态为{}",transprotPO1.getOrderState());
        final String orderTempStr = String.valueOf(redisUtil.hget(CachekeyEnum.ORDER_TEMP_CACHE_KEY.getKey(),
                transprotPO1.getOrderCode()));
        final OrderDto orderDto = GsonUtils.fromJson(orderTempStr, OrderDto.class);
        log.info("从缓存中拿到的订单状态为{}",orderDto.getOrderState());
        // 如果当前订单的状态不是3：运输中
        if (orderDto.getOrderState() != 3) {
            log.info("当前订单状态{} 异常",orderDto.getOrderState());
            throw new ServiceException(ServiceExceptionEnum.ORDER_STATUS_ERROR);
        }
        orderDto.setOrderState(4);

        final boolean update = this.update(Wrappers.<TransprotPO>lambdaUpdate()
                .set(TransprotPO::getCompletePic, transprotPO1.getCompletePic())
                .set(TransprotPO::getOrderState, orderDto.getOrderState())
                .eq(TransprotPO::getOrderCode, transprotPO1.getOrderCode()));
        log.info("更改司机运输表中的信息");
        if (!update) {
            log.info("更新司机运输表异常");
            throw new ServiceException(ServiceExceptionEnum.UPDATE_ERROR);
        }

        redisUtil.hset(CachekeyEnum.ORDER_TEMP_CACHE_KEY.getKey(), transprotPO1.getOrderCode(),GsonUtils.toJSON(orderDto));
        final DriverOrderDto driverOrderDto = new DriverOrderDto();
        driverOrderDto.setCompleteDate(new Date());
        driverOrderDto.setOrderCode(transprotPO1.getOrderCode());
        driverOrderDto.setOrderState(4);
        final ApiResponse response = driverOrderService.updateComplete(GsonUtils.toJSON(driverOrderDto));
        if (response.getHeader().equals(HeaderStatus.ERROR)) {
            log.info("司机完成时，更新状态信息dubbo调用异常");
            throw new ServiceException(ServiceExceptionEnum.DUBBO_SEVICE_ERROR);
        }
        log.info("更改司机订单中的信息");

        final OrderStatusDto statusDto = new OrderStatusDto();
        statusDto.setOrderCode(orderDto.getOrderCode());
        statusDto.setOrderName(orderDto.getOrderName());
        statusDto.setOrderState(4);
        statusDto.setCompleteDate(driverOrderDto.getCompleteDate());
        final RabbitMessage<OrderStatusDto> orderMessage = new RabbitMessage<>();
        orderMessage.setDesc("订单编号为" + statusDto.getOrderCode() + "的订单状态信息");
        orderMessage.setBody(statusDto);
        final ApiResponse<?> messageRes = orderService.providerSendOrderStatus(orderMessage);
        if (messageRes.getHeader().equals(HeaderStatus.MASSAGE_SEND_ERROR)) {
            log.info("消息丢失： {}",orderMessage.toString());
            throw new ServiceException(ServiceExceptionEnum.NO_MESSAGE_DATA);
        }
        log.info("发送订单状态消息到消息队列");

//        final ApiResponse byDriver = paymentDubboService.getByDriver(GsonUtils.toJSON(orderDto));
//        log.info("支付服务的结果{}",byDriver.getBody().toString());
//        if (!byDriver.getBody().toString().equals("收款成功")) {
//            throw new ServiceException("司机收款异常");
//        }
        log.info("司机收款完成");
        return update;
    }

    /**
     * 更新司机的位置信息
     *
     * @param drvierInfo
     */
    @Override
    public Boolean updateDriverLocation(String drvierInfo) {
        final DriverLactionDto lactionDto = GsonUtils.fromJson(drvierInfo, DriverLactionDto.class);
        // 获取听单状态的司机信息
        log.info("获取听单状态的司机信息");
        final String hget = String.valueOf(redisUtil.hget(CachekeyEnum.DRIVER_LITENING_CACHE_KEY.getKey(), lactionDto.getDriverCode()));
        final DriverLactionDto driverLactionCache = GsonUtils.fromJson(hget, DriverLactionDto.class);
        // 修改位置信息
        log.info("修改位置信息");
        driverLactionCache.setLatitude(lactionDto.getLatitude());
        driverLactionCache.setLongitude(lactionDto.getLongitude());
        // 更新缓存
        log.info("更新缓存");
        final boolean hset = redisUtil.hset(CachekeyEnum.DRIVER_LITENING_CACHE_KEY.getKey(),
                lactionDto.getDriverCode(),
                GsonUtils.toJSON(driverLactionCache));
        if (!hset) {
            throw new ServiceException(ServiceExceptionEnum.INSERT_CACHE_ERROR);
        }
        return true;
    }

}