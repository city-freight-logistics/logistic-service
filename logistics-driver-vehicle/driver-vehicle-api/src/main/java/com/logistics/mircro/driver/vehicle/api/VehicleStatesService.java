package com.logistics.mircro.driver.vehicle.api;

import com.logistics.micro.common.model.ApiResponse;

import java.util.Map;

/**
 * 管理端车辆状态接口
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-26 23:26
 */
public interface VehicleStatesService {

    /**
     * 司机车辆审核状态回调接口<br>
     * 幂等接口
     * @param driverStateDto
     * @return
     */
    ApiResponse<?> vehicleExamine(String driverStateDto, Map<String, Object> map);

    /**
     * 司机车辆禁用状态回调接口<br>
     * 幂等接口
     * @param driverStateDto
     * @return
     */
    ApiResponse<?> vehicleDisable(String driverStateDto, Map<String, Object> map);

    /**
     * 司机车辆解封状态回调接口<br>
     * 幂等接口
     * @param driverStateDto
     * @return
     */
    ApiResponse<?> vehicleUnseal(String driverStateDto, Map<String, Object> map);

    /***
     * @Parms 分页查询条件 - 绕过缓存获取
     * @return
     * @Description: 分页查询司机车辆信息
     */
    Map<String,Object> queryPage(Map<String, Object> params);
}
