package com.logistics.mircro.driver.vehicle.api;

import com.logistics.micro.common.model.ApiResponse;

import java.util.List;
import java.util.Map;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/4/2 10:46
 * @description
 */
public interface VehicledubboService {
    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询司机车辆信息
     */
    Map<String,Object> queryPage(Map<String, Object> params);

    /***
     * @Parms 司机车辆信息传输对象
     * @return 司机车辆信息对象
     * @Description: 获取单个司机车辆信息
     */
    ApiResponse getListByDriverName(String driverCode);

    ApiResponse getCarInfoByCarCode(String carCode);

    Map<String,Object> getListByDriverNames(List<String> names);

    /**
     * 查询司机名下车辆的数量
     * @param driverName
     * @return
     */
    Integer getCountByDriverName (String driverName);
}
