package com.logistics.micro.driver.vehicle.provide;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/29 14:24
 * @description
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.logistics.micro.driver.vehicle.provide.dao")
public class DriverVehicleServiceBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(DriverVehicleServiceBootstrap.class,args);
    }
}
