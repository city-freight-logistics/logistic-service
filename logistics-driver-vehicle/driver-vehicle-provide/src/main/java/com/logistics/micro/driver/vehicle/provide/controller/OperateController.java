package com.logistics.micro.driver.vehicle.provide.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.logistics.micro.common.utils.GsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.logistics.micro.driver.vehicle.provide.entity.pojo.OperatePO;
import com.logistics.micro.driver.vehicle.provide.service.OperateService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.enums.HeaderStatus;



/**
 * 车辆信息操作表
 *
 * @author xionghw
 * @date 2021-03-29 14:20:48
 */
@RestController
@RequestMapping("provide/operate")
public class OperateController {
    @Autowired
    private OperateService operateService;

    /**
     * 列表
     */
    @GetMapping("/list")
    //@RequiresPermissions("provide:operate:list")
    public ApiResponse<PageUtils> list(@RequestParam Map<String, Object> params){
        PageUtils page = operateService.queryPage(params);

        return new ApiResponse<>(page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    //@RequiresPermissions("provide:operate:info")
    public ApiResponse<OperatePO> info(@PathVariable("id") Integer id){
		OperatePO operate = operateService.getById(id);

        return new ApiResponse<>(operate);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    //@RequiresPermissions("provide:operate:save")
    public ApiResponse<String> save(@RequestParam String operate){
        final OperatePO operatePO = GsonUtils.fromJson(operate, OperatePO.class);
        final boolean save = operateService.save(operatePO);
        if (save){
            return new ApiResponse<>("保存成功");
        }

        return new ApiResponse<>(HeaderStatus.ERROR,"保存失败");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@RequiresPermissions("provide:operate:update")
    public ApiResponse<String> update(@RequestParam String operate){
        final OperatePO operatePO = GsonUtils.fromJson(operate, OperatePO.class);
        final boolean update = operateService.updateById(operatePO);
        if (update){
            return new ApiResponse<>("更新成功");
        }

        return new ApiResponse<>(HeaderStatus.ERROR,"更新失败");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    //@RequiresPermissions("provide:operate:delete")
    public ApiResponse<String> delete(@RequestBody Integer[] ids){
		operateService.removeByIds(Arrays.asList(ids));

        return new ApiResponse<>("删除成功");
    }

}
