package com.logistics.micro.driver.vehicle.provide.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.logistics.micro.common.utils.GsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.logistics.micro.driver.vehicle.provide.entity.pojo.VehiclePO;
import com.logistics.micro.driver.vehicle.provide.service.VehicleService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.enums.HeaderStatus;



/**
 * 车辆信息表
 *
 * @author xionghw
 * @date 2021-03-29 14:20:48
 */
@RestController
@RequestMapping("provide/vehicle")
public class VehicleController {
    @Autowired
    private VehicleService vehicleService;

    /**
     * 列表,查询所有
     */
    @GetMapping("/list")
    //@RequiresPermissions("provide:vehicle:list")
    public ApiResponse<PageUtils> list(@RequestParam Map<String, Object> params){
        PageUtils page = vehicleService.queryPage(params);

        return new ApiResponse<>(page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    //@RequiresPermissions("provide:vehicle:info")
    public ApiResponse<VehiclePO> info(@PathVariable("id") Integer id){
		VehiclePO vehicle = vehicleService.getById(id);

        return new ApiResponse<>(vehicle);
    }

    @GetMapping("/single/{carCode}")
    public ApiResponse<VehiclePO> singleInfo(@PathVariable("carCode") String carCode){
        final VehiclePO vehiclePO = vehicleService.singleInfo(carCode);
        return new ApiResponse<>(vehiclePO);
    }

    @GetMapping("/infoCarType")
    public ApiResponse<?> infoCarType() {
        return vehicleService.infoCarType();
    }
    /**
     * 保存
     */
    @PostMapping("/save")
    //@RequiresPermissions("provide:vehicle:save")
    public ApiResponse<String> save(@RequestParam String vehicle){
        final VehiclePO vehiclePO = GsonUtils.fromJson(vehicle, VehiclePO.class);
        vehicleService.saveOne(vehiclePO);

        return new ApiResponse<>("保存成功");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@RequiresPermissions("provide:vehicle:update")
    public ApiResponse<String> update(@RequestParam String vehicle){
        final VehiclePO vehiclePO = GsonUtils.fromJson(vehicle, VehiclePO.class);
        vehicleService.updateOne(vehiclePO);

        return new ApiResponse<>("更新成功");
    }

    @PostMapping("/updateState")
    public ApiResponse<String> updateState(@RequestParam String vehicle) {
        final VehiclePO vehiclePO = GsonUtils.fromJson(vehicle, VehiclePO.class);
        vehicleService.updateState(vehiclePO);

        return new ApiResponse<>("更新成功");
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    //@RequiresPermissions("provide:vehicle:delete")
    public ApiResponse<String> delete(@RequestBody Integer[] ids){
		vehicleService.removeByIds(Arrays.asList(ids));

        return new ApiResponse<>("删除成功");
    }

}
