package com.logistics.micro.driver.vehicle.provide.dao;

import com.logistics.micro.driver.vehicle.provide.entity.pojo.OperatePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 车辆信息操作表
 * 
 * @author xionghw
 * @date 2021-03-29 14:20:48
 */
@Mapper
public interface OperateDao extends BaseMapper<OperatePO> {
	
}
