package com.logistics.micro.driver.vehicle.provide.dao;

import com.logistics.micro.driver.vehicle.provide.entity.pojo.VehiclePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 车辆信息表
 * 
 * @author xionghw
 * @date 2021-03-29 14:20:48
 */
@Mapper
public interface VehicleDao extends BaseMapper<VehiclePO> {
	
}
