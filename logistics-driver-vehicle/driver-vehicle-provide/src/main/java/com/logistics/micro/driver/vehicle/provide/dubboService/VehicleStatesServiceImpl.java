package com.logistics.micro.driver.vehicle.provide.dubboService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.driver.vehicle.provide.entity.dto.DriverStateDto;
import com.logistics.micro.driver.vehicle.provide.entity.pojo.OperatePO;
import com.logistics.micro.driver.vehicle.provide.entity.pojo.VehiclePO;
import com.logistics.micro.driver.vehicle.provide.service.OperateService;
import com.logistics.micro.driver.vehicle.provide.service.VehicleService;
import com.logistics.micro.driver.vehicle.provide.service.VehicleStatisticsService;
import com.logistics.micro.driver.vehicle.provide.utils.RedisUtil;
import com.logistics.micro.user.api.UserAdminService;
import com.logistics.mircro.driver.vehicle.api.VehicleStatesService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Objects;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-26 23:38
 */
@Service
@Slf4j
public class VehicleStatesServiceImpl implements VehicleStatesService {

    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private OperateService operateService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private VehicleStatisticsService statisticsService;
    @Reference
    private UserAdminService userAdminService;

    @Override
    public ApiResponse<?> vehicleExamine(String driverStateDto, Map<String, Object> map) {
        try {
            DriverStateDto driverState = GsonUtils.fromJson(driverStateDto, DriverStateDto.class);
            VehiclePO vehiclePO = vehicleService.getOne(new QueryWrapper<VehiclePO>().eq("id", driverState.getId()));
            if (Objects.isNull(vehiclePO)) {
                return new ApiResponse<>(HeaderStatus.QUERY_EMPTY);
            }
            if (vehiclePO.getState().equals(driverState.getState())) {
                return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
            }
            boolean update = vehicleService.update(new UpdateWrapper<VehiclePO>()
                    .set("state", driverState.getState())
                    .set("reject_reason", driverState.getRejectReason())
                    .eq("id", driverState.getId())
                    .eq("state", 2));
            if (update) {
                operateService.save(OperatePO.builder()
                        .carCode(vehiclePO.getCarCode())
                        .operateMan(3)
                        .operateType(4)
                        .operateContent("车辆审核，审核原因：" + driverState.getRejectReason())
                        .build());
                if (driverState.getState() == 4) {
                    // 同步司机详情表
                    Integer up = userAdminService.setVehicleNumByDriverCode(statisticsService.getVehicleByDriverCode(vehiclePO.getDriverCode()), vehiclePO.getDriverCode());
                    //TODO 定时重试机制
//                    if (! (up > 0)) {
//
//                    }
                }
                clearCache(map);
                return new ApiResponse<>(HeaderStatus.SUCCESS);
            } else {
                return new ApiResponse<>(HeaderStatus.QUERY_UPDATE_STATUS_EXCEPTION);
            }
        } catch (Exception e) {
            log.error("VehicleStatesServiceImpl -> vehicleExamine() ret={} exception={}", driverStateDto, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public ApiResponse<?> vehicleDisable(String driverStateDto, Map<String, Object> map) {
        try {
            DriverStateDto driverState = GsonUtils.fromJson(driverStateDto, DriverStateDto.class);
            VehiclePO vehiclePO = vehicleService.getOne(new QueryWrapper<VehiclePO>().eq("id", driverState.getId()));
            if (Objects.isNull(vehiclePO)) {
                return new ApiResponse<>(HeaderStatus.QUERY_EMPTY);
            }
            if (vehiclePO.getState().equals(driverState.getState())) {
                return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
            }
            boolean update = vehicleService.update(new UpdateWrapper<VehiclePO>()
                    .set("state", driverState.getState())
                    .eq("id", driverState.getId()));
            if (update) {
                operateService.save(OperatePO.builder()
                        .carCode(vehiclePO.getCarCode())
                        .operateMan(3)
                        .operateType(4)
                        .operateContent("车辆禁用")
                        .build());
                clearCache(map);
                return new ApiResponse<>(HeaderStatus.SUCCESS);
            } else {
                return new ApiResponse<>(HeaderStatus.QUERY_UPDATE_STATUS_EXCEPTION);
            }
        } catch (Exception e) {
            log.error("VehicleStatesServiceImpl -> vehicleDisable() ret={} exception={}", driverStateDto, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public ApiResponse<?> vehicleUnseal(String driverStateDto, Map<String, Object> map) {
        try {
            DriverStateDto driverState = GsonUtils.fromJson(driverStateDto, DriverStateDto.class);
            VehiclePO vehiclePO = vehicleService.getOne(new QueryWrapper<VehiclePO>().eq("id", driverState.getId()));
            if (Objects.isNull(vehiclePO)) {
                return new ApiResponse<>(HeaderStatus.QUERY_EMPTY);
            }
            if (vehiclePO.getState().equals(driverState.getState())) {
                return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
            }
            boolean update = vehicleService.update(new UpdateWrapper<VehiclePO>()
                    .set("state", driverState.getState())
                    .eq("id", driverState.getId())
                    .eq("state", 0));
            if (update) {
                operateService.save(OperatePO.builder()
                        .carCode(vehiclePO.getCarCode())
                        .operateMan(3)
                        .operateType(4)
                        .operateContent("车辆解除禁用")
                        .build());
                clearCache(map);
                return new ApiResponse<>(HeaderStatus.SUCCESS);
            } else {
                return new ApiResponse<>(HeaderStatus.QUERY_UPDATE_STATUS_EXCEPTION);
            }
        } catch (Exception e) {
            log.error("VehicleStatesServiceImpl -> vehicleDisable() ret={} exception={}", driverStateDto, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public Map<String, Object> queryPage(Map<String, Object> params) {
        clearCache(params);
        PageUtils page = vehicleService.queryPage(params);
        final Map<String, Object> map = GsonUtils.beanToMap(page);
        return map;
    }

    private void clearCache(Map<String, Object> map) {
        // 清除缓存
        StringBuilder key = new StringBuilder();
        key.append("vehicle-");
        for (String s : map.keySet()) {
            key.append(map.get(s));
        }
        redisUtil.del(key.toString());
    }
}
