package com.logistics.micro.driver.vehicle.provide.dubboService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.driver.vehicle.provide.entity.pojo.VehiclePO;
import com.logistics.micro.driver.vehicle.provide.service.VehicleService;
import com.logistics.mircro.driver.vehicle.api.VehicledubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/4/2 10:46
 * @description
 */
@org.apache.dubbo.config.annotation.Service
public class VehicledubboServiceImpl implements VehicledubboService {

    @Autowired
    VehicleService vehicleService;
    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询司机车辆信息
     * @param params
     */
    @Override
    public Map<String, Object> queryPage(Map<String, Object> params) {
        PageUtils page = vehicleService.queryPage(params);
        final Map<String, Object> map = GsonUtils.beanToMap(page);
        return map;
    }

    /***
     * @Parms 司机车辆信息传输对象
     * @return 司机车辆信息对象
     * @Description: 获取单个司机车辆信息
     * @param driverCode
     */
    @Override
    public ApiResponse<List<VehiclePO>> getListByDriverName(String driverCode) {
        final List<VehiclePO> list = vehicleService.list(Wrappers.<VehiclePO>lambdaQuery().eq(VehiclePO::getDriverCode, driverCode));
        return new ApiResponse<List<VehiclePO>>(list);
    }

    @Override
    public ApiResponse getCarInfoByCarCode(String carCode) {
        VehiclePO car= vehicleService.getOne(new QueryWrapper<VehiclePO>().eq("car_code", carCode));
        return new ApiResponse<VehiclePO>(car);
    }

    @Override
    public Map<String, Object> getListByDriverNames(List<String> names) {
        return null;
    }

    /**
     * 查询司机名下车辆的数量
     *
     * @param driverName
     * @return
     */
    @Override
    public Integer getCountByDriverName(String driverName) {
        final QueryWrapper<VehiclePO> condition = new QueryWrapper<>();
        condition.eq("driver_name",driverName);
        final Integer count = vehicleService.count(condition);
        return count;
    }


}
