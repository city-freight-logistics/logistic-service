package com.logistics.micro.driver.vehicle.provide.entity.dto;


import lombok.Data;

/**
 * Controller
 *
 * @author xionghw0982
 * @date 2021-04-18 21:34
 */
@Data
public class DriverOrderDataDto {
    /**
     *
     */
    private Integer id;
    /**
     * 司机编号
     */
    private String driverCode;
    /**
     * 车牌号
     */
    private String carCode;
}
