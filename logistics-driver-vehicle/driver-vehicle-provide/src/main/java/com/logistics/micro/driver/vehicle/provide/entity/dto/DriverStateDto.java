package com.logistics.micro.driver.vehicle.provide.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-26 23:30
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DriverStateDto {
    private Integer id;
    private String driverCode;
    private String carCode;
    private Integer state;
    private String rejectReason;
}
