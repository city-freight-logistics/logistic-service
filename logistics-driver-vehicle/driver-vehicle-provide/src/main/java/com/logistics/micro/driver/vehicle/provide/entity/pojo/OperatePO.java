package com.logistics.micro.driver.vehicle.provide.entity.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

/**
 * 车辆信息操作表
 * 
 * @author xionghw
 * @date 2021-03-29 14:20:48
 */
@Data
@Builder
@TableName("VMS_OPERATE")
public class OperatePO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 车牌号
	 */
	private String carCode;
	/**
	 * 操作人[1：用户；2：系统；3：后台管理员]
	 */
	private Integer operateMan;
	/**
	 * 1：增加；2：删除；3：查询；4：修改
	 */
	private Integer operateType;
	/**
	 * 操作内容
	 */
	private String operateContent;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifyDate;

}
