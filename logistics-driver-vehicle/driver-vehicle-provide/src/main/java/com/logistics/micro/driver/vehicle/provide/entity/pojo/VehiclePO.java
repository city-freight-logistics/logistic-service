package com.logistics.micro.driver.vehicle.provide.entity.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

/**
 * 车辆信息表
 * 
 * @author xionghw
 * @date 2021-03-29 14:20:48
 */
@Data
@Builder
@TableName("VMS_VEHICLE")
public class VehiclePO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 司机姓名
	 */
	private String driverName;
	/**
	 * 车牌
	 */
	private String carCode;
	/**
	 * 车款
	 */
	private String carName;
	/**
	 * 车辆类型
	 */
	private String carType;
	/**
	 * 司机的车的图片
	 */
	private String carUrl;
	/**
	 * 载重量
	 */
	private Double loadCapacity;
	/**
	 * 容量体积
	 */
	private Double volumeCapacity;
	/**
	 * 车辆状态(整型 0：禁用 1：锁定  2：审核中  3：正在使用 4：空闲  5：驳回)
	 */
	private Integer state;

	/**
	 * 拒绝原因
	 */
	private String rejectReason;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifyDate;
	/**
	 * 逻辑删除（0：否，1：是）
	 */
	@TableLogic
	private Integer isDelete;

}
