package com.logistics.micro.driver.vehicle.provide.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.driver.vehicle.provide.entity.pojo.OperatePO;

import java.util.Map;

/**
 * 车辆信息操作表
 *
 * @author xionghw
 * @date 2021-03-29 14:20:48
 */
public interface OperateService extends IService<OperatePO> {

    PageUtils queryPage(Map<String, Object> params);
}

