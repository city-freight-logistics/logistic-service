package com.logistics.micro.driver.vehicle.provide.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.driver.vehicle.provide.entity.pojo.VehiclePO;

import java.util.Map;

/**
 * 车辆信息表
 *
 * @author xionghw
 * @date 2021-03-29 14:20:48
 */
public interface VehicleService extends IService<VehiclePO> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 通过车牌号找到车辆信息
     * @param carCode
     * @return
     */
    VehiclePO singleInfo(String carCode);

    /**
     * 查询车辆类型
     * @return
     */
    ApiResponse<?> infoCarType();

    /**
     * 保存
     * @param vehiclePO
     */
    void saveOne(VehiclePO vehiclePO);

    /***
     * 更新
     * @param vehiclePO
     */
    void updateOne(VehiclePO vehiclePO);

    /**
     * 更改车辆状态
     * @param vehiclePO
     */
    void updateState(VehiclePO vehiclePO);

}

