package com.logistics.micro.driver.vehicle.provide.service;

/**
 * 车辆统计接口，用于进行车辆汇总统计dubbo统计
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/23 18:08
 */
public interface VehicleStatisticsService {

    /**
     * 根据司机编号获取
     * @param driverCode
     * @return
     */
    Integer getVehicleByDriverCode(String driverCode);
}
