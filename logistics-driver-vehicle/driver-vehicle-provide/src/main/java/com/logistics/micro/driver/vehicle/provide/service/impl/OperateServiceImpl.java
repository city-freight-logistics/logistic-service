package com.logistics.micro.driver.vehicle.provide.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;

import com.logistics.micro.driver.vehicle.provide.dao.OperateDao;
import com.logistics.micro.driver.vehicle.provide.entity.pojo.OperatePO;
import com.logistics.micro.driver.vehicle.provide.service.OperateService;


@Service("operateService")
public class OperateServiceImpl extends ServiceImpl<OperateDao, OperatePO> implements OperateService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OperatePO> page = this.page(
                new Query<OperatePO>().getPage(params),
                new QueryWrapper<OperatePO>()
        );

        return new PageUtils(page);
    }

}