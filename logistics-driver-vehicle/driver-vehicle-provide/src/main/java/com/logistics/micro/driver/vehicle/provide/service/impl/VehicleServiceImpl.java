package com.logistics.micro.driver.vehicle.provide.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.enums.ServiceExceptionEnum;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.CodeUtils;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.driver.vehicle.provide.entity.dto.DriverOrderDataDto;
import com.logistics.micro.driver.vehicle.provide.utils.RedisUtil;
import com.logistics.micro.order.api.DriverOrderDataService;
import lombok.extern.slf4j.Slf4j;
import onwer.find.api.OwnerFindService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;

import com.logistics.micro.driver.vehicle.provide.dao.VehicleDao;
import com.logistics.micro.driver.vehicle.provide.entity.pojo.VehiclePO;
import com.logistics.micro.driver.vehicle.provide.service.VehicleService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service("vehicleService")
@Slf4j
public class VehicleServiceImpl extends ServiceImpl<VehicleDao, VehiclePO> implements VehicleService {
    @Resource
    private RedisUtil redisUtil;

    @Reference
    private DriverOrderDataService driverOrderDataService;
    @Reference
    private OwnerFindService ownerFindService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        if (!isNumeric(String.valueOf(params.get("page")))) {
            throw new ServiceException(ServiceExceptionEnum.PAGE_ERROR);
        }
        // 先查缓存
        StringBuilder key = new StringBuilder();
        key.append("vehicle-");
        for (String s : params.keySet()) {
            key.append(params.get(s));
        }
        String item = String.valueOf(params.get("page"));
        final Object beanObject =  redisUtil.hget(key.toString(),item);
        if (Objects.isNull(beanObject)) {
            IPage<VehiclePO> page = getPageFromDB(params);
            final PageUtils pageUtils = new PageUtils(page);
            if (pageUtils.getTotalCount() == 0){
                throw new ServiceException(ServiceExceptionEnum.DATA_ZERO);
            }
            // 查完了后放入redis
            redisUtil.hset(key.toString(), item, GsonUtils.toJSON(pageUtils),5*60);
            return pageUtils;
        }
        log.info("命中，直接返回");
        final PageUtils pageUtils =  GsonUtils.fromJson(String.valueOf(beanObject),PageUtils.class) ;
        return pageUtils;
    }


    private IPage<VehiclePO> getPageFromDB(Map<String, Object> params) {
        // 没有命中，查db
        log.info("没有命中，查db");
        final QueryWrapper<VehiclePO> condition = new QueryWrapper<>();
        if (!Objects.isNull(params.get("driverName"))) {
            condition.eq("driver_name",params.get("driverName"));
        }
        if (!Objects.isNull(params.get("carClass"))) {
            condition.eq("car_type",params.get("carClass"));
        }
        if (!Objects.isNull(params.get("carCode"))) {
            condition.eq("car_code",params.get("carCode"));
        }
        if (!Objects.isNull(params.get("carState"))) {
            final String carState = String.valueOf(params.get("carState"));
            if ("0".equals(carState)) {
                condition.and(con -> con.eq("state",carState).or(i -> i.eq("state",5)));
            } else if ("4".equals(carState)){
                condition.and(con -> con.eq("state",carState).or(i -> i.eq("state",3)));
            } else {
                condition.eq("state",carState);
            }
        }
        return this.page(
                new Query<VehiclePO>().getPage(params),
                condition
        );
    }

    /**
     * 通过车牌号找到车辆信息
     *
     * @param carCode
     * @return
     */
    @Override
    public VehiclePO singleInfo(String carCode) {
        final VehiclePO one = this.getOne(Wrappers.<VehiclePO>lambdaQuery().eq(VehiclePO::getCarCode, carCode));
        return one;
    }

    /**
     * 查询车辆类型
     *
     * @return
     */
    @Override
    public ApiResponse<?> infoCarType() {
        return ownerFindService.getCarList();
    }

    /**
     * 保存
     *
     * @param vehiclePO
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void saveOne(VehiclePO vehiclePO) {
        final DriverOrderDataDto dataDto = new DriverOrderDataDto();
        dataDto.setDriverCode(vehiclePO.getDriverCode());
        dataDto.setCarCode(vehiclePO.getCarCode());
        final ApiResponse apiResponse = driverOrderDataService.addOne(GsonUtils.toJSON(dataDto));
        if (apiResponse.getHeader().equals(HeaderStatus.ERROR)){
           log.info("插入司机订单模型表dubbo调用异常");
           throw new ServiceException(ServiceExceptionEnum.DUBBO_SEVICE_ERROR);
        }
        final boolean save = this.save(vehiclePO);
        if (!save) {
            throw new ServiceException(ServiceExceptionEnum.INSERT_ERROR);
        }
        // 删除缓存
        redisUtil.del("14" + vehiclePO.getDriverName() + vehiclePO.getState());
    }

    /***
     * 更新
     * @param vehiclePO
     */
    @Override
    public void updateOne(VehiclePO vehiclePO) {
        final boolean update = this.update(vehiclePO, Wrappers.<VehiclePO>lambdaQuery().eq(VehiclePO::getCarCode, vehiclePO.getCarCode()));
        if (!update) {
            throw new ServiceException(ServiceExceptionEnum.UPDATE_ERROR);
        }
        // 删除缓存
        redisUtil.del("14" + vehiclePO.getDriverName() + vehiclePO.getState());
    }

    /**
     * 更改车辆状态
     *
     * @param vehiclePO
     */
    @Override
    public void updateState(VehiclePO vehiclePO) {
        final boolean update = this.lambdaUpdate().set(VehiclePO::getState, vehiclePO.getState())
                .eq(VehiclePO::getCarCode, vehiclePO.getCarCode())
                .update();
        if (!update) {
            throw new ServiceException(ServiceExceptionEnum.UPDATE_ERROR);
        }
    }

    private boolean isNumeric(String s) {
        if (s != null && !"".equals(s.trim())){
            return s.matches("^[0-9]*$");
        }
        return false;

    }

}