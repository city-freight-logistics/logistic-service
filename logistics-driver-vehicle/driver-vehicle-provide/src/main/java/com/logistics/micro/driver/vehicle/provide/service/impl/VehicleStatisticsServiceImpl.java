package com.logistics.micro.driver.vehicle.provide.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.logistics.micro.driver.vehicle.provide.dao.VehicleDao;
import com.logistics.micro.driver.vehicle.provide.entity.pojo.VehiclePO;
import com.logistics.micro.driver.vehicle.provide.service.VehicleStatisticsService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/23 18:13
 */
@Service
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class VehicleStatisticsServiceImpl implements VehicleStatisticsService {
    private final VehicleDao vehicleDao;

    @Override
    public Integer getVehicleByDriverCode(String driverCode) {
        return vehicleDao.selectCount(new QueryWrapper<VehiclePO>().eq("driver_code", driverCode).ne("state", 2));
    }
}
