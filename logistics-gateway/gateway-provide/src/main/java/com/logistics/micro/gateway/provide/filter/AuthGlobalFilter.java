package com.logistics.micro.gateway.provide.filter;

import static com.logistics.micro.common.utils.JwtUtils.checkToken;

import com.google.gson.JsonObject;
import com.logistics.micro.common.constant.AuthServerConstant;
import com.logistics.micro.common.utils.JwtUtils;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 全局Filter，统一处理会员登录与外部不允许访问的服务
 *
 * @author ju.huang02@hand-chian.com 2021/05/14 22:54
 */
@Component
public class AuthGlobalFilter implements GlobalFilter, Ordered {

	private AntPathMatcher antPathMatcher = new AntPathMatcher();

	@Autowired
	StringRedisTemplate redisTemplate;

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		ServerHttpRequest request = exchange.getRequest();
		String path = request.getURI().getPath();
		//物流管理api接口，校验用户必须登录
		if (antPathMatcher.match("/provide/**", path)) {
			if (antPathMatcher.match("/provide/user/**", path) || antPathMatcher.match("/provide/third-party/**", path)) {
				return chain.filter(exchange);
			} else {
				List<String> tokenList = request.getHeaders().get("token");
				if (CollectionUtils.isEmpty(tokenList)) {
					ServerHttpResponse response = exchange.getResponse();
					return out(response);
				} else {
					String jwtToken = tokenList.get(0);
					boolean isCheck = checkToken(jwtToken);
					String redisJwtToken = redisTemplate.opsForValue().get(AuthServerConstant.JWT_TOKEN + jwtToken);
					if (!isCheck && !redisJwtToken.equals(jwtToken)) {
						ServerHttpResponse response = exchange.getResponse();
						return out(response);
					}
				}
			}
		}
		return chain.filter(exchange);
	}


	@Override
	public int getOrder() {
		return 0;
	}

	private Mono<Void> out(ServerHttpResponse response) {
		JsonObject message = new JsonObject();
		message.addProperty("success", false);
		message.addProperty("code", 28004);
		message.addProperty("data", "鉴权失败");
		byte[] bits = message.toString().getBytes(StandardCharsets.UTF_8);
		DataBuffer buffer = response.bufferFactory().wrap(bits);
		//response.setStatusCode(HttpStatus.UNAUTHORIZED);
		//指定编码，否则在浏览器中会中文乱码
		response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
		return response.writeWith(Mono.just(buffer));
	}
}