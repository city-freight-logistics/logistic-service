package com.logistics.micro.grabbing.dispatching.api;

import java.util.Map;

/**
 * 提供对外确认消息或者重新入队
 *
 * @author xionghw0982
 * @date 2021-04-15 17:53
 */
public interface MQAckDubboService {

    /**
     * 通知消息队列可以消费消息了
     * @param deliveryTag 消息的tag
     * @return
     */
    Map<String,Object> noticeMQAck(Long deliveryTag);

    /**
     * 重新入队
     * @param messageAckDtoMap
     */
    void reQueueMQ(Map<String,String> messageAckDtoMap);
}
