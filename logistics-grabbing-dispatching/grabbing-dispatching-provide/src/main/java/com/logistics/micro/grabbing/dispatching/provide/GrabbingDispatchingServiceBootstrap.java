package com.logistics.micro.grabbing.dispatching.provide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Controller
 *
 * @author xionghw0982
 * @date 2021-04-08 17:42
 */
@SpringBootApplication
@EnableDiscoveryClient
public class GrabbingDispatchingServiceBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(GrabbingDispatchingServiceBootstrap.class,args);
    }
}
