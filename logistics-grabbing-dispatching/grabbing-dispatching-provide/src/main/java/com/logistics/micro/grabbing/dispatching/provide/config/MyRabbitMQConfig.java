package com.logistics.micro.grabbing.dispatching.provide.config;


import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/9 23:38
 * @description
 */
@Configuration
public class MyRabbitMQConfig {

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

}
