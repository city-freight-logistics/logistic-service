package com.logistics.micro.grabbing.dispatching.provide.config;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/10 15:29
 * @description
 * WebSocket配置类
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

@Configuration
public class WebSocketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
