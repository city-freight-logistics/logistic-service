package com.logistics.micro.grabbing.dispatching.provide.controller;


import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.grabbing.dispatching.provide.service.DriverListeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 派单 Controller
 *
 * @author xionghw0982
 * @date 2021-04-08 17:47
 */
@RestController
@RequestMapping("provide/dispatching")
public class DispatchingController {
    @Autowired
    DriverListeningService driverListeningService;

    @PostMapping("/onListening")
    public ApiResponse onListening(@RequestParam String driverOrderDataDto) {
        driverListeningService.onLitening(driverOrderDataDto);
        return new ApiResponse<>(HeaderStatus.SUCCESS);
    }

    @PostMapping("/cancleListening")
    public ApiResponse cancleListening(@RequestParam String driverOrderDataDto) {
        driverListeningService.cancleListening(driverOrderDataDto);
        return new ApiResponse<>(HeaderStatus.SUCCESS);
    }


}
