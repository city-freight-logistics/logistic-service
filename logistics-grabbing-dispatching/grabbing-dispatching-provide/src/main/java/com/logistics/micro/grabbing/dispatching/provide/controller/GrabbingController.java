package com.logistics.micro.grabbing.dispatching.provide.controller;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.GoodsType;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.GrabbingOrderDto;
import com.logistics.micro.grabbing.dispatching.provide.service.GrabbingService;
import com.logistics.micro.owner.manage.api.OwnerManageService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 抢单 Controller
 *
 * @author xionghw0982
 * @date 2021-04-08 17:46
 */
@RestController
@RequestMapping("provide/grabbing")
public class GrabbingController {
    @Autowired
    GrabbingService grabbingService;

    @PostMapping("/accept")
    public ApiResponse<String> accept(@RequestParam String grabbingOrderDto) {
        final GrabbingOrderDto grabbingOrderDto1 = GsonUtils.fromJson(grabbingOrderDto, GrabbingOrderDto.class);
        grabbingService.acceptOrder(grabbingOrderDto1);
        return new ApiResponse<String>("抢单成功");
    }

    @PostMapping("/reject")
    public ApiResponse reject(@RequestParam String orderRejectDto) {
        return grabbingService.rejectOrder(orderRejectDto);
    }


}
