package com.logistics.micro.grabbing.dispatching.provide.dubboService;

import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.grabbing.dispatching.api.MQAckDubboService;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.MessageAckDto;
import com.logistics.micro.grabbing.dispatching.provide.enums.CachekeyEnum;
import com.logistics.micro.grabbing.dispatching.provide.service.impl.MessageServiceImpl;
import com.logistics.micro.grabbing.dispatching.provide.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller
 *
 * @author xionghw0982
 * @date 2021-04-15 17:51
 */
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class MQAckDubboServiceImpl implements MQAckDubboService {

    @Resource
    MessageServiceImpl messageServicel;
    @Autowired
    RedissonClient redissonClient;

    @Resource
    RedisUtil redisUtil;

    /**
     * 通知消息队列可以消费消息了
     *
     * @param deliveryTag 消息的tag
     * @return
     */
    @Override
    public Map<String, Object> noticeMQAck(Long deliveryTag) {
        Map<String,Object> map = new HashMap<String,Object>(3);
        Boolean success = messageServicel.ackMessage(deliveryTag);
        log.info("消息已确认消费");
        if (success) {
            map.put("header", HeaderStatus.SUCCESS);
        } else {
            map.put("header",HeaderStatus.ERROR);
        }
        map.put("body",null);
        return map;
    }

    /**
     * 重新入队
     *
     * @param messageAckDtoMap
     */
    @Override
    public void reQueueMQ(Map<String, String> messageAckDtoMap) {
        final MessageAckDto ackDto = GsonUtils.fromJson(messageAckDtoMap.get("message"), MessageAckDto.class);
        if (ackDto.getMessageState() == 0) {
            if (ackDto.getOrderState() < 2) {
                log.info("消息重新入队,订单号为{}",ackDto.getOrderCode());
                messageServicel.nackMessage(ackDto.getDeliveryTag());
            } else {
                final RLock lock = redissonClient.getLock("order-grabbing-lock");
                try {
                    lock.lock();
                    // 可能是没得来及消费确认，但事实上已经消费了
                    ackDto.setMessageState(1);
                    messageServicel.ackMessage(ackDto.getDeliveryTag());
                    redisUtil.hset(CachekeyEnum.ORDER_MESSAGE_CACHE_KEY.getKey(), ackDto.getOrderCode(), GsonUtils.toJSON(ackDto));
                } finally {
                    lock.unlock();
                }

            }
        }
    }
}
