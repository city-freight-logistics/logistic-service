package com.logistics.micro.grabbing.dispatching.provide.entity.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 正在听单的司机 dto
 *
 * @author xionghw0982
 * @date 2021-04-09 14:23
 */
@Data
public class DriverOnListening implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 司机姓名
     */
    private String driverName;
    /**
     * 车牌
     */
    private String carCode;
    /**
     * 司机位置-纬度
     */
    private String latitude;
    /**
     * 司机位置-经度
     */
    private String longitude;
}
