package com.logistics.micro.grabbing.dispatching.provide.entity.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 司机听单数据
 *
 * @author xionghw0982
 * @date 2021-04-09 13:47
 */
@Data
public class DriverOnListeningDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    private Integer id;
    /**
     * 司机编号
     */
    private String driverCode;
    /**
     * 车牌号
     */
    private String carCode;
    /**
     * 接驾里程
     */
    private Double mileage;
    /**
     * 历史接单数
     */
    private Integer countOrder;
    /**
     * 历史接单率
     */
    private Double historyRate;
    /**
     * 当前接单率
     */
    private Double currentRate;
    /**
     * 司机位置-纬度
     */
    private String latitude;
    /**
     * 司机位置-经度
     */
    private String longitude;

}
