package com.logistics.micro.grabbing.dispatching.provide.entity.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用于存放到DB的司机方订单信息
 *
 * @author xionghw0982
 * @date 2021-04-14 20:46
 */
@Data
public class DriverOrderDto {
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 订单名称
     */
    private String orderName;
    /**
     * 订单执行人
     */
    private String driverName;
    /**
     * 收货人
     */
    private String consigneeName;
    /**
     * 订单发起人
     */
    private String customerName;
    /**
     * 运费
     */
    private Double freightAmount;
    /**
     * 收取金额
     */
    private Double receiveAmount;
    /**
     * 支付方式【1->支付宝；2->微信】
     */
    private Integer payType;
    /**
     * 订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）
     */
    private Integer orderState;
    /**
     * 货物分类名称
     */
    private String typeName;
    /**
     * 订单评价
     */
    private String orderComment;
    /**
     * 订单备注
     */
    private String orderRemarks;
    /**
     * 发起人联系电话
     */
    private String customerTel;
    /**
     * 收货人联系电话
     */
    private String consigneeTel;
    /**
     * 订单到司机的距离
     */
    private String distanceDriver;
    /**
     * 下单位置-纬度
     */
    private String latitude;
    /**
     * 下单位置-经度
     */
    private String longitude;
    /**
     * 订单开始日期
     */
    private Date beginDate;
    /**
     * 订单结束日期
     */
    private Date completeDate;
}
