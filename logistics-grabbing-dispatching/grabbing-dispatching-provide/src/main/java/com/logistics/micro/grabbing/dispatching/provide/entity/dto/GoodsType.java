package com.logistics.micro.grabbing.dispatching.provide.entity.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 货物类别
 *
 * @author xionghw0982
 * @date 2021-04-30 15:04
 */
@Data
public class GoodsType implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer typeId;

    private String typeName;

    private Double acceptRate;

    List<Map<String,Object>> children=new ArrayList<>();
}
