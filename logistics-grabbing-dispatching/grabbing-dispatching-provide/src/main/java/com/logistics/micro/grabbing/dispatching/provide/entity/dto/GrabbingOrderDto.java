package com.logistics.micro.grabbing.dispatching.provide.entity.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 司机发起抢单的Dto
 *
 * @author xionghw0982
 * @date 2021-04-09 09:38
 */
@Data
public class GrabbingOrderDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private Integer id;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 订单名称
     */
    private String orderName;
    /**
     * 司机姓名
     */
    private String driverName;
    /**
     * 司机编号
     */
    private String driverCode;
    /**
     * 车牌
     */
    private String carCode;
    /**
     * 运输的货物类型
     */
    private Integer goodsType;
    /**
     * 订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）
     */
    private Integer orderState;
    /**
     * 订单开始时间
     */
    private Date beginDate;
    /**
     * 起点到终点的距离
     */
    private String distanceOrder;
    /**
     * 订单到司机的距离
     */
    private String distanceDriver;

}
