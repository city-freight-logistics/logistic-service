package com.logistics.micro.grabbing.dispatching.provide.entity.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 司机接单率表  
 * 
 * @author hj
 * @date 2021-04-19 09:45:51
 */
@Data
public class OrderAcceptRateDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private Integer id;

	/**
	 * 订单起点到终点的距离概率[0,1)
	 */
	private Double distanceOrderRate01;
	/**
	 * 订单起点到终点的距离概率[1,3)
	 */
	private Double distanceOrderRate13;
	/**
	 * 订单起点到终点的距离概率[3,5)
	 */
	private Double distanceOrderRate35;
	/**
	 * 订单起点到终点的距离概率[5,10)
	 */
	private Double distanceOrderRate510;
	/**
	 * 订单起点到终点的距离概率[10,∞)
	 */
	private Double distanceOrderRate10More;

	private List<Double> distanceOrderRates;

	/**
	 * 订单与司机的距离概率[0,0.5)
	 */
	private Double distanceDriverRate005;
	/**
	 * 订单与司机的距离概率[0.5,2)
	 */
	private Double distanceDriverRate052;
	/**
	 * 订单与司机的距离概率[2,5)
	 */
	private Double distanceDriverRate25;
	/**
	 * 订单与司机的距离概率[5,10)
	 */
	private Double distanceDriverRate510;
	/**
	 * 订单与司机的距离概率[10,∞)
	 */
	private Double distanceDriverRate10More;

	private List<Double> distanceDriverRates;

	/**
	 * 历史接单数概率[0,100)
	 */
	private Double historyCount0100;
	/**
	 * 历史接单数概率[100,300)
	 */
	private Double historyCount100300;
	/**
	 * 历史接单数概率[300,500)
	 */
	private Double historyCount300500;
	/**
	 * 历史接单数概率[500,1000)
	 */
	private Double historyCount5001000;
	/**
	 * 历史接单数概率[1000,∞)
	 */
	private Double historyCount1000More;

	private List<Double> historyCountRates;

	/**
	 * 历史接单率的概率[0,10)
	 */
	private Double historyRate010;
	/**
	 * 历史接单率的概率[10,30)
	 */
	private Double historyRate1030;
	/**
	 * 历史接单率的概率[30,50)
	 */
	private Double historyRate3050;
	/**
	 * 历史接单率的概率[50,80)
	 */
	private Double historyRate5080;
	/**
	 * 历史接单率的概率[80,100)
	 */
	private Double historyRate80100;

	private List<Double> historyRateRates;

	/**
     * 创建时间
	 */
	private Date createDate;
}
