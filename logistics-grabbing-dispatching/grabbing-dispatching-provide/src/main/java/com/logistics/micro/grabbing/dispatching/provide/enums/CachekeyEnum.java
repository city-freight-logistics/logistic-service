package com.logistics.micro.grabbing.dispatching.provide.enums;

/**
 * redis 里面缓存用的key
 *
 * @author xionghw0982
 * @date 2021-04-16 09:36
 */
public enum CachekeyEnum {

    DRIVER_LITENING_CACHE_KEY("driver-listening"),

    ORDER_MESSAGE_CACHE_KEY("order-message"),

    ORDER_TEMP_CACHE_KEY("order-temp"),

    DRIVER_REJECT_CACHE_KEY("driver-reject"),

    ORDER_ACCEPT("order-accept-number")
    ;

    private String key;

    CachekeyEnum(String key) {
        this.key = key;
    }

    public String getKey(){
        return this.key;
    }
}
