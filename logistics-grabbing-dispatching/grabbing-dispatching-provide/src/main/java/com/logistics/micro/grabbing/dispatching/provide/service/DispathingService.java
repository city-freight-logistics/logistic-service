package com.logistics.micro.grabbing.dispatching.provide.service;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.DispatchOrderDto;


import java.util.List;

/**
 * 派单 service
 *
 * @author xionghw0982
 * @date 2021-04-08 17:49
 */
public interface DispathingService {
    /**
     * 派单
     * @param dispatchOrderDto
     * @return
     */
    List<String> dispaching(DispatchOrderDto dispatchOrderDto);
}
