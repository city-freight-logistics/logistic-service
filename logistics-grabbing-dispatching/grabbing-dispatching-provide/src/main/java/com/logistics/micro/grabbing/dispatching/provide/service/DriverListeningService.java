package com.logistics.micro.grabbing.dispatching.provide.service;

/**
 * 司机听单中
 *
 * @author xionghw0982
 * @date 2021-04-15 14:48
 */
public interface DriverListeningService {
    /**
     * 司机正在听单
     * @param driverOrderDataDto
     * @return
     */
    void onLitening(String driverOrderDataDto);

    /**
     * 取消听单
     * @param driverOrderDataDto
     */
    void cancleListening(String driverOrderDataDto);
}
