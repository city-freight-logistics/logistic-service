package com.logistics.micro.grabbing.dispatching.provide.service;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.GrabbingOrderDto;

/**
 * 抢单 service
 *
 * @author xionghw0982
 * @date 2021-04-08 17:49
 */
public interface GrabbingService {
    /**
     * 抢单操作
     * @param grabbingOrderDto
     */
    void acceptOrder(GrabbingOrderDto grabbingOrderDto);

    /**
     * 拒绝单
     * @param orderRejectDto
     * @return
     */
    ApiResponse rejectOrder(String orderRejectDto);
}
