package com.logistics.micro.grabbing.dispatching.provide.service;

import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.DispatchOrderDto;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

/**
 * 接受消息
 *
 * @author xionghw0982
 * @date 2021-04-13 22:44
 */
public interface MessageService {
    /**
     * 接受消息
     * @param message
     * @param content
     * @param channel
     */
    void ReceiveMessage(Message message, RabbitMessage<?> content, Channel channel);
}
