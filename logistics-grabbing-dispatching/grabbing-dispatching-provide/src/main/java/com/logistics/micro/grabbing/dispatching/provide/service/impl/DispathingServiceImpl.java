package com.logistics.micro.grabbing.dispatching.provide.service.impl;

import com.logistics.micro.common.enums.ServiceExceptionEnum;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.DispatchOrderDto;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.DriverOnListeningDto;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.GoodsType;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.OrderAcceptRateDto;
import com.logistics.micro.grabbing.dispatching.provide.enums.CachekeyEnum;
import com.logistics.micro.grabbing.dispatching.provide.service.DispathingService;
import com.logistics.micro.grabbing.dispatching.provide.utils.RedisUtil;
import com.logistics.micro.order.api.OrderAcceptRateService;
import com.logistics.micro.owner.manage.api.OwnerManageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * 派单 service
 *
 * @author xionghw0982
 * @date 2021-04-08 17:50
 */
@Service
@Slf4j
public class DispathingServiceImpl implements DispathingService {
    @Resource
    RedisUtil redisUtil;
    @Autowired
    RedissonClient redissonClient;
    @Resource(name = "createThreadPool")
    private ExecutorService executor;
    @Reference
    OrderAcceptRateService orderAcceptRateService;
    @Reference
    OwnerManageService ownerManageService;


    /**
     * 地球半径
     */
    private final Double R = 6371.004;

    /**
     * 派单
     *
     * @param dispatchOrderDto
     * @return
     */
    @Override
    public List<String> dispaching(DispatchOrderDto dispatchOrderDto) throws ServiceException {
        // 第一步：去redis中获取正在听单且没有拒绝过该订单的司机群
        final CompletableFuture<List<DriverOnListeningDto>> driverGroupFuture = CompletableFuture.supplyAsync(() -> {
            final Map<Object, Object> driverGroup = redisUtil.hmget(CachekeyEnum.DRIVER_LITENING_CACHE_KEY.getKey());
            if (driverGroup == null || driverGroup.size() <= 0) {
                throw new ServiceException(ServiceExceptionEnum.NO_LISTENING_DRIVER_DATA);
            }
            // 获取拒绝过该订单的司机群
            final String rejectListsStr = String.valueOf(redisUtil.hget(CachekeyEnum.DRIVER_REJECT_CACHE_KEY.getKey(), dispatchOrderDto.getOrderCode()));
            if (!"null".equals(rejectListsStr)) {
                final List<String> rejectDto = GsonUtils.gsonToList(rejectListsStr, String.class);
                final List<DriverOnListeningDto> dataDto = switchToDriverOrderData(driverGroup)
                        .stream().filter(driver -> !rejectDto.contains(driver.getDriverCode()))
                        .collect(Collectors.toList());
                return dataDto;
            }

            final List<DriverOnListeningDto> dataDto = switchToDriverOrderData(driverGroup);
            return dataDto;


        }, executor);

        // 第二步：获取接单率模型数据
        final CompletableFuture<OrderAcceptRateDto> rateFuture = CompletableFuture.supplyAsync(() -> {
            final ApiResponse<OrderAcceptRateDto> rateModel = orderAcceptRateService.getRateModel();
            Map<String, Object> map = (Map<String, Object>) rateModel.getBody();
            final OrderAcceptRateDto rateDto = new OrderAcceptRateDto();
            GsonUtils.mapToBean(map, rateDto);
            initRate(rateDto);
            return rateDto;
        }, executor);

        // 第三步：计算接单率
        final CompletableFuture<List<String>> finalCollectFuture = driverGroupFuture.thenCombineAsync(rateFuture, (f1, f2) -> {
            calculateAcceptRate(f1, dispatchOrderDto, f2);
            final List<String> collect = f1.stream().
                    sorted(((o1, o2) -> o2.getCurrentRate().compareTo(o1.getCurrentRate())))
                    .limit(30)
                    .map(s -> s.getDriverCode())
                    .collect(Collectors.toList());
            return collect;
        }, executor);

        try {
            CompletableFuture.allOf(driverGroupFuture,rateFuture,finalCollectFuture).get();
            final List<String> fianlCollecttion = finalCollectFuture.get();
            return fianlCollecttion;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    private void initRate(OrderAcceptRateDto rateDto) {
        final List<Double> distanceDriverRates = new ArrayList<>(5);
        distanceDriverRates.add(rateDto.getDistanceDriverRate005());
        distanceDriverRates.add(rateDto.getDistanceDriverRate052());
        distanceDriverRates.add(rateDto.getDistanceDriverRate25());
        distanceDriverRates.add(rateDto.getDistanceDriverRate510());
        distanceDriverRates.add(rateDto.getDistanceDriverRate10More());
        rateDto.setDistanceDriverRates(distanceDriverRates);

        final List<Double> distanceOrderRates = new ArrayList<>(5);
        distanceOrderRates.add(rateDto.getDistanceOrderRate01());
        distanceOrderRates.add(rateDto.getDistanceOrderRate13());
        distanceOrderRates.add(rateDto.getDistanceOrderRate35());
        distanceOrderRates.add(rateDto.getDistanceOrderRate510());
        distanceOrderRates.add(rateDto.getDistanceOrderRate10More());
        rateDto.setDistanceOrderRates(distanceOrderRates);

        final List<Double> historyCountRates = new ArrayList<>(5);
        historyCountRates.add(rateDto.getHistoryCount0100());
        historyCountRates.add(rateDto.getHistoryCount100300());
        historyCountRates.add(rateDto.getHistoryCount300500());
        historyCountRates.add(rateDto.getHistoryCount5001000());
        historyCountRates.add(rateDto.getHistoryCount1000More());
        rateDto.setHistoryCountRates(historyCountRates);

        final List<Double> historyRateRates = new ArrayList<>(5);
        historyRateRates.add(rateDto.getHistoryRate010());
        historyRateRates.add(rateDto.getHistoryRate1030());
        historyRateRates.add(rateDto.getHistoryRate3050());
        historyRateRates.add(rateDto.getHistoryRate5080());
        historyRateRates.add(rateDto.getHistoryRate80100());
        rateDto.setHistoryRateRates(historyRateRates);
    }

    private List<DriverOnListeningDto> switchToDriverOrderData(Map<Object, Object> source) {
        final RLock lock = redissonClient.getLock("driver-listening-lock");
        List<DriverOnListeningDto> target = null;
        try {
            lock.lock();
            target = new ArrayList<>(source.size());
            List<DriverOnListeningDto> finalTarget = target;
            source.forEach((key, value) ->{
                finalTarget.add(GsonUtils.fromJson(String.valueOf(value),DriverOnListeningDto.class));
            });
        }catch (Exception e){
            log.warn(e.getMessage());
        } finally {
            lock.unlock();
        }
       return target;
    }

    private void calculateAcceptRate(List<DriverOnListeningDto> dataDto,
                                     DispatchOrderDto dispatchOrderDto,
                                     OrderAcceptRateDto rateDto) {
        final ApiResponse<?> response = ownerManageService.listGoods();
        final List<Map<String,Object>> body = (List<Map<String, Object>>) response.getBody();
        List<GoodsType> goodsTypeList = new ArrayList<>(body.size());
        body.forEach(item -> {
            final GoodsType goodsType = new GoodsType();
            GsonUtils.mapToBean(item,goodsType);
            goodsTypeList.add(goodsType);
        });
        final Iterator<DriverOnListeningDto> iterator = dataDto.iterator();
        while (iterator.hasNext()){
            final DriverOnListeningDto driverOrderDataModel = iterator.next();
            // 计算司机与订单的距离
            final Double distance = calculateDistance(Double.parseDouble(driverOrderDataModel.getLongitude()),
                    Double.parseDouble(driverOrderDataModel.getLatitude()),
                    Double.parseDouble(dispatchOrderDto.getLongitude()),
                    Double.parseDouble(dispatchOrderDto.getLatitude()));
            log.info("司机与订单的距离为{}",distance);
            // 货物类别概率
            final Double goodsRate = goodsTypeRate(goodsTypeList,dispatchOrderDto.getTypeName());
            // 起点到终点距离的概率
            final Double startToEndDistanceRate = startToEndDistanceRate(Double.parseDouble(dispatchOrderDto.getDistanceOrder()),
                    rateDto.getDistanceOrderRates());
            // 订单与司机距离的概率
            final Double orderFromDriverDistanceRate = orderFromDriverDistance(distance,rateDto.getDistanceDriverRates());
            // 历史接单数的概率
            final Double historyCountRate = historyCount(driverOrderDataModel.getCountOrder(),rateDto.getHistoryCountRates());
            // 历史接单率的概率
            final Double historyRate = historyRate(driverOrderDataModel.getHistoryRate(),rateDto.getHistoryRateRates());
            // 最终概率
            final Double finalRate = finalRate(goodsRate,
                    startToEndDistanceRate,
                    orderFromDriverDistanceRate,
                    historyCountRate,
                    historyRate);
            driverOrderDataModel.setCurrentRate(finalRate);
        }
    }

    /**
     * 计算司机与订单的距离
     * @param lonA 司机当前经度
     * @param latA 司机当前纬度
     * @param lonOrder 订单的经度
     * @param latOrder 订单的纬度
     * @return 结果单位是 km
     */
    private Double calculateDistance(Double lonA, Double latA, Double lonOrder, Double latOrder) {
        Double a = Math.abs((latA - latOrder) / 2.0);
        Double b = Math.abs((lonA - lonOrder) / 2.0);
        final double v = Math.sin(a) * Math.sin(a) + Math.cos(latA) * Math.cos(latOrder) * Math.sin(b) * Math.sin(b);
        final double distance = Math.asin(Math.sqrt(v)) * 2 * this.R;
        return distance;
    }

    /**
     * 货物类型接单概率
     * @param typeName
     * @return
     */
    private Double goodsTypeRate(List<GoodsType> goodsTypeList, String typeName){
        AtomicReference<Double> rate = new AtomicReference<>(0.0);
        goodsTypeList.stream().filter(goods -> goods.getChildren().size() > 0)
                .forEach(goods -> goods.getChildren().forEach(child -> {
            if (typeName.equals(child.get("typeName"))) {
                rate.set(Double.parseDouble(child.get("acceptRate").toString()));
            }
        }));
       return rate.get();
    }

    /**
     * 起点到终点距离的概率
     * @param distance
     * @return
     */
    private Double startToEndDistanceRate(Double distance,List<Double> list){
        if (0 <= distance && distance < 1.0) {
            return list.get(0);
        } else if (distance < 3) {
            return list.get(1);
        } else if (distance < 5) {
            return list.get(2);
        } else if (distance < 10) {
            return list.get(3);
        } else {
            return list.get(4);
        }
    }

    /**
     * 订单与司机距离的概率
     * @param distance
     * @return
     */
    private Double orderFromDriverDistance(Double distance,List<Double> list) {
        if (0 <= distance && distance < 0.5) {
            return list.get(0);
        } else if (distance < 2) {
            return list.get(1);
        } else if (distance < 5) {
            return list.get(2);
        } else if (distance < 10) {
            return list.get(3);
        } else {
            return list.get(4);
        }
    }

    /**
     * 历史接单数的概率
     * @param countOrder
     * @return
     */
    private Double historyCount(Integer countOrder,List<Double> list) {
        if (0 <= countOrder && countOrder < 100) {
            return list.get(0);
        } else if (countOrder < 300) {
            return list.get(1);
        } else if (countOrder < 500) {
            return list.get(2);
        } else if ( countOrder < 1000) {
            return list.get(3);
        } else {
            return list.get(4);
        }
    }

    /**
     * 历史接单率的概率
     * @param hRate
     * @return
     */
    private Double historyRate(Double hRate,List<Double> list) {
        if (0 <= hRate && hRate < 10) {
            return Math.pow(hRate / 100.0, list.get(0)) * 100.0;
        } else if (hRate < 30.0) {
            return  Math.pow(hRate / 100.0, list.get(1)) * 100.0;
        } else if (hRate < 50.0) {
            return  Math.pow(hRate / 100.0, list.get(2)) * 100.0;
        } else if (hRate < 80.0) {
            return  Math.pow(hRate / 100.0, list.get(3)) * 100.0;
        } else {
            return Math.pow(hRate / 100.0, list.get(4)) * 100.0;
        }
    }

    /**
     * 计算最终接单率
     * @param x0 接驾里程概率
     * @param x1 起点到终点概率
     * @param x2 订单与司机距离概率
     * @param x3 历史接单数概率
     * @param x4 历史接单率概率
     * @return
     */
    private Double finalRate(Double x0,Double x1, Double x2, Double x3, Double x4) {
        return (1 * x0 + 2.0 * x1 + 3.0* x2 + 2 * x3 + 3.0 * x4) / 11.0;
    }
}
