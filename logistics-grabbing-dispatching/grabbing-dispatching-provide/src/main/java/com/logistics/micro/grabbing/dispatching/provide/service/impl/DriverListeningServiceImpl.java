package com.logistics.micro.grabbing.dispatching.provide.service.impl;

import com.logistics.micro.common.enums.ServiceExceptionEnum;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.DriverOnListeningDto;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.DriverOrderDataDto;
import com.logistics.micro.grabbing.dispatching.provide.enums.CachekeyEnum;
import com.logistics.micro.grabbing.dispatching.provide.service.DriverListeningService;
import com.logistics.micro.grabbing.dispatching.provide.utils.RedisUtil;
import com.logistics.micro.order.api.DriverOrderDataService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 司机听单中
 *
 * @author xionghw0982
 * @date 2021-04-15 14:51
 */
@Service
@Slf4j
public class DriverListeningServiceImpl implements DriverListeningService {


    @Resource
    RedisUtil redisUtil;

    @Reference
    DriverOrderDataService driverOrderDataService;

    /**
     * 司机正在听单
     * 需要司机的相关信息放入缓存
     * @param driverOrderDataDto
     * @return
     */
    @Override
    public void onLitening(String driverOrderDataDto) {
        log.info("司机准备听单");
        final DriverOnListeningDto dataDto = GsonUtils.fromJson(driverOrderDataDto, DriverOnListeningDto.class);
        final ApiResponse response = driverOrderDataService.getOne(dataDto.getDriverCode());
        // dubbo调用从db里面获取了司机-订单关系数据
        final DriverOrderDataDto body = GsonUtils.fromJson(String.valueOf(response.getBody()), DriverOrderDataDto.class);
        BeanUtils.copyProperties(body,dataDto);
        final boolean hset = redisUtil.hset(CachekeyEnum.DRIVER_LITENING_CACHE_KEY.getKey(), dataDto.getDriverCode(), GsonUtils.toJSON(dataDto));
        if (!hset) {
            log.warn("插入缓存失败");
            throw new ServiceException(ServiceExceptionEnum.INSERT_CACHE_ERROR);
        }
        log.info("司机听单插入缓存成功");
    }

    /**
     * 取消听单
     *
     * @param driverOrderDataDto
     */
    @Override
    public void cancleListening(String driverOrderDataDto) {
        final DriverOnListeningDto dataDto = GsonUtils.fromJson(driverOrderDataDto, DriverOnListeningDto.class);
        redisUtil.hdel(CachekeyEnum.DRIVER_LITENING_CACHE_KEY.getKey(),dataDto.getDriverCode());
        log.info("司机听单删除缓存成功");
    }
}
