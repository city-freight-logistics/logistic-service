package com.logistics.micro.grabbing.dispatching.provide.service.impl;

import com.logistics.micro.common.enums.ServiceExceptionEnum;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.order.OrderStatusDto;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.*;
import com.logistics.micro.grabbing.dispatching.provide.enums.CachekeyEnum;
import com.logistics.micro.grabbing.dispatching.provide.service.GrabbingService;
import com.logistics.micro.grabbing.dispatching.provide.utils.RedisUtil;
import com.logistics.micro.message.api.OrderService;
import com.logistics.micro.order.api.DriverOrderDataService;
import com.logistics.micro.order.api.DriverOrderService;
import com.logistics.micro.order.api.OrderRejectService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

/**
 * 抢单 service
 *
 * @author xionghw0982
 * @date 2021-04-08 17:51
 */
@Slf4j
@Service
public class GrabbingServiceImpl implements GrabbingService {
    @Autowired
    RedissonClient redissonClient;
    @Autowired
    MessageServiceImpl messageServicel;
    @Resource
    RedisUtil redisUtil;
    @Resource(name = "createThreadPool")
    private ExecutorService executor;


    @Reference
    DriverOrderService driverOrderService;
    @Reference
    OrderRejectService orderRejectService;
    @Reference
    DriverOrderDataService driverOrderDataService;
    @Reference
    OrderService orderService;


    /**
     * 抢单操作
     *
     * @param grabbingOrderDto
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void acceptOrder(GrabbingOrderDto grabbingOrderDto) {
        final RLock lock = redissonClient.getLock("order-grabbing-lock");
        // 成功抢单
        try {
            lock.lock();
            executor.execute(() ->{
                // 抢单人数 + 1
                Integer num = 1;
                final Integer acceptNum = (Integer) redisUtil.hget(CachekeyEnum.ORDER_ACCEPT.getKey(), grabbingOrderDto.getOrderCode());
                if (!Objects.isNull(acceptNum)) {
                    num += acceptNum;
                }
                redisUtil.hset(CachekeyEnum.ORDER_ACCEPT.getKey(), grabbingOrderDto.getOrderCode(),num);
                driverOrderDataService.updateDistanceData(GsonUtils.toJSON(grabbingOrderDto));
            });

            log.info("进入抢单状态，加锁成功");
            // 首先判断抢的这个订单的状态是否在2以下，并从订单临时表中取出订单相关信息
            final MessageAckDto canAccept = isCanAccept(grabbingOrderDto);
            if (Objects.isNull(canAccept)) {
                log.info("抢单失败");
                throw new ServiceException(ServiceExceptionEnum.GRABBING_FAIL);
            }
            log.info("可以抢单");
            //  这里应该通过订单编号从订单临时表中取出订单相关信息
            final String orderInfoStr = String.valueOf(redisUtil.hget(CachekeyEnum.ORDER_TEMP_CACHE_KEY.getKey(), grabbingOrderDto.getOrderCode()));
            if ("null".equals(orderInfoStr)){
                throw new ServiceException(ServiceExceptionEnum.NO_ORDER_CACHE_DATA, grabbingOrderDto.getOrderCode());
            }
            final OrderDto orderDto = GsonUtils.fromJson(orderInfoStr, OrderDto.class);
            log.info("获取订单缓存信息");
            // 保存司机接单，并通知消息队列可以消费消息了
            insertDriverOrderDBAndCache(orderDto,
                    canAccept,
                    grabbingOrderDto);
        } catch (ServiceException e){
            throw new ServiceException(e.getMessage());
        } catch (Exception e) {
            log.warn(e.getMessage());
         } finally {
            lock.unlock();
            log.info("抢单释放锁");
        }
    }

    /**
     * 拒绝单
     *
     * @param orderRejectDto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse rejectOrder(String orderRejectDto) {
        final OrderRejectDto rejectDto = GsonUtils.fromJson(orderRejectDto, OrderRejectDto.class);
        final String hget = String.valueOf(redisUtil.hget(CachekeyEnum.DRIVER_REJECT_CACHE_KEY.getKey(), rejectDto.getOrderCode()));

        List<String> rejectList = null;
        if ("null".equals(hget)) {
            rejectList = new ArrayList<>(1);
        }else {
            rejectList = GsonUtils.gsonToList(hget, String.class);
        }
        if (rejectList.contains(rejectDto.getDriverCode())) {
            throw new ServiceException(ServiceExceptionEnum.DUPLICATE_REJECT);
        }
        rejectList.add(rejectDto.getDriverCode());
        final boolean hset = redisUtil.hset(CachekeyEnum.DRIVER_REJECT_CACHE_KEY.getKey(), rejectDto.getOrderCode(),
                GsonUtils.toJSON(rejectList));
        if (!hset) {
            throw new ServiceException(ServiceExceptionEnum.INSERT_CACHE_ERROR);
        }
        driverOrderDataService.updateRejectNum(orderRejectDto);
        return orderRejectService.addOne(orderRejectDto);

    }

    /**
     * 可以抢单就返回这个临时订单的缓存，不然就返回null
     * @param grabbingOrderDto
     * @return
     */
    private MessageAckDto isCanAccept(GrabbingOrderDto grabbingOrderDto) {
        final String tempOrder = String.valueOf(redisUtil.hget(CachekeyEnum.ORDER_MESSAGE_CACHE_KEY.getKey(), grabbingOrderDto.getOrderCode()));
        final MessageAckDto ackDto = GsonUtils.fromJson(tempOrder, MessageAckDto.class);
        final Integer state = ackDto.getOrderState();
        final Integer messageState = ackDto.getMessageState();
        // 2表示受理状态 0表示未消费
        if (state < 2 && messageState == 0) {
            // 可以接这个订单
            return ackDto;
        }
        // 订单已经处于受理状态或以后
        return null;
    }

    /**
     * 1、保存司机方订单
     * 2、通知消息队列消费消息
     * 3、更改订单消息缓存为已消费
     * 4、更改订单缓存
     * @param orderDto
     * @param ackDto
     */
    private void insertDriverOrderDBAndCache(OrderDto orderDto,MessageAckDto ackDto, GrabbingOrderDto grabbingOrderDto) {
        executor.execute(() -> {
            ackDto.setOrderState(2);
            ackDto.setMessageState(1);
            ackDto.setDriverCode(grabbingOrderDto.getDriverCode());
            ackDto.setCarCode(grabbingOrderDto.getCarCode());
            // 抢单成功，通知消息队列该消息可以消费
            messageServicel.ackMessage(ackDto.getDeliveryTag());
            log.info("通知消息队列消费消息");
            // 更改 订单消息的状态 为已消费
            redisUtil.hset(CachekeyEnum.ORDER_MESSAGE_CACHE_KEY.getKey(),
                    ackDto.getOrderCode(),
                    GsonUtils.toJSON(ackDto));
            log.info("更改消息缓存为已消费");
        });

        executor.execute(() -> {
            // 更改订单缓存
            orderDto.setDriverName(grabbingOrderDto.getDriverName());
            orderDto.setOrderState(2);
            orderDto.setCarCode(grabbingOrderDto.getCarCode());
            orderDto.setDistanceDriver(grabbingOrderDto.getDistanceDriver());
            redisUtil.hset(CachekeyEnum.ORDER_TEMP_CACHE_KEY.getKey(),orderDto.getOrderCode(),GsonUtils.toJSON(orderDto));
            log.info("更改订单缓存信息");
            final DriverOrderDto driverOrderDto = new DriverOrderDto();
            BeanUtils.copyProperties(orderDto,driverOrderDto);
            driverOrderService.addOne(GsonUtils.toJSON(driverOrderDto));
            log.info("司机方订单保存成功");
        });

        final OrderStatusDto statusDto = new OrderStatusDto();
        statusDto.setOrderCode(orderDto.getOrderCode());
        statusDto.setOrderName(orderDto.getOrderName());
        statusDto.setOrderState(2);
        final RabbitMessage<OrderStatusDto> orderMessage = new RabbitMessage<>();
        orderMessage.setDesc("订单编号为" + statusDto.getOrderCode() + "的订单状态信息");
        orderMessage.setBody(statusDto);
        orderService.providerSendOrderStatus(orderMessage);
        log.info("发送订单状态消息到消息队列");
    }
}
