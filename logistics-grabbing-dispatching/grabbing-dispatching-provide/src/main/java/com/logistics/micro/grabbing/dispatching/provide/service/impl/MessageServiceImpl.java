package com.logistics.micro.grabbing.dispatching.provide.service.impl;

import com.logistics.micro.common.enums.ServiceExceptionEnum;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.DispatchOrderDto;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.MessageAckDto;
import com.logistics.micro.grabbing.dispatching.provide.enums.CachekeyEnum;
import com.logistics.micro.grabbing.dispatching.provide.service.DispathingService;
import com.logistics.micro.grabbing.dispatching.provide.service.MessageService;
import com.logistics.micro.grabbing.dispatching.provide.utils.RedisUtil;
import com.logistics.micro.grabbing.dispatching.provide.webSocket.WebSocketServerEndpoint;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * 接受消息
 *
 * @author xionghw0982
 * @date 2021-04-13 22:44
 */
@Component
@Slf4j
public class MessageServiceImpl implements MessageService {
    @Autowired
    DispathingService dispathingService;
    @Autowired
    //引入WebSocket
    private WebSocketServerEndpoint webSocketServerEndpoint;
    @Resource
    RedisUtil redisUtil;

    @Resource(name = "createThreadPool")
    private ExecutorService threadPool;

    private final static String ORDER_QUEUE = "order-queue";

    private static Channel local_channel = null;
    /**
     * 接受消息
     *
     * @param message
     * @param content
     * @param channel
     */
    @RabbitListener(queues = ORDER_QUEUE) //监听队列
    @Override
    public void ReceiveMessage(Message message, RabbitMessage<?> content, Channel channel) {
        log.info("----------------接收到消息--------------------"+content);
        if ( local_channel == null) {
            log.info("local_channel is already");
            local_channel = channel;
        }
        threadPool.execute(() -> {
            final DispatchOrderDto orderDto = new DispatchOrderDto();
            GsonUtils.mapToBean((Map<String, Object>) content.getBody(),orderDto);

            if (orderDto == null) {
                throw new ServiceException(ServiceExceptionEnum.NO_MESSAGE_DATA);
            }

            if (orderDto.getOrderState() > 1) {
                log.info("当前订单状态为：{}，异常",orderDto.getOrderState());
                throw new ServiceException(ServiceExceptionEnum.ORDER_STATUS_ERROR);
            }

            try {
                // 筛选司机群
                log.info("订单号为{}的开始筛选司机群",orderDto.getOrderCode());
                final List<String> targetDriver = dispathingService.dispaching(orderDto);
                //发送给WebSocket 由WebSocket推送给前端
                log.info("订单号为{}的开始推送给前端",orderDto.getOrderCode());
                webSocketServerEndpoint.sendMessageByGroupId(targetDriver, orderDto);
            } catch (ServiceException e) {
               log.warn(e.getMessage());
            } catch(Exception e) {
                log.warn(e.getMessage());
                try {
                    channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,true);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }finally {
                log.info("开始准备缓存");
                final MessageAckDto ackDto = new MessageAckDto();
                ackDto.setDeliveryTag(message.getMessageProperties().getDeliveryTag());
                ackDto.setOrderCode(orderDto.getOrderCode());
                ackDto.setOrderState(1);
                ackDto.setMessageState(0);
                redisUtil.hset(CachekeyEnum.ORDER_MESSAGE_CACHE_KEY.getKey(), orderDto.getOrderCode(), GsonUtils.toJSON(ackDto));
                log.info("订单消息已缓存");
            }
        });

    }
    public Boolean ackMessage(Long deliveryTag) {
        if(local_channel != null) {
            try {
                log.info("消息确认");
                local_channel.basicAck(deliveryTag, false);
            } catch (IOException e) {
                log.warn(e.getMessage());
                return false;
            }
            return true;
        }
        return false;
    }

    public Boolean nackMessage(Long deliveryTag) {
        try {
            log.info("消息重新入队");
            local_channel.basicNack(deliveryTag,false,true);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
