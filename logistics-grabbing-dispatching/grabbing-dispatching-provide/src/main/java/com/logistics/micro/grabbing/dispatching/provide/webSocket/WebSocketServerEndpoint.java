package com.logistics.micro.grabbing.dispatching.provide.webSocket;

import com.alibaba.fastjson.JSON;
import com.logistics.micro.grabbing.dispatching.provide.entity.dto.DispatchOrderDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/3/10 15:27
 * @description
 * WebSocket 服务配置类
 *  *  定义 userId 为当前连接(在线) WebSocket 的用户
 */

@Slf4j
@Component
@ServerEndpoint(value = "/ws/{userId}")
public class WebSocketServerEndpoint {
    private Session session; //建立连接的会话
    private String userId; //当前连接用户id   路径参数

    /**
     * 存放存活的Session集合(map保存)
     */
    private static ConcurrentHashMap<String, WebSocketServerEndpoint> livingSession = new ConcurrentHashMap<>();

    /**
     *  建立连接的回调
     *  session 建立连接的会话
     *  userId 当前连接用户id   路径参数
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("userId") String userId){
        this.session = session;
        this.userId = userId;
        livingSession.put(userId, this);

        log.info("----[ WebSocket ]---- 用户id为 : {} 的用户进入WebSocket连接 ! 当前在线人数为 : {} 人 !--------",this.userId,livingSession.size());
    }

    /**
     *  关闭连接的回调
     *  移除用户在线状态
     */
    @OnClose
    public void onClose(){
        livingSession.remove(userId);
        log.info("----[ WebSocket ]---- 用户id为 : {} 的用户退出WebSocket连接 ! 当前在线人数为 : {} 人 !--------",userId,livingSession.size());
    }

    @OnMessage
    public void onMessage(String message, Session session, @PathParam("userId") String userId) {
        log.info("--------收到用户id为 : {} 的用户发送的消息 ! 消息内容为 : {}",userId,message);
        //sendMessageToAll(userId + " : " + message);
        // TODO 收到目标反馈信息，进行相应的操作
    }

    @OnError
    public void onError(Session session, Throwable error) throws Throwable {
        log.error("----------------WebSocket发生错误----------------");
        log.error(error.getStackTrace() + "");
        throw error;
    }

    /**
     *  根据userId发送给用户
     * @param userId
     * @param message
     */
    public void sendMessageById(String userId, String message) {
        livingSession.forEach((sessionId, session) -> {
            //发给指定的接收用户
            if (userId.equals(session.userId)) {
                sendMessageBySession(session.session, message);
            }
        });
    }

    public void sendMessageByGroupId(List<String> userIDs, DispatchOrderDto orderDto) {
        livingSession.forEach((sessionId, session) ->{
            log.info("即将发送给指定用户群");
            // 在线，且是指定的用户群
            if (session.session.isOpen() && userIDs.contains(sessionId)) {
                try {
                    session.session.getAsyncRemote().sendText(JSON.toJSONString(orderDto));
                    log.info("发送成功");
                }catch (Exception e){
                    try {
                        throw new Exception(e);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }

            }
        });
    }

    /**
     *  根据Session发送消息给用户
     * @param session
     * @param message
     */
    public void sendMessageBySession(Session session, String message)  {
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("webSocket发生错误");
        }
    }

    /**
     *  给在线用户发送消息
     * @param message
     */
    public void sendMessageOnline(String message) {
        livingSession.forEach((sessionId, session) -> {
            if(session.session.isOpen()){
                sendMessageBySession(session.session, message);
            }
        });
    }
}
