package com.logistics.micro.order.api;

import com.logistics.micro.common.model.ApiResponse;


import java.util.List;
import java.util.Map;

/**
 * 司机-订单模型表
 *
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
public interface DriverOrderDataService {

    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询司机订单特征数据
     */
    Map<String,Object> queryPage(Map<String, Object> params);

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 司机订单特征数据
     * @Description: 获取单个司机订单特征数据
     */
    ApiResponse getOne(String driverCode);

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 添加成功或失败
     * @Description: 添加单个司机订单特征数据
     */
    ApiResponse addOne(String DriverOrderDataDTO);

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 添加成功或失败
     * @Description: 批量添加司机订单特征数据
     */
    ApiResponse addBatch(List<String> DriverOrderDataList);

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 删除成功或失败
     * @Description: 删除单个司机订单特征数据
     */
    ApiResponse deleteOne(String DriverOrderDataDTO);

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 删除成功或失败
     * @Description: 批量删除司机订单特征数据
     * @param DriverOrderDataList
     */
    ApiResponse deleteBatch(List<Integer> DriverOrderDataList);

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 更新成功或失败
     * @Description: 更新单个司机订单特征数据
     */
    ApiResponse updateOne(String DriverOrderDataDTO);

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 更新成功或失败
     * @Description: 批量更新司机订单特征数据
     */
    ApiResponse updateBatch(List<String> DriverOrderDataList);

    /**
     * 从抢单服务传来的司机数据，更新司机-订单模型 中距离的信息
     * @param grabbingOrderDto
     */
    ApiResponse updateDistanceData(String grabbingOrderDto);

    /**
     * 更新拒绝次数
     * @param orderRejectDto
     * @return
     */
    ApiResponse updateRejectNum(String orderRejectDto);

    /**
     * 重新计算接单率
     */
    void calculateRate();


}

