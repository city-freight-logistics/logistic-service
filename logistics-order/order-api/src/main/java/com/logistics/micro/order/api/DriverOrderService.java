package com.logistics.micro.order.api;

import com.logistics.micro.common.model.ApiResponse;


import java.util.List;
import java.util.Map;

/**
 * 司机方订单表
 *
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
public interface DriverOrderService {

    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询司机订单
     */
    Map<String,Object> queryPage(Map<String, Object> params);

    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询司机已完成订单
     */
    Map<String,Object> queryPageFinish(Map<String, Object> params);

    /**
     * 查找订单
     * @param key
     * @param driverName
     * @return
     */
    ApiResponse searchOrder(String key,String driverName);

    /***
     * @Parms 司机订单传输对象
     * @return 司机订单对象
     * @Description: 获取单个司机订单
     */
    ApiResponse getOne(String id);

    /**
     * 通过订单编号获取司机订单信息
     * @param orderCode
     * @return
     */
    ApiResponse getOneByOrderCode(String orderCode);

    /***
     * @Parms 司机订单传输对象
     * @return 添加成功或失败
     * @Description: 添加单个司机订单
     */
    ApiResponse addOne(String driverOrderDTO);

    /***
     * @Parms 司机订单传输对象
     * @return 添加成功或失败
     * @Description: 批量添加司机订单
     */
    ApiResponse addBatch(List<String> driverOrderList);

    /***
     * @Parms 司机订单传输对象
     * @return 删除成功或失败
     * @Description: 删除单个司机订单
     */
    ApiResponse deleteOne(String driverOrderDTO);

    /***
     * @Parms 司机订单传输对象
     * @return 删除成功或失败
     * @Description: 批量删除司机订单
     * @param driverOrderList
     */
    ApiResponse deleteBatch(List<Integer> driverOrderList);

    /***
     * @Parms 司机订单传输对象
     * @return 更新成功或失败
     * @Description: 更新单个司机订单
     */
    ApiResponse updateOne(String driverOrderDTO);

    /**
     * 更新司机订单中的状态
     * @param orderCode
     * @param orderState
     */
    Boolean updateOrderState(String orderCode,Integer orderState);

    /**
     * 订单运输结束后更新
     * @param driverOrderDTO
     * @return
     */
    ApiResponse updateComplete(String driverOrderDTO);

    /***
     * @Parms 司机订单传输对象
     * @return 更新成功或失败
     * @Description: 批量更新司机订单
     */
    ApiResponse updateBatch(List<String> driverOrderList);


}
