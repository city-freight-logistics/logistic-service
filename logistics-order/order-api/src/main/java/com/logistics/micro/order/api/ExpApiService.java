package com.logistics.micro.order.api;

import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * 示例dubbo接口（仅示例使用，后续删除）
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-3-23 22:12
 */
public interface ExpApiService {
    ImmutablePair demo();
}
