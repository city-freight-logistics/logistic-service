package com.logistics.micro.order.api;

import com.logistics.micro.common.model.ApiResponse;

/**
 * 意见反馈
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-26 17:16
 */
public interface FeedBackService {

    /**
     * 添加意见记录
     *
     * @param feedBackJson
     * @return
     */
    ApiResponse<?> addFeedBack(String feedBackJson);

    /**
     * 管理端查询接口
     *
     * @param queryDto
     * @return
     */
    ApiResponse<?> getFeedBackAdmin(String queryDto);
}
