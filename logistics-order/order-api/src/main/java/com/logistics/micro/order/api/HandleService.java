package com.logistics.micro.order.api;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.order.dto.AddHandleDto;
import com.logistics.micro.order.dto.HandleDto;

/**
 * 管理端申诉管理
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-29 22:45
 */
public interface HandleService {

    /**
     * 管理端获取申诉单信息
     *
     * @param handleDto
     * @return
     */
    ApiResponse<?> queryHandleList(HandleDto handleDto);

    /**
     * 申诉单审核接口
     *
     * @param handlePo
     * @return
     */
    ApiResponse<?> handleExamine(String handlePo);

    /**
     * 申诉单申请接口
     *
     * @param handleDto
     * @return
     */
    ApiResponse<?> applyHandle(AddHandleDto handleDto);

    /**
     * 订单是否存在申诉单申请记录
     *
     * @param orderCode
     * @param userId
     * @return
     */
    ApiResponse<?> verifyHandelApply(String orderCode, String userId);

    /**
     * 获取用户下的申诉单列表
     *
     * @param userId
     * @return
     */
    ApiResponse<?> getUserHandleList(String userId);
}
