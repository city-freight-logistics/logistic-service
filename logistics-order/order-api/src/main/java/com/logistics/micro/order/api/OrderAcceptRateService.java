package com.logistics.micro.order.api;

import com.logistics.micro.common.model.ApiResponse;

/**
 * 司机接单率概率表
 *
 * @author xionghw0982
 * @date 2021-04-19 09:55
 */
public interface OrderAcceptRateService {
    /**
     * 获取
     * @return
     */
    ApiResponse getRateModel();

    /**
     * 插入
     * @return
     * @param orderAcceptRate
     */
    ApiResponse insertRateModel(String orderAcceptRate);
}
