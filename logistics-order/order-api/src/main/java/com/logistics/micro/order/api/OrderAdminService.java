package com.logistics.micro.order.api;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.order.dto.AdminOrderChartDto;
import com.logistics.micro.order.dto.OrderAdminQueryDto;

/**
 * 管理端订单服务
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-27 21:57
 */
public interface OrderAdminService {

    /**
     * 管理端获取订单信息
     *
     * @param orderAdminQueryDto
     * @return
     */
    ApiResponse<?> queryOrder(OrderAdminQueryDto orderAdminQueryDto);

    /**
     * 终止订单方法接口
     * @param orderAdminPo
     * @return
     */
    ApiResponse<?> interruptOrder(String orderAdminPo);

    /**
     * 管理端首页数据展示
     * @return
     */
    ApiResponse<AdminOrderChartDto> dashBoardChart();

    /**
     * 管理端首页数据展示
     * @return
     */
    ApiResponse<AdminOrderChartDto> dashBoardChartL();
}
