package com.logistics.micro.order.api;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 司机拒绝订单表
 *
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
public interface OrderRejectService  {

    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询司机拒绝订单
     */
    Map<String,Object> queryPage(Map<String, Object> params);

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 司机拒绝订单对象
     * @Description: 获取单个司机拒绝订单
     */
    ApiResponse getOne(String id);

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 添加成功或失败
     * @Description: 添加单个司机拒绝订单
     */
    ApiResponse addOne(String OrderRejectDTO);

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 添加成功或失败
     * @Description: 批量添加司机拒绝订单
     */
    ApiResponse addBatch(List<String> OrderRejectList);

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 删除成功或失败
     * @Description: 删除单个司机拒绝订单
     */
    ApiResponse deleteOne(String OrderRejectDTO);

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 删除成功或失败
     * @Description: 批量删除司机拒绝订单
     * @param OrderRejectList
     */
    ApiResponse deleteBatch(List<Integer> OrderRejectList);

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 更新成功或失败
     * @Description: 更新单个司机拒绝订单
     */
    ApiResponse updateOne(String OrderRejectDTO);

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 更新成功或失败
     * @Description: 批量更新司机拒绝订单
     */
    ApiResponse updateBatch(List<String> OrderRejectList);
}

