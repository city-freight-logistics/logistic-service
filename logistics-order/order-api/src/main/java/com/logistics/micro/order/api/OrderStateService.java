package com.logistics.micro.order.api;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.order.OrderStatusDto;

/**
 * 用户订单同步司机订单状态
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-27 14:27
 */
public interface OrderStateService {

    /**
     * 订单数据同步接口
     * @return
     */
    ApiResponse<?> syncDriverOrderState(OrderStatusDto orderStatusDto);
}
