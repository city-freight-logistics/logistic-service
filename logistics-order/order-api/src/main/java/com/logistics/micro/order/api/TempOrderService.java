package com.logistics.micro.order.api;

import com.logistics.micro.common.model.ApiResponse;

import java.util.List;
import java.util.Map;

/**
 * 用户下单但还未接单表
 *
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
public interface TempOrderService {

    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询用户暂存订单
     */
    Map<String,Object> queryPage(Map<String, Object> params);

    /**
     * 获取全部  list to Json
     * @return
     */
    String getAll();

    /***
     * @Parms 用户暂存订单传输对象
     * @return 用户暂存订单对象
     * @Description: 获取单个用户暂存订单
     */
    ApiResponse getOne(String TempOrderID);

    /***
     * @Parms 用户暂存订单传输对象
     * @return 添加成功或失败
     * @Description: 添加单个用户暂存订单
     */
    ApiResponse addOne(String TempOrdertDTO);

    /***
     * @Parms 用户暂存订单传输对象
     * @return 添加成功或失败
     * @Description: 批量添加用户暂存订单
     */
    ApiResponse addBatch(List<String> TempOrdertList);


    /***
     * @Parms 用户暂存订单传输对象
     * @return 删除成功或失败
     * @Description: 批量删除用户暂存订单
     * @param TempOrdertList
     */
    void deleteBatch(List<String> TempOrdertList);

    /***
     * @Parms 用户暂存订单传输对象
     * @return 更新成功或失败
     * @Description: 更新单个用户暂存订单
     */
    ApiResponse updateOne(String TempOrdertDTO);

    /***
     * @Parms 用户暂存订单传输对象
     * @return 更新成功或失败
     * @Description: 批量更新用户暂存订单
     */
    ApiResponse updateBatch(List<String> TempOrdertList);
}

