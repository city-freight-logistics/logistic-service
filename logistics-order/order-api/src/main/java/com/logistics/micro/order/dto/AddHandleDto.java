package com.logistics.micro.order.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 申诉单创建dto
 *
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-30 15:09
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddHandleDto implements Serializable {
    private String handleCode;
    private String orderCode;
    private Integer userId;
    private String handleImg;
    private String handleReason;
    private String reason;
    private Integer handleState;
}
