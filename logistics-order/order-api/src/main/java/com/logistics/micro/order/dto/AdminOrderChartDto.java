package com.logistics.micro.order.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-29 11:16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdminOrderChartDto implements Serializable {
    private List<Double> amount;
    private String sum;
    private String num;
}
