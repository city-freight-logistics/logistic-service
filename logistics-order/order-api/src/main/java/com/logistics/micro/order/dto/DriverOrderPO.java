package com.logistics.micro.order.dto;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 司机方订单表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("OMS_DRIVER_ORDER")
public class DriverOrderPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 订单名称
	 */
	private String orderName;
	/**
	 * 订单执行人
	 */
	private String driverName;
	/**
	 * 订单发起人
	 */
	private String customerName;
	/**
	 * 收货人
	 */
	private String consigneeName;
	/**
	 * 发起人联系电话
	 */
	private String customerTel;
	/**
	 * 收货人联系电话
	 */
	private String consigneeTel;
	/**
	 * 运费
	 */
	private Double freightAmount;
	/**
	 * 货物分类名称
	 */
	private String typeName;
	/**
	 * 收取金额
	 */
	private String receiveAmount;
	/**
	 * 支付方式【1->支付宝；2->微信】
	 */
	private Integer payType;
	/**
	 * 订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）
	 */
	private Integer orderState;
	/**
	 * 订单评价
	 */
	private String orderComment;
	/**
	 * 订单到司机的距离
	 */
	private String distanceDriver;
	/**
	 * 订单开始日期
	 */
	private Date beginDate;
	/**
	 * 订单结束日期
	 */
	private Date completeDate;
	/**
	 * 订单备注
	 */
	private String orderRemarks;
	/**
	 * 下单位置-纬度
	 */
	private String latitude;
	/**
	 * 下单位置-经度
	 */
	private String longitude;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifyDate;
	/**
	 * 逻辑删除（0：否，1：是）
	 */
	@TableLogic
	private Integer isDelete;

}
