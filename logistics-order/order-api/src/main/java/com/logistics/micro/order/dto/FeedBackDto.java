package com.logistics.micro.order.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-26 17:28
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FeedBackDto implements Serializable {
    private String page;
    private String limit;
}
