package com.logistics.micro.order.dto;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-26 17:12
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("OMS_FEEDBACK")
public class FeedBackPo implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String userId;
    private String userName;
    private String feedback;
    private String url;
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;
}
