package com.logistics.micro.order.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 查询dto
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-29 22:39
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HandleDto implements Serializable {
    private String page;
    private String limit;
    private String handleCode;
    private String handleState;
    private String userId;
}
