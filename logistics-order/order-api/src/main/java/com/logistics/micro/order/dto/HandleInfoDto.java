package com.logistics.micro.order.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 前端返回dto
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-29 23:52
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HandleInfoDto implements Serializable {
    private Integer id;
    private String handleCode;
    private String orderCode;
    private Integer userId;
    private String handleImg;
    private String handleReason;
    private String reason;
    private Integer handleState;


    private String customerName;
    private String driverName;
    private String orderDistance;
    private String freightAmount;
    private Integer orderState;
    private String receiveAmount;
}
