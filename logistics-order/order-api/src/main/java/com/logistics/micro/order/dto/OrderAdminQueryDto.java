package com.logistics.micro.order.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-27 23:24
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderAdminQueryDto implements Serializable {
    private String page;
    private String limit;
    private String orderCode;
    private String orderType;
    private String orderState;

    public boolean isNumeric(String s) {
        if (s != null && !"".equals(s.trim())){
            return s.matches("^[0-9]*$");
        }
        return false;

    }
}
