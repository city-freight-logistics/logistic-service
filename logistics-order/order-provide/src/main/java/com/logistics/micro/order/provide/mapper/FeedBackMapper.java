package com.logistics.micro.order.provide.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.order.dto.FeedBackPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-26 17:15
 */
@Mapper
public interface FeedBackMapper extends BaseMapper<FeedBackPo> {
}
