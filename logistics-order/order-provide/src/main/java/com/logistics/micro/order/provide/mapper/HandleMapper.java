package com.logistics.micro.order.provide.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.logistics.micro.order.dto.HandleDto;
import com.logistics.micro.order.dto.HandleInfoDto;
import com.logistics.micro.order.provide.model.pojo.HandlePo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-29 22:45
 */
@Mapper
public interface HandleMapper extends BaseMapper<HandlePo> {

    IPage<HandleInfoDto> query(Page<?> page, @Param("handleDto") HandleDto handleDto);
}
