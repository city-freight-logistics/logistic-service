package com.logistics.micro.order.provide.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.common.model.order.OrderStatusDto;
import com.logistics.micro.order.provide.model.pojo.OrderAdminPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-27 23:20
 */
@Mapper
public interface OrderAdminMapper extends BaseMapper<OrderAdminPo> {

    int syncDriverOrderState(@Param("orderStatusDto") OrderStatusDto orderStatusDto);

    List<Double> dashBoardChartH(@Param("date") String date);

    List<Double> dashBoardChartL(@Param("date") String date);

    int dashBoardChartNum(@Param("date") String date);
}
