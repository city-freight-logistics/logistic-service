package com.logistics.micro.order.provide.mapper.driver;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.order.dto.DriverOrderPO;
import org.apache.ibatis.annotations.Mapper;


/**
 * 司机方订单表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Mapper
public interface DriverOrderDao extends BaseMapper<DriverOrderPO> {
}
