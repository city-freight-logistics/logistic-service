package com.logistics.micro.order.provide.mapper.driver;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.order.provide.model.pojo.DriverOrderDataPO;
import com.logistics.micro.order.provide.model.pojo.GoodsTypePO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 司机-订单模型表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Mapper
public interface DriverOrderDataDao extends BaseMapper<DriverOrderDataPO> {
    DriverOrderDataPO getAllDataSum();

    List<DriverOrderDataPO> getListLimit100();

    List<GoodsTypePO> getGoodsTypeEachCount();
}
