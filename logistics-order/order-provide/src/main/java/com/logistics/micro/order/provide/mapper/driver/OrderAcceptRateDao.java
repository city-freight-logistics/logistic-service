package com.logistics.micro.order.provide.mapper.driver;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.order.provide.model.pojo.OrderAcceptRatePO;

/**
 * 司机接单率概率表
 *
 * @author xionghw0982
 * @date 2021-04-19 10:02
 */
public interface OrderAcceptRateDao  extends BaseMapper<OrderAcceptRatePO> {
}
