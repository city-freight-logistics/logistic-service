package com.logistics.micro.order.provide.mapper.driver;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.order.provide.model.pojo.OrderRejectPO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 司机拒绝订单表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Mapper
public interface OrderRejectDao extends BaseMapper<OrderRejectPO> {
	
}
