package com.logistics.micro.order.provide.mapper.driver;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.order.provide.model.pojo.TempOrderPO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户下单但还未接单表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Mapper
public interface TempOrderDao extends BaseMapper<TempOrderPO> {
	
}
