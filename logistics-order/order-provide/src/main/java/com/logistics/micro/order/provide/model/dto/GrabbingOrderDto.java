package com.logistics.micro.order.provide.model.dto;

import lombok.Data;

/**
 * 司机发起抢单的Dto
 *
 * @author xionghw0982
 * @date 2021-04-20 17:19
 */
@Data
public class GrabbingOrderDto {
    /**
     * 司机姓名
     */
    private String driverName;
    /**
     * 司机编号
     */
    private String driverCode;
    /**
     * 车牌
     */
    private String carCode;
    /**
     * 起点到终点的距离
     */
    private String distanceOrder;
    /**
     * 订单到司机的距离
     */
    private String distanceDriver;
}
