package com.logistics.micro.order.provide.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 司机拒绝订单Dto
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Data
public class OrderRejectDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 订单名称
	 */
	private String orderName;
	/**
	 * 司机姓名
	 */
	private String driverName;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 拒绝原因
	 */
	private String reason;

}
