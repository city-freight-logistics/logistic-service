package com.logistics.micro.order.provide.model.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 司机-订单模型表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("OMS_DRIVER_ORDER_DATA")
public class DriverOrderDataPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 车牌号
	 */
	private String carCode;
	/**
	 * 接驾里程
	 */
	private Double mileage;
	/**
	 * 起点到终点的距离[0,1)
	 */
	@TableField("distance_order_0_1")
	private Integer distanceOrder01;
	/**
	 * 起点到终点的距离[1,3)
	 */
	@TableField("distance_order_1_3")
	private Integer distanceOrder13;
	/**
	 * 起点到终点的距离[3,5)
	 */
	@TableField("distance_order_3_5")
	private Integer distanceOrder35;
	/**
	 * 起点到终点的距离[5,10)
	 */
	@TableField("distance_order_5_10")
	private Integer distanceOrder510;
	/**
	 * 起点到终点的距离[10,+)
	 */
	@TableField("distance_order_10_more")
	private Integer distanceOrder10More;
	/**
	 * 订单与司机的距离[0,0.5)
	 */
	@TableField("distance_driver_0_05")
	private Integer distanceDriver005;
	/**
	 * 订单与司机的距离[0.5,2)
	 */
	@TableField("distance_driver_05_2")
	private Integer distanceDriver052;
	/**
	 * 订单与司机的距离[2,5)
	 */
	@TableField("distance_driver_2_5")
	private Integer distanceDriver25;
	/**
	 * 订单与司机的距离[5,10)
	 */
	@TableField("distance_driver_5_10")
	private Integer distanceDriver510;
	/**
	 * 订单与司机的距离[10,+)
	 */
	@TableField("distance_driver_10_more")
	private Integer distanceDriver10More;
	/**
	 * 历史接单数
	 */
	@TableField("count_order")
	private Integer countOrder;
	/**
	 * 拒绝订单数
	 */
	@TableField("count_reject")
	private Integer countReject;
	/**
	 * 接受订单数
	 */
	@TableField("count_accept")
	private Integer countAccept;
	/**
	 * 历史接单率
	 */
	@TableField("history_rate")
	private Double historyRate;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifyDate;

}
