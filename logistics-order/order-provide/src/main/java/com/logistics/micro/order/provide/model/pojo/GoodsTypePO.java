package com.logistics.micro.order.provide.model.pojo;

import lombok.Data;

/**
 * 从司机表中获取货物相关的信息
 *
 * @author xionghw0982
 * @date 2021-04-30 15:37
 */
@Data
public class GoodsTypePO {
    /**
     * 货物类别编号
     */
    private Integer id;

    /**
     * 这类编号的数量
     */
    private Integer count;

    /**
     * 概率
     */
    private Double acceptRate;
}
