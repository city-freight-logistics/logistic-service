package com.logistics.micro.order.provide.model.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-29 22:40
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("OMS_HANDEL")
public class HandlePo implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String handleCode;
    private String orderCode;
    private Integer userId;
    private String handleImg;
    private String handleReason;
    private String reason;
    private Integer handleState;
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;
}
