package com.logistics.micro.order.provide.model.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 司机接单率表  
 * 
 * @author hj
 * @date 2021-04-19 09:45:51
 */
@Data
@TableName("OMS_ORDER_ACCEPT_RATE")
public class OrderAcceptRatePO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 订单起点到终点的距离概率[0,1)
	 */
	@TableField("distance_order_rate0_1")
	private Double distanceOrderRate01;
	/**
	 * 订单起点到终点的距离概率[1,3)
	 */
	@TableField("distance_order_rate1_3")
	private Double distanceOrderRate13;
	/**
	 * 订单起点到终点的距离概率[3,5)
	 */
	@TableField("distance_order_rate3_5")
	private Double distanceOrderRate35;
	/**
	 * 订单起点到终点的距离概率[5,10)
	 */
	@TableField("distance_order_rate5_10")
	private Double distanceOrderRate510;
	/**
	 * 订单起点到终点的距离概率[10,∞)
	 */
	@TableField("distance_order_rate10_more")
	private Double distanceOrderRate10More;
	/**
	 * 订单与司机的距离概率[0,0.5)
	 */
	@TableField("distance_driver_rate0_05")
	private Double distanceDriverRate005;
	/**
	 * 订单与司机的距离概率[0.5,2)
	 */
	@TableField("distance_driver_rate05_2")
	private Double distanceDriverRate052;
	/**
	 * 订单与司机的距离概率[2,5)
	 */
	@TableField("distance_driver_rate2_5")
	private Double distanceDriverRate25;
	/**
	 * 订单与司机的距离概率[5,10)
	 */
	@TableField("distance_driver_rate5_10")
	private Double distanceDriverRate510;
	/**
	 * 订单与司机的距离概率[10,∞)
	 */
	@TableField("distance_driver_rate10_more")
	private Double distanceDriverRate10More;
	/**
	 * 历史接单数概率[0,100)
	 */
	@TableField("history_count_0_100")
	private Double historyCount0100;
	/**
	 * 历史接单数概率[100,300)
	 */
	@TableField("history_count_100_300")
	private Double historyCount100300;
	/**
	 * 历史接单数概率[300,500)
	 */
	@TableField("history_count_300_500")
	private Double historyCount300500;
	/**
	 * 历史接单数概率[500,1000)
	 */
	@TableField("history_count_500_1000")
	private Double historyCount5001000;
	/**
	 * 历史接单数概率[1000,∞)
	 */
	@TableField("history_count_1000_more")
	private Double historyCount1000More;
	/**
	 * 历史接单率的概率[0,10)
	 */
	@TableField("history_rate_0_10")
	private Double historyRate010;
	/**
	 * 历史接单率的概率[10,30)
	 */
	@TableField("history_rate_10_30")
	private Double historyRate1030;
	/**
	 * 历史接单率的概率[30,50)
	 */
	@TableField("history_rate_30_50")
	private Double historyRate3050;
	/**
	 * 历史接单率的概率[50,80)
	 */
	@TableField("history_rate_50_80")
	private Double historyRate5080;
	/**
	 * 历史接单率的概率[80,100)
	 */
	@TableField("history_rate_80_100")
	private Double historyRate80100;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;

}
