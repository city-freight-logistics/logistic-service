package com.logistics.micro.order.provide.model.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-27 22:43
 */
@TableName("OMS_DEMAND_ORDER")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderAdminPo implements Serializable {
    private Integer id;
    private String orderCode;
    private String orderName;
    private String customerName;
    private String orderType;
    private String driverName;
    private String orderDistance;
    private String freightAmount;
    private Integer orderState;
    private Date beginDate;
    private Date completeDate;
    private String receiveAmount;
}
