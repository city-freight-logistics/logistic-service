package com.logistics.micro.order.provide.model.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 司机拒绝订单表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("OMS_ORDER_REJECT")
public class OrderRejectPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 订单名称
	 */
	private String orderName;
	/**
	 * 司机姓名
	 */
	private String driverName;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 拒绝原因
	 */
	private String reason;
	/**
	 * 拒绝时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;

}
