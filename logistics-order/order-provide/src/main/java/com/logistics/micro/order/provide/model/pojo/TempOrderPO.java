package com.logistics.micro.order.provide.model.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户下单但还未接单表
 * 
 * @author xionghw
 * @date 2021-03-26 15:51:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("OMS_TEMP_ORDER")
public class TempOrderPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifyDate;
	/**
	 * 是否完成（0：否，1：是）
	 */
	@TableLogic
	private Integer isDelete;

}
