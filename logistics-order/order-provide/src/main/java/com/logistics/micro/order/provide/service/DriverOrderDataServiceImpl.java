package com.logistics.micro.order.provide.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.enums.ServiceExceptionEnum;
import com.logistics.micro.common.exception.ServiceException;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.order.api.DriverOrderDataService;
import com.logistics.micro.order.api.DriverOrderService;
import com.logistics.micro.order.api.OrderAcceptRateService;
import com.logistics.micro.order.provide.mapper.driver.DriverOrderDataDao;
import com.logistics.micro.order.provide.model.dto.GrabbingOrderDto;
import com.logistics.micro.order.provide.model.dto.OrderRejectDto;
import com.logistics.micro.order.provide.model.pojo.DriverOrderDataPO;
import com.logistics.micro.order.provide.model.pojo.GoodsTypePO;
import com.logistics.micro.order.provide.model.pojo.OrderAcceptRatePO;
import com.logistics.micro.order.provide.utils.RedisUtil;
import com.logistics.micro.owner.manage.api.OwnerManageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;


/**
 * 司机-订单模型
 */
@Service("driverOrderDataService")
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class DriverOrderDataServiceImpl extends ServiceImpl<DriverOrderDataDao, DriverOrderDataPO> implements DriverOrderDataService {
    @Resource
    RedisUtil redisUtil;
    @Autowired
    OrderAcceptRateService orderAcceptRateService;
    @Autowired
    DriverOrderService driverOrderService;
    @Reference
    OwnerManageService ownerManageService;

    @Override
    public Map<String,Object> queryPage(Map<String, Object> params) {
        IPage<DriverOrderDataPO> page = this.page(
                new Query<DriverOrderDataPO>().getPage(params),
                new QueryWrapper<DriverOrderDataPO>()
        );
        final Map<String, Object> map = GsonUtils.beanToMap(new PageUtils(page));
        return map;
    }

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 司机订单特征数据
     * @Description: 获取单个司机订单特征数据
     * @param driverCode
     */
    @Override
    public ApiResponse getOne(String driverCode) {
        final DriverOrderDataPO dataPO = this.getOne(Wrappers.<DriverOrderDataPO>lambdaQuery().eq(DriverOrderDataPO::getDriverCode, driverCode));
        return new ApiResponse(GsonUtils.toJSON(dataPO));
    }

    /***
     * @Parms 司机订单特征数据传输对象
     * @Description: 添加单个司机订单特征数据
     * @param DriverOrderDataDTO
     * @return
     */
    @Override
    public ApiResponse addOne(String DriverOrderDataDTO) {
        final DriverOrderDataPO driverOrderDataPO = GsonUtils.fromJson(DriverOrderDataDTO, DriverOrderDataPO.class);
        DriverOrderDataPO orderDataPO = this.getOne(new QueryWrapper<DriverOrderDataPO>().eq("driver_code", driverOrderDataPO.getDriverCode()));
        if (!Objects.isNull(orderDataPO)) {
            return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
        }

        final boolean save = this.save(driverOrderDataPO);
        if (save){
            return new ApiResponse("添加成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"添加失败");
    }

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 添加成功或失败
     * @Description: 批量添加司机订单特征数据
     * @param DriverOrderDataList
     */
    @Override
    public ApiResponse addBatch(List<String> DriverOrderDataList) {
        return null;
    }

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 删除成功或失败
     * @Description: 删除单个司机订单特征数据
     * @param DriverOrderDataDTO
     */
    @Override
    public ApiResponse deleteOne(String DriverOrderDataDTO) {
        return null;
    }

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 删除成功或失败
     * @Description: 批量删除司机订单特征数据
     * @param DriverOrderDataList
     */
    @Override
    public ApiResponse deleteBatch(List<Integer> DriverOrderDataList) {
        final boolean remove = this.removeByIds(DriverOrderDataList);
        if (remove){
            return new ApiResponse("删除成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"删除失败");
    }

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 更新成功或失败
     * @Description: 更新单个司机订单特征数据
     * @param DriverOrderDataDTO
     */
    @Override
    public ApiResponse updateOne(String DriverOrderDataDTO) {
        final DriverOrderDataPO driverOrderDataPO = GsonUtils.fromJson(DriverOrderDataDTO, DriverOrderDataPO.class);

        final boolean update = this.updateById(driverOrderDataPO);
        if (update){
            return new ApiResponse("更新成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"更新失败");
    }

    /***
     * @Parms 司机订单特征数据传输对象
     * @return 更新成功或失败
     * @Description: 批量更新司机订单特征数据
     * @param DriverOrderDataList
     */
    @Override
    public ApiResponse updateBatch(List<String> DriverOrderDataList) {
        return null;
    }

    /**
     * 从抢单服务传来的司机数据，更新司机-订单模型 中距离的信息
     *
     * @param grabbingOrderDto
     */
    @Override
    public ApiResponse updateDistanceData(String grabbingOrderDto) {
        final GrabbingOrderDto orderDto = GsonUtils.fromJson(grabbingOrderDto, GrabbingOrderDto.class);
        String in = String.valueOf(redisUtil.get("updateDistanceData-" + orderDto.getDriverCode()));
        if ("in".equals(in)) {
            return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
        }
        redisUtil.set("updateDistanceData-" + orderDto.getDriverCode(),"in", 3);
        final Double distanceDriver = Double.parseDouble(orderDto.getDistanceDriver());
        final Double distanceOrder = Double.parseDouble(orderDto.getDistanceOrder());
        log.info("获取编号为{}的司机数据",orderDto.getDriverCode());
        final ApiResponse one = this.getOne(orderDto.getDriverCode());
        DriverOrderDataPO orderDataPO = GsonUtils.fromJson(String.valueOf(one.getBody()),DriverOrderDataPO.class) ;
        setDistanceData(distanceDriver,distanceOrder,orderDataPO);
        final boolean b = updateById(orderDataPO);
        if (!b) {
            throw new ServiceException(ServiceExceptionEnum.UPDATE_ERROR);
        }
        return new ApiResponse(HeaderStatus.SUCCESS);
    }

    /**
     * 更新拒绝次数
     *
     * @param orderRejectDto
     * @return
     */
    @Override
    public ApiResponse updateRejectNum(String orderRejectDto) {
        log.info("更新司机-订单数据模型 司机拒绝接单数和历史接单率");
        final OrderRejectDto orderDto = GsonUtils.fromJson(orderRejectDto, OrderRejectDto.class);
        String in = String.valueOf(redisUtil.get("updateRejectNum-" + orderDto.getDriverCode()));
        if ("in".equals(in)) {
            return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
        }
        redisUtil.set("updateRejectNum-" + orderDto.getDriverCode(),"in", 3);

        final ApiResponse one = this.getOne(orderDto.getDriverCode());
        DriverOrderDataPO orderDataPO = GsonUtils.fromJson(String.valueOf(one.getBody()), DriverOrderDataPO.class);
        Double historyRate = (double)orderDataPO.getCountAccept() /( orderDataPO.getCountOrder() + 1) * 100;
        this.lambdaUpdate().set(DriverOrderDataPO::getCountReject, orderDataPO.getCountReject() + 1)
                .set(DriverOrderDataPO::getCountOrder, orderDataPO.getCountOrder() + 1)
                .set(DriverOrderDataPO::getHistoryRate,historyRate)
                .eq(DriverOrderDataPO::getDriverCode, orderDto.getDriverCode())
                .update();
        log.info("更新完成");
        return new ApiResponse(HeaderStatus.SUCCESS);
    }


    /**
     * 重新计算接单率
     */
    @Override
    public void calculateRate() {
        final DriverOrderDataPO dataPO = this.getBaseMapper().getAllDataSum();
        final List<DriverOrderDataPO> dataDaos = this.getBaseMapper().getListLimit100();
        final List<GoodsTypePO> goodsList = this.getBaseMapper().getGoodsTypeEachCount();
        OrderAcceptRatePO orderAcceptRatePO = new OrderAcceptRatePO();
        initAcceptOrderRateOfDistance(orderAcceptRatePO,dataPO);
        initAcceptOrderRateOfhistory(orderAcceptRatePO, dataDaos);
        initAcceptOrderRateOfGoodsType(goodsList);
        iniAcceptOrderRateofCount(orderAcceptRatePO,dataDaos);
        orderAcceptRateService.insertRateModel(GsonUtils.toJSON(orderAcceptRatePO));

    }

    /**
     * 重新计算与距离的接单率
     * @param orderAcceptRatePO
     * @param dataPO
     */
    private void initAcceptOrderRateOfDistance(OrderAcceptRatePO orderAcceptRatePO,DriverOrderDataPO dataPO) {
        Integer sumOrderDistance = dataPO.getDistanceOrder01() + dataPO.getDistanceOrder13() + dataPO.getDistanceOrder35()
                + dataPO.getDistanceOrder510() + dataPO.getDistanceOrder10More();
        Integer sumDriverDistance = dataPO.getDistanceDriver005() + dataPO.getDistanceDriver052() + dataPO.getDistanceDriver25()
                + dataPO.getDistanceDriver510() + dataPO.getDistanceDriver10More();

        orderAcceptRatePO.setDistanceOrderRate01((double)dataPO.getDistanceOrder01() / sumOrderDistance * 100.0);
        orderAcceptRatePO.setDistanceOrderRate13((double)dataPO.getDistanceOrder13() / sumOrderDistance * 100.0);
        orderAcceptRatePO.setDistanceOrderRate35((double)dataPO.getDistanceOrder35() / sumOrderDistance * 100.0);
        orderAcceptRatePO.setDistanceOrderRate510((double)dataPO.getDistanceOrder510() / sumOrderDistance * 100.0);
        orderAcceptRatePO.setDistanceOrderRate10More((double)dataPO.getDistanceOrder10More() / sumOrderDistance * 100.0);

        orderAcceptRatePO.setDistanceDriverRate005((double)dataPO.getDistanceDriver005() / sumDriverDistance * 100.0);
        orderAcceptRatePO.setDistanceDriverRate052((double)dataPO.getDistanceDriver052() / sumDriverDistance * 100.0);
        orderAcceptRatePO.setDistanceDriverRate25((double)dataPO.getDistanceDriver25() / sumDriverDistance * 100.0);
        orderAcceptRatePO.setDistanceDriverRate510((double)dataPO.getDistanceDriver510() / sumDriverDistance * 100.0);
        orderAcceptRatePO.setDistanceDriverRate10More((double)dataPO.getDistanceDriver10More() / sumDriverDistance * 100.0);
        log.info("重新计算订单距离的概率完毕");
    }

    /**
     * 重新计算与历史接单率、历史接单数、接驾里程相关的的接单率
     * @param orderAcceptRatePO
     * @param dataDaos
     */
    private void initAcceptOrderRateOfhistory(OrderAcceptRatePO orderAcceptRatePO, List<DriverOrderDataPO> dataDaos) {
        log.info("开始重新计算历史接单率的概率参数");

        final DoubleSummaryStatistics historyRate80100 = dataDaos.stream()
                .map(s -> s.getHistoryRate()).filter(i -> i > 80.0)
                .mapToDouble((x) -> x)
                .summaryStatistics();
        final DoubleSummaryStatistics historyRate5080 = dataDaos.stream()
                .map(s -> s.getHistoryRate()).filter(i -> i <= 80.0 && i >50.0)
                .mapToDouble((x) -> x)
                .summaryStatistics();
        final DoubleSummaryStatistics historyRate3050 = dataDaos.stream()
                .map(s -> s.getHistoryRate()).filter(i -> i <= 50.0 && i >30.0)
                .mapToDouble((x) -> x)
                .summaryStatistics();
        final DoubleSummaryStatistics historyRate1030 = dataDaos.stream()
                .map(s -> s.getHistoryRate()).filter(i -> i <= 30.0 && i > 10.0)
                .mapToDouble((x) -> x)
                .summaryStatistics();
        final DoubleSummaryStatistics historyRate010 = dataDaos.stream()
                .map(s -> s.getHistoryRate()).filter(i -> i <= 10.0 && i >0)
                .mapToDouble((x) -> x)
                .summaryStatistics();
        final Double newHistoryRate80100 = 1 - historyRate80100.getSum() / historyRate80100.getCount() * 100.0;
        final Double newHistoryRate5080 = 1 -  historyRate5080.getSum() / historyRate5080.getCount() * 100.0;
        final Double newHistoryRate3050 = 1 - historyRate3050.getSum() / historyRate3050.getCount() * 100.0;
        final Double newHistoryRate1030 = 1 - historyRate1030.getSum() / historyRate1030.getCount() * 100.0;
        final Double newHistoryRate010 = 1 - historyRate010.getSum() / historyRate010.getCount() * 100.0;

        orderAcceptRatePO.setHistoryRate010(newHistoryRate010);
        orderAcceptRatePO.setHistoryRate1030(newHistoryRate1030);
        orderAcceptRatePO.setHistoryRate3050(newHistoryRate3050);
        orderAcceptRatePO.setHistoryRate5080(newHistoryRate5080);
        orderAcceptRatePO.setHistoryRate80100(newHistoryRate80100);
        log.info("重新计算历史接单率的概率完毕");
    }

    private void initAcceptOrderRateOfGoodsType(List<GoodsTypePO> goodsList) {
        Integer total = 0;
        for(GoodsTypePO goods : goodsList){
            total += goods.getCount();
        }

        for(GoodsTypePO goods : goodsList){
            goods.setAcceptRate((double) (goods.getCount() / total));
        }
        ownerManageService.updateGoodsTypeById(goodsList);
        log.info("货物计算的概率计算完成");
    }

    private void iniAcceptOrderRateofCount(OrderAcceptRatePO orderAcceptRatePO,List<DriverOrderDataPO> dataDaos) {
        final IntSummaryStatistics count1000More = dataDaos.stream().map(s -> s.getCountOrder())
                .filter(i -> i >= 1000).mapToInt((x) -> x)
                .summaryStatistics();

        final IntSummaryStatistics count5001000 = dataDaos.stream().map(s -> s.getCountOrder())
                .filter(i -> i >= 500 && i < 1000)
                .mapToInt(x -> x)
                .summaryStatistics();

        final IntSummaryStatistics count300500 = dataDaos.stream().map(s -> s.getCountOrder())
                .filter(i -> i >= 300 && i < 500)
                .mapToInt(x -> x)
                .summaryStatistics();
        final IntSummaryStatistics count100300 = dataDaos.stream().map(s -> s.getCountOrder())
                .filter(i -> i >= 100 && i < 300)
                .mapToInt(x -> x)
                .summaryStatistics();
        final IntSummaryStatistics count0100 = dataDaos.stream().map(s -> s.getCountOrder())
                .filter(i -> i >= 0 && i < 300)
                .mapToInt(x -> x)
                .summaryStatistics();

        final Double newCountRate1000More = Math.log(count1000More.getAverage()) / Math.log(count1000More.getMax());
        final Double newCountRate5001000 = Math.log(count5001000.getAverage()) / Math.log(1500);
        final Double newCountRate300500 = Math.log(count300500.getAverage()) / Math.log(1500);
        final Double newCountRate100300 = Math.log(count100300.getAverage()) / Math.log(1500);
        final Double newCountRate0100 = Math.log(count0100.getAverage()) / Math.log(1500);

        orderAcceptRatePO.setHistoryCount1000More(newCountRate1000More);
        orderAcceptRatePO.setHistoryCount5001000(newCountRate5001000);
        orderAcceptRatePO.setHistoryCount300500(newCountRate300500);
        orderAcceptRatePO.setHistoryCount100300(newCountRate100300);
        orderAcceptRatePO.setHistoryCount0100(newCountRate0100);
        log.info("重新计算历史接单数的概率完毕");
    }
    /**
     * 初始化距离相关数据
     * @param distanceDriver
     * @param distanceOrder
     * @param orderDataPO
     */
    private void setDistanceData(Double distanceDriver,Double distanceOrder,DriverOrderDataPO orderDataPO) {
        if (distanceDriver < 0.5) {
            orderDataPO.setDistanceDriver005(orderDataPO.getDistanceDriver005() + 1);
        }else if (distanceDriver < 2.0) {
            orderDataPO.setDistanceDriver052(orderDataPO.getDistanceDriver052() + 1);
        }else if (distanceDriver < 5.0) {
            orderDataPO.setDistanceDriver25(orderDataPO.getDistanceDriver25() + 1);
        }else if (distanceDriver < 10.0) {
            orderDataPO.setDistanceDriver510(orderDataPO.getDistanceDriver510() + 1);
        }else {
            orderDataPO.setDistanceDriver10More(orderDataPO.getDistanceDriver10More() + 1);
        }

        if (distanceOrder < 1.0) {
            orderDataPO.setDistanceOrder01(orderDataPO.getDistanceOrder01() + 1);
        }else if (distanceOrder < 3.0) {
            orderDataPO.setDistanceOrder13(orderDataPO.getDistanceOrder13() + 1);
        }else if (distanceOrder < 5.0) {
            orderDataPO.setDistanceOrder35(orderDataPO.getDistanceOrder35() + 1);
        }else if (distanceOrder < 10.0) {
            orderDataPO.setDistanceOrder510(orderDataPO.getDistanceOrder510() + 1);
        }else {
            orderDataPO.setDistanceOrder10More(orderDataPO.getDistanceOrder10More() + 1);
        }

        orderDataPO.setMileage(orderDataPO.getMileage() + distanceOrder);
        orderDataPO.setCountAccept(orderDataPO.getCountAccept() + 1);
        orderDataPO.setCountOrder(orderDataPO.getCountOrder() + 1);
        orderDataPO.setHistoryRate((double) (orderDataPO.getCountAccept() / orderDataPO.getCountOrder()) * 100.0);
    }


}