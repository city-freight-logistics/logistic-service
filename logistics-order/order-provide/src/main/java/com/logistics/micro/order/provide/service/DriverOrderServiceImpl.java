package com.logistics.micro.order.provide.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.order.api.DriverOrderService;
import com.logistics.micro.order.provide.mapper.driver.DriverOrderDao;
import com.logistics.micro.order.dto.DriverOrderPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 司机方订单
 */
@Service("driverOrderService")
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class DriverOrderServiceImpl extends ServiceImpl<DriverOrderDao, DriverOrderPO> implements DriverOrderService {


    @Override
    public Map<String,Object> queryPage(Map<String, Object> params) {
        final PageUtils map = getPageFromDB(params);
        return GsonUtils.beanToMap(map);
    }

    /***
     * @Parms 分页查询条件
     * @return
     * @Description: 分页查询司机已完成订单
     * @param params
     */
    @Override
    public Map<String, Object> queryPageFinish(Map<String, Object> params) {
        String driverName = String.valueOf(params.get("driverName"));
        return GsonUtils.beanToMap(new PageUtils(this.page(
                new Query<DriverOrderPO>().getPage(params),
                Wrappers.<DriverOrderPO>lambdaQuery()
                        .eq(DriverOrderPO::getDriverName,driverName)
                        .between(DriverOrderPO::getOrderState,5,8)
        )));
    }

    /**
     * 查找订单
     *
     * @param key
     * @return
     */
    @Override
    public ApiResponse searchOrder(String key,String driverName) {
        final QueryWrapper<DriverOrderPO> condition = new QueryWrapper<>();
        if (StringUtils.isBlank(key)) {
            condition.like("order_name","");
        }
        condition.eq("driver_name",driverName)
                .like("order_name",key)
                .or(i -> i.like("consignee_name",key));
        final List<DriverOrderPO> list = this.list(condition);
        return new ApiResponse(list);
    }

    private PageUtils getPageFromDB(Map<String, Object> params) {
        String driverName = String.valueOf(params.get("driverName"));
        String orderState = String.valueOf(params.get("orderState")) ;
        IPage<DriverOrderPO> page = this.page(
                new Query<DriverOrderPO>().getPage(params),
                Wrappers.<DriverOrderPO>lambdaQuery()
                        .eq(DriverOrderPO::getDriverName,driverName)
                        .between(DriverOrderPO::getOrderState,orderState,4)
        );
        return new PageUtils(page);
    }

    /***
     * @Parms 司机订单传输对象
     * @return 司机订单对象
     * @Description: 获取单个司机订单
     * @param id
     */
    @Override
    public ApiResponse getOne(String id) {
        final DriverOrderPO driverOrderPO = this.getById(id);
        if (driverOrderPO == null) {
            return new ApiResponse(HeaderStatus.ERROR,"获取失败");
        }
        return new ApiResponse(driverOrderPO);
    }

    /**
     * 通过订单编号获取司机订单信息
     *
     * @param orderCode
     * @return
     */
    @Override
    public ApiResponse getOneByOrderCode(String orderCode) {
        final DriverOrderPO one = this.getOne(Wrappers.<DriverOrderPO>lambdaQuery().eq(DriverOrderPO::getOrderCode, orderCode));
        return new ApiResponse(one);
    }

    /***
     * @Parms 司机订单传输对象
     * @return 添加成功或失败
     * @Description: 添加单个司机订单
     * @param driverOrderDTO
     */
    @Override
    public ApiResponse addOne(String driverOrderDTO) {
        final DriverOrderPO driverOrderPO = GsonUtils.fromJson(driverOrderDTO, DriverOrderPO.class);

        DriverOrderPO orderPO = this.getOne(new QueryWrapper<DriverOrderPO>().eq("order_code", driverOrderPO.getOrderCode()));
        if (!Objects.isNull(orderPO)) {
            return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
        }

        final boolean save = this.save(driverOrderPO);
        if (save) {
            return new ApiResponse("添加成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"添加失败");
    }

    /***
     * @Parms 司机订单传输对象
     * @return 添加成功或失败
     * @Description: 批量添加司机订单
     * @param driverOrderList
     */
    @Override
    public ApiResponse addBatch(List<String> driverOrderList) {
        return null;
    }

    /***
     * @Parms 司机订单传输对象
     * @return 删除成功或失败
     * @Description: 删除单个司机订单
     * @param driverOrderDTO
     */
    @Override
    public ApiResponse deleteOne(String driverOrderDTO) {
        return null;
    }

    /***
     * @Parms 司机订单传输对象
     * @return 删除成功或失败
     * @Description: 批量删除司机订单
     * @param driverOrderList
     */
    @Override
    public ApiResponse deleteBatch(List<Integer> driverOrderList) {
        final boolean remove = this.removeByIds(driverOrderList);
        if (remove) {
            return new ApiResponse("删除成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"删除失败");
    }

    /***
     * @Parms 司机订单传输对象
     * @return 更新成功或失败
     * @Description: 更新单个司机订单
     * @param driverOrderDTO
     */
    @Override
    public ApiResponse updateOne(String driverOrderDTO) {
        final DriverOrderPO driverOrderPO = GsonUtils.fromJson(driverOrderDTO, DriverOrderPO.class);
        final boolean update = this.updateById(driverOrderPO);
        if (update) {
            return new ApiResponse("更新成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"更新失败");
    }

    /**
     * 更新司机订单中的状态
     *  @param orderCode
     * @param orderState
     * @return
     */
    @Override
    public Boolean updateOrderState(String orderCode, Integer orderState) {
        final boolean update = this.lambdaUpdate().set(DriverOrderPO::getOrderState, orderState)
                .eq(DriverOrderPO::getOrderCode, orderCode)
                .update();
        return update;
    }

    /**
     * 订单运输结束后更新
     *
     * @param driverOrderDTO
     * @return
     */
    @Override
    public ApiResponse updateComplete(String driverOrderDTO) {
        final DriverOrderPO driverOrderPO = GsonUtils.fromJson(driverOrderDTO, DriverOrderPO.class);
        final boolean update = this.lambdaUpdate()
                .set(DriverOrderPO::getOrderState, driverOrderPO.getOrderState())
                .set(DriverOrderPO::getCompleteDate, driverOrderPO.getCompleteDate())
                .eq(DriverOrderPO::getOrderCode, driverOrderPO.getOrderCode())
                .update();
        if (update) {
            return new ApiResponse("更新成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"更新失败");
    }

    /***
     * @Parms 司机订单传输对象
     * @return 更新成功或失败
     * @Description: 批量更新司机订单
     * @param driverOrderList
     */
    @Override
    public ApiResponse updateBatch(List<String> driverOrderList) {
        return null;
    }



}