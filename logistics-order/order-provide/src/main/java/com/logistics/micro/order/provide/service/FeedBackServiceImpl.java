package com.logistics.micro.order.provide.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.order.api.FeedBackService;
import com.logistics.micro.order.dto.FeedBackDto;
import com.logistics.micro.order.dto.FeedBackPo;
import com.logistics.micro.order.dto.HandleInfoDto;
import com.logistics.micro.order.provide.mapper.FeedBackMapper;
import com.logistics.micro.order.provide.model.pojo.HandlePo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;

import java.util.Map;
import java.util.Objects;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-26 17:16
 */
@Service
@Slf4j
public class FeedBackServiceImpl extends ServiceImpl<FeedBackMapper, FeedBackPo> implements FeedBackService {

    @Override
    public ApiResponse<?> addFeedBack(String feedBackJson) {
        try {
            FeedBackPo feedBackPo = GsonUtils.fromJson(feedBackJson, FeedBackPo.class);
            return this.count(new QueryWrapper<FeedBackPo>()
                    .eq("user_id", feedBackPo.getUserId())
                    .eq("feedback", feedBackPo.getFeedback())
                    .eq("url", feedBackPo.getUrl())) > 0 ?
                    new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT) :
                    this.save(feedBackPo) ? new ApiResponse<>(HeaderStatus.SUCCESS) : new ApiResponse<>(HeaderStatus.ERROR);
        } catch (Exception e) {
            log.info("FeedBackServiceImpl -> addFeedBack ret={} exception={}", feedBackJson, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public ApiResponse<?> getFeedBackAdmin(String queryDto) {
        try {
            if (StringUtils.isEmpty(queryDto)) {
                return new ApiResponse<>(HeaderStatus.INPUT_DATA_NULL);
            }
            Map<String, Object> params = GsonUtils.strToMap(GsonUtils.toJSON(GsonUtils.fromJson(queryDto, FeedBackDto.class)));
            IPage<FeedBackPo> page = getPageFromDB(params);
            final PageUtils pageUtils = new PageUtils(page);
            if (pageUtils.getTotalCount() == 0){
                return new ApiResponse<>(HeaderStatus.PAGE_CONTEXT_NULL_INFO);
            }
            return new ApiResponse<>(GsonUtils.beanToMap(pageUtils));
        } catch (Exception e) {
            log.info("FeedBackServiceImpl -> getFeedBackAdmin() ret={} exception={}", queryDto, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    private IPage<FeedBackPo> getPageFromDB(Map<String, Object> params) {
        // 没有命中，查db
        log.info("组装订单分页条件");
        final QueryWrapper<FeedBackPo> condition = new QueryWrapper<>();
        condition.orderByDesc("id");
        return this.page(
                new Query<FeedBackPo>().getPage(params),
                condition
        );
    }

}
