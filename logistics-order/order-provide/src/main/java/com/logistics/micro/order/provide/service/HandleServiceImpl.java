package com.logistics.micro.order.provide.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.common.utils.SnowflakeIdUtils;
import com.logistics.micro.order.api.HandleService;
import com.logistics.micro.order.dto.AddHandleDto;
import com.logistics.micro.order.dto.HandleDto;
import com.logistics.micro.order.dto.HandleInfoDto;
import com.logistics.micro.order.provide.mapper.HandleMapper;
import com.logistics.micro.order.provide.model.pojo.HandlePo;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Objects;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-29 22:45
 */
@Slf4j
@Service
@org.springframework.stereotype.Service
public class HandleServiceImpl extends ServiceImpl<HandleMapper, HandlePo> implements HandleService {

    @Autowired
    private HandleMapper handleMapper;

    @Override
    public ApiResponse<?> queryHandleList(HandleDto handleDto) {
        try {
            log.info("HandleServiceImpl -> queryHandleList start ret={}", handleDto);
            if (Objects.isNull(handleDto)) {
                return new ApiResponse<>(HeaderStatus.INPUT_DATA_NULL);
            }
            Map<String, Object> params = GsonUtils.strToMap(GsonUtils.toJSON(handleDto));
//            IPage<HandlePo> page = getPageFromDB(params);
            IPage<HandleInfoDto> query = handleMapper.query((Page<?>) new Query<HandlePo>().getPage(params), handleDto);
            final PageUtils pageUtils = new PageUtils(query);
            if (pageUtils.getTotalCount() == 0){
                return new ApiResponse<>(HeaderStatus.PAGE_CONTEXT_NULL_INFO);
            }
            return new ApiResponse<>(GsonUtils.beanToMap(pageUtils));
        } catch (Exception e) {
            log.info("HandleServiceImpl -> queryHandleList() ret={} exception={}",handleDto.toString(), e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public ApiResponse<?> handleExamine(String handlePo) {
        try {
            log.info("HandleServiceImpl -> handleExamine start ret={}", handlePo);
            HandlePo handle = GsonUtils.fromJson(handlePo, HandlePo.class);
            HandlePo handleExit = this.getOne(new QueryWrapper<HandlePo>().eq("handle_code", handle.getHandleCode()));
            if (Objects.isNull(handleExit)) {
                return new ApiResponse<>(HeaderStatus.QUERY_EMPTY);
            }
            if (handleExit.getHandleState().equals(handle.getHandleState())) {
                return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
            }
            boolean update = this.update(new UpdateWrapper<HandlePo>()
                    .set("handle_state", handle.getHandleState())
                    .set("reason", handle.getReason())
                    .eq("handle_code", handle.getHandleCode())
                    .eq("handle_state", 0));
            if (update) {
                return new ApiResponse<>(HeaderStatus.SUCCESS);
            } else {
                return new ApiResponse<>(HeaderStatus.QUERY_UPDATE_STATUS_EXCEPTION);
            }
        } catch (Exception e) {
            log.error("HandleServiceImpl -> handleExamine() ret={} exception={}", handlePo, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public ApiResponse<?> applyHandle(AddHandleDto handleDto) {
        try {
            handleDto.setHandleCode(String.valueOf(SnowflakeIdUtils.nextId()));
            HandlePo handlePo = GsonUtils.fromJson(GsonUtils.toJSON(handleDto), HandlePo.class);
            HandlePo one = this.getOne(new QueryWrapper<HandlePo>()
                    .eq("order_code", handlePo.getOrderCode())
                    .eq("user_id", handlePo.getUserId()));
            if (Objects.isNull(one)) {
                boolean save = this.save(handlePo);
                return save ? new ApiResponse<>(HeaderStatus.SUCCESS) : new ApiResponse<>(HeaderStatus.EXCPEITON);
            } else {
                return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
            }
        } catch (Exception e) {
            log.error("HandleServiceImpl -> applyHandle() ret={} exception={}", handleDto, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public ApiResponse<?> verifyHandelApply(String orderCode, String userId) {
        try {
            return this.count(new QueryWrapper<HandlePo>().eq("order_code", orderCode).eq("user_id", userId)) > 0 ?
                    new ApiResponse<>(HeaderStatus.HANDLE_ALREADY_APPLY) : new ApiResponse<>(HeaderStatus.SUCCESS);
        } catch (Exception e) {
            log.error("HandleServiceImpl -> verifyHandelApply() orderCode={} userId={} exception={}", orderCode, userId, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public ApiResponse<?> getUserHandleList(String userId) {
        try {
            return new ApiResponse<>(this.list(new QueryWrapper<HandlePo>().eq("user_id", userId)));
        } catch (Exception e) {
            log.error("HandleServiceImpl -> getUserHandleList() userId={} exception={}", userId, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    private IPage<HandlePo> getPageFromDB(Map<String, Object> params) {
        // 没有命中，查db
        log.info("组装订单分页条件");
        final QueryWrapper<HandlePo> condition = new QueryWrapper<>();
        if (!Objects.isNull(params.get("handleCode"))) {
            condition.eq("handle_code",params.get("handleCode"));
        }
        if (!Objects.isNull(params.get("handleState"))) {
            condition.eq("handle_state",params.get("handleState"));
        }
        if (!Objects.isNull(params.get("userId"))) {
            condition.eq("user_id",params.get("userId"));
        }
        return this.page(
                new Query<HandlePo>().getPage(params),
                condition
        );
    }
}
