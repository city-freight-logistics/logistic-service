package com.logistics.micro.order.provide.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.order.api.OrderAcceptRateService;
import com.logistics.micro.order.provide.mapper.driver.OrderAcceptRateDao;
import com.logistics.micro.order.provide.model.pojo.OrderAcceptRatePO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 司机接单率概率表
 *
 * @author xionghw0982
 * @date 2021-04-19 09:57
 */
@Service("orderAcceptRateService")
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class OrderAcceptRateServiceImpl extends ServiceImpl<OrderAcceptRateDao, OrderAcceptRatePO> implements OrderAcceptRateService {

    /**
     * 获取
     *
     * @return
     */
    @Override
    public ApiResponse getRateModel() {
        final OrderAcceptRatePO one = this.getOne(Wrappers.<OrderAcceptRatePO>lambdaQuery()
                .orderByDesc(OrderAcceptRatePO::getId));
        final ApiResponse<OrderAcceptRatePO> objectApiResponse = new ApiResponse<>();
        objectApiResponse.setBody(one);
        return objectApiResponse;
    }

    /**
     * 插入
     *
     * @return
     * @param orderAcceptRate
     */
    @Override
    public ApiResponse insertRateModel(String orderAcceptRate) {

        final OrderAcceptRatePO ratePO = GsonUtils.fromJson(orderAcceptRate, OrderAcceptRatePO.class);
        final boolean save = this.save(ratePO);
        return new ApiResponse(HeaderStatus.SUCCESS);
    }
}
