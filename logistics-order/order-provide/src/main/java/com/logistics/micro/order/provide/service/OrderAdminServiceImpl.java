package com.logistics.micro.order.provide.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.DateUtils;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.order.api.DriverOrderService;
import com.logistics.micro.order.api.OrderAdminService;
import com.logistics.micro.order.dto.AdminOrderChartDto;
import com.logistics.micro.order.dto.DriverOrderPO;
import com.logistics.micro.order.dto.OrderAdminQueryDto;
import com.logistics.micro.order.provide.mapper.admin.OrderAdminMapper;
import com.logistics.micro.order.provide.model.pojo.OrderAdminPo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-04-27 21:58
 */
@Service
@Slf4j
public class OrderAdminServiceImpl extends ServiceImpl<OrderAdminMapper, OrderAdminPo> implements OrderAdminService {

    @Autowired
    private DriverOrderService driverOrderService;
    @Autowired
    private OrderAdminMapper orderAdminMapper;

    @Override
    public ApiResponse<?> queryOrder(OrderAdminQueryDto orderAdminQueryDto) {
        try {
            if (Objects.isNull(orderAdminQueryDto)) {
                return new ApiResponse<>(HeaderStatus.INPUT_DATA_NULL);
            }
//            if (orderAdminQueryDto.isNumeric(orderAdminQueryDto.getPage())) {
//                return new ApiResponse<>(HeaderStatus.PAGE_IS_NOT_NUMBER_ERROR);
//            }
            Map<String, Object> params = GsonUtils.strToMap(GsonUtils.toJSON(orderAdminQueryDto));
            IPage<OrderAdminPo> page = getPageFromDB(params);
            final PageUtils pageUtils = new PageUtils(page);
            if (pageUtils.getTotalCount() == 0){
                return new ApiResponse<>(HeaderStatus.PAGE_CONTEXT_NULL_INFO);
            }
            return new ApiResponse<>(GsonUtils.beanToMap(pageUtils));
        } catch (Exception e) {
            log.info("OrderAdminServiceImpl -> queryOrder() ret={} exception={}",orderAdminQueryDto.toString(), e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse<?> interruptOrder(String orderAdminPo) {
        try {
            if (StringUtils.isBlank(orderAdminPo)) {
                return new ApiResponse<>(HeaderStatus.INPUT_DATA_NULL);
            }
            OrderAdminPo order = GsonUtils.fromJson(orderAdminPo, OrderAdminPo.class);
            OrderAdminPo targetOrder = this.getOne(new QueryWrapper<OrderAdminPo>().eq("order_code", order.getOrderCode()));
            Object body = driverOrderService.getOne(String.valueOf(targetOrder.getId())).getBody();
            String driverOrder = GsonUtils.toJSON(body);
            DriverOrderPO driverOrderPO = null;
            if (StringUtils.isNotBlank(driverOrder) && !"获取失败".equals(body.toString())) {
                driverOrderPO = GsonUtils.fromJson(driverOrder, DriverOrderPO.class);
            }
            if (order.getOrderState().equals(targetOrder.getOrderState())) {
                if (Objects.isNull(driverOrderPO) || driverOrderPO.getOrderState().equals(targetOrder.getOrderState())) {
                    return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
                }
            }
            boolean updateDemand = this.update(new UpdateWrapper<OrderAdminPo>()
                    .set("order_state", order.getOrderState())
                    .ne("order_state", 9)
                    .eq("order_code", order.getOrderCode()));
            Boolean updateDriver = driverOrderService.updateOrderState(order.getOrderCode(), 9);
            if (updateDemand) {
                if (Objects.isNull(driverOrderPO)) {
                    return new ApiResponse<>(HeaderStatus.SUCCESS);
                } else {
                    if (updateDriver) {
                        return new ApiResponse<>(HeaderStatus.SUCCESS);
                    }
                }
            } else {
                if (!updateDriver) {
                    return new ApiResponse<>(HeaderStatus.UPDATE_PERMISSION_DEAD);
                }
            }
            return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
        } catch (Exception e) {
            log.info("OrderAdminServiceImpl -> interruptOrder() ret={} exception={}",orderAdminPo, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public ApiResponse<AdminOrderChartDto> dashBoardChart() {
        String date = DateUtils.dataFormatAdd(-6);
        List<Double> doubles = orderAdminMapper.dashBoardChartH(date);
        AdminOrderChartDto build = AdminOrderChartDto.builder()
                .amount(doubles)
                .sum(String.valueOf(doubles.stream().mapToDouble(Double::doubleValue).sum()))
                .build();
        return new ApiResponse<>(build);
    }

    @Override
    public ApiResponse<AdminOrderChartDto> dashBoardChartL() {
        String date = DateUtils.dataFormatAdd(-6);
        List<Double> doubles = orderAdminMapper.dashBoardChartL(date);
        int i = orderAdminMapper.dashBoardChartNum(date);
        AdminOrderChartDto build = AdminOrderChartDto.builder()
                .amount(doubles)
                .sum(String.valueOf(doubles.stream().mapToDouble(Double::doubleValue).sum()))
                .num(String.valueOf(i))
                .build();
        return new ApiResponse<>(build);
    }

    private IPage<OrderAdminPo> getPageFromDB(Map<String, Object> params) {
        // 没有命中，查db
        log.info("组装订单分页条件");
        final QueryWrapper<OrderAdminPo> condition = new QueryWrapper<>();
        if (!Objects.isNull(params.get("orderCode"))) {
            condition.eq("order_code",params.get("orderCode"));
        }
        if (!Objects.isNull(params.get("orderType"))) {
            condition.eq("order_type",params.get("orderType"));
        }
        if (!Objects.isNull(params.get("orderState"))) {
            condition.eq("order_state",params.get("orderState"));
        }
        return this.page(
                new Query<OrderAdminPo>().getPage(params),
                condition
        );
    }
}
