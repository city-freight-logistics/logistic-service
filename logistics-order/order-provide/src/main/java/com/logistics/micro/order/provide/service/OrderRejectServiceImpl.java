package com.logistics.micro.order.provide.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;

import com.logistics.micro.common.utils.Query;
import com.logistics.micro.order.api.OrderRejectService;
import com.logistics.micro.order.provide.mapper.driver.OrderRejectDao;
import com.logistics.micro.order.provide.model.pojo.OrderRejectPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 司机拒绝订单
 */
@Service("orderRejectService")
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class OrderRejectServiceImpl extends ServiceImpl<OrderRejectDao, OrderRejectPO> implements OrderRejectService {

    @Override
    public Map<String,Object> queryPage(Map<String, Object> params) {
        IPage<OrderRejectPO> page = this.page(
                new Query<OrderRejectPO>().getPage(params),
                new QueryWrapper<OrderRejectPO>()
        );
        final Map<String, Object> map = GsonUtils.beanToMap(new PageUtils(page));
        return map;
    }

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 司机拒绝订单对象
     * @Description: 获取单个司机拒绝订单
     * @param id
     */
    @Override
    public ApiResponse getOne(String id) {
        final OrderRejectPO orderRejectPO = this.getById(id);

        return new ApiResponse(orderRejectPO);
    }

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 添加成功或失败
     * @Description: 添加单个司机拒绝订单
     * @param OrderRejectDTO
     */
    @Override
    public ApiResponse addOne(String OrderRejectDTO) {
        final OrderRejectPO orderRejectPO = GsonUtils.fromJson(OrderRejectDTO, OrderRejectPO.class);
        OrderRejectPO orderPO = this.getOne(new QueryWrapper<OrderRejectPO>().eq("order_code", orderRejectPO.getOrderCode()));
        if (!Objects.isNull(orderPO)) {
            return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
        }

        final boolean save = this.save(orderRejectPO);
        if (save){
            return new ApiResponse("添加成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"添加失败");
    }

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 添加成功或失败
     * @Description: 批量添加司机拒绝订单
     * @param OrderRejectList
     */
    @Override
    public ApiResponse addBatch(List<String> OrderRejectList) {
        return null;
    }

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 删除成功或失败
     * @Description: 删除单个司机拒绝订单
     * @param OrderRejectDTO
     */
    @Override
    public ApiResponse deleteOne(String OrderRejectDTO) {
        return null;
    }

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 删除成功或失败
     * @Description: 批量删除司机拒绝订单
     * @param OrderRejectList
     */
    @Override
    public ApiResponse deleteBatch(List<Integer> OrderRejectList) {
        final boolean remove = this.removeByIds(OrderRejectList);
        if (remove){
            return new ApiResponse("删除成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"删除失败");
    }

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 更新成功或失败
     * @Description: 更新单个司机拒绝订单
     * @param OrderRejectDTO
     */
    @Override
    public ApiResponse updateOne(String OrderRejectDTO) {
        final OrderRejectPO orderRejectPO = GsonUtils.fromJson(OrderRejectDTO, OrderRejectPO.class);
        final boolean update = this.updateById(orderRejectPO);
        if (update){
            return new ApiResponse("更新成功");
        }
        return new ApiResponse(HeaderStatus.ERROR,"更新失败");
    }

    /***
     * @Parms 司机拒绝订单传输对象
     * @return 更新成功或失败
     * @Description: 批量更新司机拒绝订单
     * @param OrderRejectList
     */
    @Override
    public ApiResponse updateBatch(List<String> OrderRejectList) {
        return null;
    }

}