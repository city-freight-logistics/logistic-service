package com.logistics.micro.order.provide.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.order.OrderStatusDto;
import com.logistics.micro.order.api.OrderStateService;
import com.logistics.micro.order.provide.mapper.admin.OrderAdminMapper;
import com.logistics.micro.order.provide.model.pojo.OrderAdminPo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-27 14:27
 */
@Service
@Slf4j
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class OrderStateServiceImpl implements OrderStateService {

    private final OrderAdminMapper orderAdminMapper;

    @Override
    public ApiResponse<?> syncDriverOrderState(OrderStatusDto orderStatusDto) {
        try {
            log.info("OrderStateServiceImpl -> syncDriverOrderState start ret={}", orderStatusDto);
            OrderAdminPo orderAdminPo = orderAdminMapper.selectOne(new QueryWrapper<OrderAdminPo>()
                    .eq("order_code", orderStatusDto.getOrderCode())
                    .eq("order_state", orderStatusDto.getOrderState()));
            if (Objects.nonNull(orderAdminPo)) {
                return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
            }
            return orderAdminMapper.syncDriverOrderState(orderStatusDto) > 0 ?
                    new ApiResponse<>(HeaderStatus.SUCCESS) : new ApiResponse<>(HeaderStatus.QUERY_EMPTY, "无此数据");
        } catch (Exception e) {
            log.error("OrderStateServiceImpl -> syncDriverOrderState exception ret={}, exception={}", orderStatusDto, e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }

    }
}
