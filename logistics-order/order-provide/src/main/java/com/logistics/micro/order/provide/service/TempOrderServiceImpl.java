package com.logistics.micro.order.provide.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.order.api.TempOrderService;
import com.logistics.micro.order.provide.mapper.driver.TempOrderDao;
import com.logistics.micro.order.provide.model.pojo.TempOrderPO;
import com.logistics.micro.order.provide.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 用户下单但还未接单
 */
@Service("tempOrderService")
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class TempOrderServiceImpl extends ServiceImpl<TempOrderDao, TempOrderPO> implements TempOrderService {

    @Override
    public Map<String,Object> queryPage(Map<String, Object> params) {

        IPage<TempOrderPO> page = this.page(
                new Query<TempOrderPO>().getPage(params),
                new QueryWrapper<TempOrderPO>()
        );
        log.info("分页查询");
        final Map<String, Object> map = GsonUtils.beanToMap(new PageUtils(page));
        return map;
    }

    /**
     * 获取全部  list to Json
     *
     * @return
     */
    @Override
    public String getAll() {
        final List<TempOrderPO> list = this.list();

        return GsonUtils.toJSON(list);
    }

    /***
     * @Parms 用户暂存订单传输对象
     * @return 用户暂存订单对象
     * @Description: 获取单个用户暂存订单
     * @param TempOrderID
     */
    @Override
    public ApiResponse getOne(String TempOrderID) {
        final TempOrderPO one = this.lambdaQuery().eq(TempOrderPO::getId, TempOrderID).one();
        if (one == null) {
            return new ApiResponse(HeaderStatus.ERROR,"没有该记录");
        }
        return new ApiResponse(one);
    }

    /***
     * @Parms 用户暂存订单传输对象
     * @return 添加成功或失败
     * @Description: 添加单个用户暂存订单
     * @param TempOrdertDTO
     */
    @Override
    public ApiResponse addOne(String TempOrdertDTO) {
        final TempOrderPO tempOrderPO = GsonUtils.fromJson(TempOrdertDTO, TempOrderPO.class);
        final boolean save = this.save(tempOrderPO);
        if (!save) {
            return new ApiResponse(HeaderStatus.ERROR,"保存失败");
        }
        return new ApiResponse("保存成功");
    }

    /***
     * @Parms 用户暂存订单传输对象
     * @return 添加成功或失败
     * @Description: 批量添加用户暂存订单
     * @param TempOrdertList
     */
    @Override
    public ApiResponse addBatch(List<String> TempOrdertList) {
        List<TempOrderPO> list = new ArrayList<>(TempOrdertList.size());
        for (String tempOrder:
             TempOrdertList) {
            final TempOrderPO tempOrderPO = GsonUtils.fromJson(tempOrder, TempOrderPO.class);
            list.add(tempOrderPO);
        }
        final boolean save = this.saveBatch(list);
        if (!save) {
            return new ApiResponse(HeaderStatus.ERROR,"保存失败");
        }
        return new ApiResponse("保存成功");
    }

    /***
     * @Parms 用户暂存订单传输对象
     * @return 删除成功或失败
     * @Description: 批量删除用户暂存订单
     * @param TempOrdertList
     */
    @Override
    public void deleteBatch(List<String> TempOrdertList) {
        this.remove(Wrappers.<TempOrderPO>lambdaQuery().in(TempOrderPO::getOrderCode,TempOrdertList));
    }


    /***
     * @Parms 用户暂存订单传输对象
     * @return 更新成功或失败
     * @Description: 更新单个用户暂存订单
     * @param TempOrdertDTO
     */
    @Override
    public ApiResponse updateOne(String TempOrdertDTO) {
        return null;
    }

    /***
     * @Parms 用户暂存订单传输对象
     * @return 更新成功或失败
     * @Description: 批量更新用户暂存订单
     * @param TempOrdertList
     */
    @Override
    public ApiResponse updateBatch(List<String> TempOrdertList) {
        return null;
    }

}