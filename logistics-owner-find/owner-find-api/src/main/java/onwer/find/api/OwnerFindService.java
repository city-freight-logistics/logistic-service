package onwer.find.api;

import com.logistics.micro.common.model.ApiResponse;

/**
 * @author TBSWFGP
 * @creat 2021- 04-27-上午 11:39
 **/
public interface OwnerFindService {
    /**
     * 通过车辆id 获取车辆信息
     * @return
     */
    ApiResponse<?> getCarInfoById(Integer businessId);

    /**
     *获取车辆信息列表h
     */
    ApiResponse<?> getCarList();

    /**
     * 通过订单编号，获取找单信息
     * @param orderCode
     * @return
     */
    String getOrderInfo(String orderCode);
    ApiResponse<?> payOrder(OrderDto demandOrder);
}
