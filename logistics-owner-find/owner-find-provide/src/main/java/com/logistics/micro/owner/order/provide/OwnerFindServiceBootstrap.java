package com.logistics.micro.owner.order.provide;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author TBSWFGP
 * @creat 2021- 04-11-上午 11:04
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class OwnerFindServiceBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(OwnerFindServiceBootstrap.class,args);
    }
}
