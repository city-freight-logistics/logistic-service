package com.logistics.micro.owner.order.provide.controller;


import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.owner.order.provide.entity.DmsCarInfo;
import com.logistics.micro.owner.order.provide.service.DmsCarInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 订单业务表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
@RestController
@RequestMapping("/dms-order/dms-car-info")
public class DmsCarInfoController {

    @Autowired
    private DmsCarInfoService dmsCarInfoService;

    @ApiOperation(value = "获取全部车辆信息")
    @GetMapping("/getCarInfo")
    public ApiResponse<?> getCarInfo(){
        List<DmsCarInfo> list = dmsCarInfoService.list();
        return  new ApiResponse<List<DmsCarInfo>>(list);
    }
    @ApiOperation(value = "根据id获取车辆息")
    @GetMapping("/getCarInfoById")
    public ApiResponse<?> getCarInfoById(Integer id){
        System.out.println(id);
        DmsCarInfo carInfo = dmsCarInfoService.getById(id);
        System.out.println(carInfo);
        return  new ApiResponse<DmsCarInfo>(carInfo);
    }

}

