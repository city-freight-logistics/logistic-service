package com.logistics.micro.owner.order.provide.controller;


import com.alibaba.fastjson.JSONObject;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.Header;
import com.logistics.micro.owner.order.provide.dubboService.OwnerFindDubboService;
import com.logistics.micro.owner.order.provide.entity.DmsOrder;
import com.logistics.micro.owner.order.provide.service.DmsOrderService;
import com.logistics.micro.payment.api.PaymentDubboService;
import com.logistics.mircro.driver.publish.api.DriverPublishApiService;
import com.logistics.mircro.driver.vehicle.api.VehicledubboService;
import com.logistics.owner.order.publish.api.DemandOrder;
import com.logistics.owner.order.publish.api.OwnerPublishService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import onwer.find.api.OrderDto;
import onwer.find.api.OwnerFindService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 货主找单表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-04-23
 */
@RestController
@RequestMapping("/dms-order")
@Api(tags = "用户找单模块")
public class DmsOrderController {

     @Autowired
     private OwnerFindDubboService ownerFindDubboService;
    @Reference
    private OwnerPublishService ownerPublishService;
    @Reference
    private DriverPublishApiService driverPublishApiService;
    @Reference
    private VehicledubboService vehicledubboService;
    @Autowired
    private DmsOrderService dmsOrderService;
    /**
     * 货主找单  选中运力后下单  选中运力后生成订单
     * @return
     */
  @Transactional(rollbackFor = Exception.class)
  @ApiOperation(value = "用户找单生成订单模块，传参参数在OrderVO")
  @PostMapping("/findOrder")
  public ApiResponse<?> findOrder(@RequestBody DemandOrder demandOrder){

       ApiResponse<?> one = driverPublishApiService.getOneById(demandOrder.getOrderId());
       Object body = one.getBody();
       Map map = JSONObject.parseObject(JSONObject.toJSONString(body), Map.class);
       //判断当前是否找单状态
      if(0!=(Integer)map.get("state")){
          ApiResponse<String> response = new ApiResponse<>("当前订单取消,结束或被接单");
          response.setHeader(new Header(HeaderStatus.ERROR));
          return response;
      }
      //查询司机当前车辆是否禁用
//      VehiclePO vehiclePO= (VehiclePO) vehicledubboService.getCarInfoByCarCode((String)map.get("carCode")).getBody();
//      if(vehiclePO==null || vehiclePO.getState()!=4){
//          ApiResponse<String> response = new ApiResponse<>("当前司机当前车辆不可用");
//          response.setHeader(new Header(HeaderStatus.ERROR));
//          return response;
//      }
       //远程调用生成订单
      ApiResponse<?> order = ownerPublishService.order(demandOrder,0);
      if(order.getHeader().getCode()!=200){
          ApiResponse<String> response = new ApiResponse<>("保存订单信息失败");
          response.setHeader(new Header(HeaderStatus.ERROR));
          return response;
      }
       //更改订单运力发布订单状态
       ApiResponse state = driverPublishApiService.updateOrderStateById(demandOrder.getOrderId(), 3);
       if(!(boolean)state.getBody()){
           ApiResponse<String> response = new ApiResponse<>("服务器繁忙");
           response.setHeader(new Header(HeaderStatus.ERROR));
           return response;
       }

      return  new ApiResponse<>("订单生成成功");
  }


  public OrderDto getOrderDto(DemandOrder demandOrder){
      OrderDto dto = new OrderDto();
      dto.setBeginDate(demandOrder.getBeginDate());
      dto.setCompleteDate(demandOrder.getCompleteDate());
      dto.setConsigneeName(demandOrder.getConsigneeName());
      dto.setConsigneeTel(demandOrder.getConsigneeTel());
      dto.setCustomerName(demandOrder.getCustomerName());
      dto.setCustomerTel(demandOrder.getCustomerTel());
      dto.setDriverName(demandOrder.getDriverName());
      dto.setFreightAmount(new BigDecimal(demandOrder.getFreightAmount()));
      dto.setGoodsId(demandOrder.getGoodsId());
      dto.setOrderCode(demandOrder.getOrderCode());
      dto.setOrderName(demandOrder.getOrderName());
      //dto.setReceiveAmount(new BigDecimal(demandOrder.getReceiveAmount()));
      dto.setOrderRemarks(demandOrder.getOrderRemarks());
      dto.setOrderState(demandOrder.getOrderState());
      return dto;
  }
    private List getFiledsInfo(Object o) {
                 Field[] fields = o.getClass().getDeclaredFields();
                 String[] fieldNames = new String[fields.length];
                 List list = new ArrayList();
                 Map infoMap = null;
                 for (int i = 0; i < fields.length; i++) {
                         infoMap = new HashMap();
                         infoMap.put("type", fields[i].getType().toString());
                         infoMap.put("name", fields[i].getName());
                         infoMap.put("value", getFieldValueByName(fields[i].getName(), o));
                         list.add(infoMap);
                     }
                return list;
            }
    private Object getFieldValueByName(String fieldName, Object o) {
                 try {
                         String firstLetter = fieldName.substring(0, 1).toUpperCase();
                         String getter = "get" + firstLetter + fieldName.substring(1);
                        Method method = o.getClass().getMethod(getter, new Class[] {});
                         Object value = method.invoke(o, new Object[] {});
                         return value;
                     } catch (Exception e) {

                         return null;
                    }
             }
    /**
     * 远程服务调用  /分页获取司机运力发布列表
     * @param params
     * @return
     */
    @ApiOperation(value = "分页获取司机运力发布列表，参数为map类型，至少需要一个page参数")
    @GetMapping("/getOrderPublishPage")
  public ApiResponse<?> getOrderPublishPage(Map<String, Object> params){
        System.out.println(111);
      Map<String, Object> page = driverPublishApiService.queryPage(params);
      return  new ApiResponse<>(page);
  }
    /**
     *  通过司机id获取司机的运力发布
      */
    @ApiOperation(value = "通过司机订单编号获取司机的运力发布")
   @GetMapping("/getDriverPublishById")
  public ApiResponse<?> getDriverPublishById(String orderCode){
        ApiResponse one = driverPublishApiService.getOne(orderCode);
        return  one;
    }

    /**
     * 根据司机姓名获取车辆信息
     * @return
     */
    @ApiOperation(value = "根据司机姓名获取车辆信息 ")
   @GetMapping("/getDriverVehicleInfoByDriverName")
    public ApiResponse<?> getDriverVehicleInfoByDriverName(String driverName){
       ApiResponse listByDriverName = vehicledubboService.getListByDriverName(driverName);
       return  listByDriverName;
   }

    /**
     * 取消订单
     * @param orderCode
     * @return
     */
   @PostMapping("/cancleOrder")
   public ApiResponse<?> cancleOrder(@RequestParam String orderCode){
       final Boolean aBoolean = dmsOrderService.cancleOrder(orderCode);
       if (!aBoolean) {
           return new ApiResponse<>(HeaderStatus.ERROR,"取消失败");
       }
       return new ApiResponse<>(HeaderStatus.SUCCESS);
   }
}

