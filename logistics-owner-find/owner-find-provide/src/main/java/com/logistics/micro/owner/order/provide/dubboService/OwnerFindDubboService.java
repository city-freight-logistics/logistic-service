package com.logistics.micro.owner.order.provide.dubboService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.owner.order.provide.entity.DmsCarInfo;
import com.logistics.micro.owner.order.provide.mapper.DmsCarInfoMapper;
import com.logistics.micro.owner.order.provide.service.DmsOrderService;
import com.logistics.micro.payment.api.PaymentDubboService;
import com.logistics.owner.order.publish.api.DemandOrder;
import onwer.find.api.OrderDto;
import onwer.find.api.OwnerFindService;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author TBSWFGP
 * @creat 2021- 04-27-上午 11:40
 **/
@Service
@Component
public class OwnerFindDubboService extends ServiceImpl<DmsCarInfoMapper, DmsCarInfo> implements OwnerFindService {
    @Autowired
    DmsOrderService dmsOrderService;
    @Reference
    private PaymentDubboService paymentDubboService;
    @Override
    public ApiResponse<?> getCarInfoById(Integer id) {
        DmsCarInfo carInfo = this.getById(id);
        return new ApiResponse<DmsCarInfo>(carInfo);
    }

    @Override
    public ApiResponse<?> getCarList() {
        List<DmsCarInfo> list = this.list();
        return  new ApiResponse<List<DmsCarInfo>>(list) ;
    }

    /**
     * 通过订单编号，获取找单信息
     *
     * @param orderCode
     * @return
     */
    @Override
    public String getOrderInfo(String orderCode) {
        if (StringUtils.isBlank(orderCode)) {
            return "";
        }
        return dmsOrderService.getOrderInfo(orderCode);
    }

    @Override
    public ApiResponse<?> payOrder(OrderDto orderDto) {
        ApiResponse pay = paymentDubboService.payByUser(GsonUtils.toJSON(orderDto));
        return new ApiResponse<>(pay);
    }
    public OrderDto getOrderDto(DemandOrder demandOrder){
        OrderDto dto = new OrderDto();
        dto.setBeginDate(demandOrder.getBeginDate());
        dto.setCompleteDate(demandOrder.getCompleteDate());
        dto.setConsigneeName(demandOrder.getConsigneeName());
        dto.setConsigneeTel(demandOrder.getConsigneeTel());
        dto.setCustomerName(demandOrder.getCustomerName());
        dto.setCustomerTel(demandOrder.getCustomerTel());
        dto.setDriverName(demandOrder.getDriverName());
        dto.setFreightAmount(new BigDecimal(demandOrder.getFreightAmount()));
        dto.setGoodsId(demandOrder.getGoodsId());
        dto.setOrderCode(demandOrder.getOrderCode());
        dto.setOrderName(demandOrder.getOrderName());
        dto.setReceiveAmount(new BigDecimal(demandOrder.getReceiveAmount()));
        dto.setOrderRemarks(demandOrder.getOrderRemarks());
        dto.setOrderState(demandOrder.getOrderState());
        return dto;
    }
}
