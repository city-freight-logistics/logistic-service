package com.logistics.micro.owner.order.provide.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 订单业务表
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("DMS_CAR_INFO")
@ApiModel(value="DmsCarInfo对象", description="订单业务表")
public class DmsCarInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "business_id", type = IdType.AUTO)
    private Integer businessId;

    @ApiModelProperty(value = "业务类型")
    private String businessType;

    @ApiModelProperty(value = "车型")
    private String carType;

    @ApiModelProperty(value = "载重")
    private String carWeight;

    @ApiModelProperty(value = "长宽高")
    private String carLwh;

    @ApiModelProperty(value = "体积")
    private String carVolume;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "修改时间")
    private Date modifyDate;

    @ApiModelProperty(value = "逻辑删除（0：否，1：是）")
    private Integer isDelete;

    /**
     * 图片地址
     */
    private String src;

}
