package com.logistics.micro.owner.order.provide.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 货主找单表
 * </p>
 *
 * @author ${author}
 * @since 2021-04-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("DMS_ORDER_FIND")
@ApiModel(value="DmsOrder对象", description="货主找单表")
public class DmsOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String orderCode;

    @ApiModelProperty(value = "找单标题")
    private String orderTitle;

    @ApiModelProperty(value = "期望回报")
    private Double exception;

    @ApiModelProperty(value = "实际装卸货地址")
    private String realTitle;

    @ApiModelProperty(value = "订单发起人/发货人姓名")
    private String customerName;

    @ApiModelProperty(value = "发起人联系电话")
    private String customerTel;

    @ApiModelProperty(value = "收货人姓名")
    private String consigneeName;

    @ApiModelProperty(value = "收货人联系电话")
    private String consigneeTel;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "司机姓名")
    private String driverName;

    @ApiModelProperty(value = "司机电话")
    private String driverTel;

    @ApiModelProperty(value = "实际运输时间")
    private Date realTime;

    @ApiModelProperty(value = "车辆信息")
    private Integer vehicleId;

    @ApiModelProperty(value = "货物信息")
    private Integer goodsId;

    @ApiModelProperty(value = "备注")
    private String orderRemarks;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date modifyDate;

    @ApiModelProperty(value = "逻辑删除（0：否，1：是）")
    @TableLogic
    private Integer isDelete;



}
