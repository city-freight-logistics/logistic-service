package com.logistics.micro.owner.order.provide.entity.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 车辆信息表
 * 
 * @author xionghw
 * @date 2021-03-29 14:20:48
 */
@Data
@TableName("VMS_VEHICLE")
public class VehiclePO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 司机姓名
	 */
	private String driverName;
	/**
	 * 车牌
	 */
	private String carCode;
	/**
	 * 车款
	 */
	private String carName;
	/**
	 * 车辆类型
	 */
	private String carType;
	/**
	 * 司机的车的图片
	 */
	private String carUrl;
	/**
	 * 载重量
	 */
	private Double loadCapacity;
	/**
	 * 容量体积
	 */
	private Double volumeCapacity;
	/**
	 * 车辆状态(整型 0：禁用 1：锁定  2：审核中  3：正在使用 4：空闲)
	 */
	private Integer state;
	/**
	 * 创建时间
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 修改时间
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifyDate;
	/**
	 * 逻辑删除（0：否，1：是）
	 */
	@TableLogic
	private Integer isDelete;

}
