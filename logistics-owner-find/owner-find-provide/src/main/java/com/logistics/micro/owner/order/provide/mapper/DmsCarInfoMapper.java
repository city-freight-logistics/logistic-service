package com.logistics.micro.owner.order.provide.mapper;

import com.logistics.micro.owner.order.provide.entity.DmsCarInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单业务表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
public interface DmsCarInfoMapper extends BaseMapper<DmsCarInfo> {

}
