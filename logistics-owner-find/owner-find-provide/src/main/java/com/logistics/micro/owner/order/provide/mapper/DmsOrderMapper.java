package com.logistics.micro.owner.order.provide.mapper;

import com.logistics.micro.owner.order.provide.entity.DmsOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 货主找单表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-04-23
 */
public interface DmsOrderMapper extends BaseMapper<DmsOrder> {

}
