package com.logistics.micro.owner.order.provide.service;

import com.logistics.micro.owner.order.provide.entity.DmsCarInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单业务表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
public interface DmsCarInfoService extends IService<DmsCarInfo> {

}
