package com.logistics.micro.owner.order.provide.service;

import com.logistics.micro.owner.order.provide.entity.DmsOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 货主找单表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-23
 */
public interface DmsOrderService extends IService<DmsOrder> {
    /**
     * 通过订单编号，获取找单信息
     * @param orderCode
     * @return
     */
    String getOrderInfo(String orderCode);

    /**
     * 取消订单
     * @param orderCode
     * @return
     */
    Boolean cancleOrder(String orderCode);


}
