package com.logistics.micro.owner.order.provide.service.impl;

import com.logistics.micro.owner.order.provide.entity.DmsCarInfo;
import com.logistics.micro.owner.order.provide.mapper.DmsCarInfoMapper;
import com.logistics.micro.owner.order.provide.service.DmsCarInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单业务表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
@Service
public class DmsCarInfoServiceImpl extends ServiceImpl<DmsCarInfoMapper, DmsCarInfo> implements DmsCarInfoService {

}
