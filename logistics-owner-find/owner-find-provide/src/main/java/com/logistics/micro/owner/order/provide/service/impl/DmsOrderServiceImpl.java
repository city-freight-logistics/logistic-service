package com.logistics.micro.owner.order.provide.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.owner.order.provide.entity.DmsOrder;
import com.logistics.micro.owner.order.provide.mapper.DmsOrderMapper;
import com.logistics.micro.owner.order.provide.service.DmsOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.mircro.driver.publish.api.DriverPublishApiService;
import com.logistics.owner.order.publish.api.OwnerPublishService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 货主找单表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-23
 */
@Service
public class DmsOrderServiceImpl extends ServiceImpl<DmsOrderMapper, DmsOrder> implements DmsOrderService {
    @Reference
    private DriverPublishApiService driverPublishApiService;
    @Reference
    private OwnerPublishService ownerPublishService;
    /**
     * 通过订单编号，获取找单信息
     *
     * @param orderCode
     * @return
     */
    @Override
    public String getOrderInfo(String orderCode) {
        String response = ownerPublishService.getOrderOrderCodeStateFind(orderCode);
        return response;
    }

    /**
     * 取消订单
     *
     * @param orderCode
     * @return
     */
    @Override
    public Boolean cancleOrder(String orderCode) {
        final ApiResponse<?> one = driverPublishApiService.getOne(orderCode);
        Map<String,Object> map = (Map<String, Object>) one.getBody();
        final QueryWrapper<DmsOrder> condition = new QueryWrapper<>();
        condition.eq("order_code",orderCode);
        final boolean remove = this.remove(condition);
        final String id = String.valueOf(map.get("id"));
        driverPublishApiService.updateOrderStateById(Integer.parseInt(id), 0);
        return remove;
    }
}
