package com.logistics.micro.owner.manage.api;

import com.logistics.micro.common.model.ApiResponse;

import java.util.List;

/**
 * @author TBSWFGP
 * @creat 2021- 04-27-上午 10:55
 **/
public interface OwnerManageService {
    /**
     * 通过订单编号获取订单
     * @param orderCode
     * @return
     */
    ApiResponse<?> gerOrderByOrderCode(String orderCode);

    /**
     *  通过订单编号修改订单
     * @param orderCode
     * @param order demandOrder
     * @return
     */
    ApiResponse<?> updateOrderByOrderCode(String orderCode,DemandOrder order);

    /**
     * 查询类别不包含父类
     * @return
     */
    ApiResponse<?>  listGoodsNotContainsParent();
    /**
     * 通过货物编号查询货物
     * @param goodsId
     * @return
     */
    ApiResponse<?> getGoodById(Integer goodsId);

    /**
     * 查询货物类型关系列表 深度遍历返回列表关系
     */
    ApiResponse<?>   listGoods();

    /**
     * 通过货物typeid查询货物类型
     * @return
     */
    ApiResponse<?>  getGoodsTypeByTypeId(Integer typeId) ;

    /**
     * 更新货物 传得对象至少包含货物得id主键值
     * @return
     */
    ApiResponse<?> updateGoodsInfo(Object gmsGoods);

    /**
     * 根据类型名来修改货物类型
     */
    ApiResponse<?> updateGoodsTypeById(List<?> goodsTypeList);

 }

