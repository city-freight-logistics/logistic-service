package com.logistics.micro.owner.order.provide.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;

import com.logistics.micro.common.model.Header;
import com.logistics.micro.owner.order.provide.dubboService.OwnerManageDubboService;
import com.logistics.micro.owner.manage.api.DemandOrder;
import com.logistics.micro.owner.order.provide.service.DemandOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-04-13
 */

@RestController
@RequestMapping("/manage-order")
@Api(tags = "用户订单管理模块")
public class DemandOrderController {

   @Autowired
   private DemandOrderService demandOrderService;
    @Autowired
    private OwnerManageDubboService ownerManageDubboService;
   /**
    * 分页查询订单列表
    * @param current
    * @param size
    * @param customerTel  发起人电话号
    * @return
    */
   @ApiOperation(value = "根据用户号码分页查询订单列表")
   @GetMapping("/getOrderPage")
    public ApiResponse<List<DemandOrder>> getOrderPage(@RequestParam("current") Integer current,
                                       @RequestParam("size") Integer size,
                                       @RequestParam("userId") Integer userId,
                                       @RequestParam(value = "customerTel",required = false) String customerTel){
       //订单无限下拉 就不需要显示总条数了
       IPage page=new Page(current, size);
       QueryWrapper<DemandOrder> wrapper = new QueryWrapper<>();
       wrapper.eq("user_id", userId);
       if(!StringUtils.isEmpty(customerTel)){
           wrapper.eq("customer_tel", customerTel);
       }                             
       IPage iPage = demandOrderService.page(page, wrapper);
       List<DemandOrder> records = iPage.getRecords();
       //long total = iPage.getTotal();
       ApiResponse<List<DemandOrder>> response = new ApiResponse<>(records);
       return response;
    }

    @PostMapping("/pay")
    public ApiResponse<?> pay(@RequestBody  DemandOrder demandOrder) {
        ApiResponse<?> order = ownerManageDubboService.pay(1, demandOrder);
        return  new ApiResponse<>(order);
    }

   /**
    * 修改订单信息 发布时不能修改 取消发布重新发布 不考虑同步问题
    * @param  demandOrder
    * @return
    */
    @ApiOperation(value = "修改订单信息，参数为demanOrder")
    @PutMapping("/updateOrder")
    public ApiResponse<?> updateOrder(@RequestBody DemandOrder demandOrder){
       UpdateWrapper<DemandOrder> wrapper = new UpdateWrapper<DemandOrder>()
               .eq("order_code", demandOrder.getOrderCode());
       boolean b = demandOrderService.update(demandOrder, wrapper);
       ApiResponse<Object> response = new ApiResponse<>();
       if(b){
          response.setHeader(new Header(HeaderStatus.SUCCESS));
       }
       return response;
    }
    @ApiOperation(value = "查询货物类型列表")
    @GetMapping("/getGoodsTypeList")
    public ApiResponse<?> getGoodsTypeList(){
        System.out.println(111);
        ApiResponse<?> goods = ownerManageDubboService.listGoods();
        return  goods;
    }
    @ApiOperation(value = "查询货物类型列表不包含父类")
    @GetMapping("/getTypeList")
    public ApiResponse<?> getTypeList(){
        System.out.println(111);
        ApiResponse<?> goods = ownerManageDubboService.listGoodsNotContainsParent();
        return  goods;
    }
    @GetMapping("/test")
    public ApiResponse<?> test(){
        return new ApiResponse<>("123");
    }
   /**
    * 删除订单
    * @param id  订单id 真实删除
    * @return
    */
   @ApiOperation(value = "真实删除订单")
    @DeleteMapping("/deleteOrderTruly/{id}")
    public ApiResponse<?> deleteOrderTruly(@PathVariable int id){

       boolean b = demandOrderService.removeById(id);
       ApiResponse<Object> response = new ApiResponse<>();
       if(b){
          response.setHeader(new Header(HeaderStatus.SUCCESS));
       }
       return response;
    }
}

