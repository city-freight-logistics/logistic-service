package com.logistics.micro.owner.order.provide.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 货物表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
@RestController
@RequestMapping("/gms-goods")
public class GmsGoodsController {

}

