package com.logistics.micro.owner.order.provide.dubboService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.order.api.DriverOrderService;
import com.logistics.micro.owner.manage.api.OwnerManageService;
import com.logistics.micro.owner.manage.api.DemandOrder;
import com.logistics.micro.owner.order.provide.entity.GmsGoods;
import com.logistics.micro.owner.order.provide.entity.GmsGoodsType;
import com.logistics.micro.owner.order.provide.mapper.DemandOrderMapper;
import com.logistics.micro.owner.order.provide.service.GmsGoodsService;
import com.logistics.micro.owner.order.provide.service.GmsGoodsTypeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import com.logistics.micro.payment.api.PaymentDubboService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author TBSWFGP
 * @creat 2021- 04-27-上午 10:56
 **/
@Slf4j
@Service
@org.springframework.stereotype.Service
public class OwnerManageDubboService extends ServiceImpl<DemandOrderMapper, DemandOrder> implements OwnerManageService {
	@Autowired
	private GmsGoodsService gmsGoodsService;
	@Autowired
	private GmsGoodsTypeService gmsGoodsTypeService;
	@Reference
	private DriverOrderService driverOrderService;
	@Reference
	private PaymentDubboService paymentDubboService;

	@Override
	public ApiResponse<?> gerOrderByOrderCode(String orderCode) {
		DemandOrder order = this.baseMapper.selectOne(new QueryWrapper<DemandOrder>().eq("order_code", orderCode));
		return new ApiResponse(order);
	}

	public ApiResponse<?> pay(Integer orderState, DemandOrder order) {
		paymentDubboService.payByUser(GsonUtils.toJSON(order));
		order.setOrderState(orderState);
		//折扣
		Double discount = (Math.random() * 2 + 8)/10;
		String format = String.format("%.2f", discount);
		String freightAmount = order.getFreightAmount();
		Double receiveAmount = Double.parseDouble(freightAmount) * Double.parseDouble(format);
		order.setReceiveAmount(String.format("%.2f", receiveAmount));
		Integer i = this.baseMapper.updateById(order);
		driverOrderService.updateOrderState(order.getOrderCode(), 5);

		return new ApiResponse(i);
	}

	@Override
	public ApiResponse<?> updateOrderByOrderCode(String orderCode, DemandOrder order) {
		UpdateWrapper<DemandOrder> wrapper = new UpdateWrapper<>();
		wrapper.eq("order_code", orderCode);
		Integer i = this.baseMapper.update(order, wrapper);
		return new ApiResponse(i);
	}

	@Override
	public ApiResponse<?> listGoodsNotContainsParent() {
		List<GmsGoodsType> goods = gmsGoodsTypeService.list();
		ArrayList<GmsGoodsType> list = new ArrayList<>();
		for (GmsGoodsType good : goods) {
			if (good.getParentId() != null && good.getParentId() != 0) {
				list.add(good);
			}
		}
		return new ApiResponse<>(list);
	}

	@Override
	public ApiResponse<?> getGoodById(Integer goodsId) {
		GmsGoods goods = gmsGoodsService.getById(goodsId);
		return new ApiResponse<GmsGoods>(goods);
	}

	@Override
	public ApiResponse<?> listGoods() {
		//一次性查出来所有得
		List<GmsGoodsType> goods = gmsGoodsTypeService.list();
		ArrayList<GmsGoodsType> list = new ArrayList<>();
		for (GmsGoodsType good : goods) {
			if (null == good.getParentId() || 0 == good.getParentId()) {
				getChildren(good, goods);
				list.add(good);
			}
		}
		return new ApiResponse<ArrayList<GmsGoodsType>>(list);
	}

	@Override
	public ApiResponse<?> getGoodsTypeByTypeId(Integer typeId) {
		GmsGoodsType goodsType = gmsGoodsTypeService.getOne(new QueryWrapper<GmsGoodsType>().eq("type_id", typeId));

		return new ApiResponse<GmsGoodsType>(goodsType);
	}

	@Override
	public ApiResponse<?> updateGoodsInfo(Object gmsGoods) {
		GmsGoods goods = (GmsGoods) gmsGoods;
		boolean b = gmsGoodsService.update(goods, new UpdateWrapper<GmsGoods>().eq("goods_id", goods.getGoodsId()));
		return new ApiResponse<Boolean>(b);
	}

	@Override
	public ApiResponse<?> updateGoodsTypeById(List<?> goodsTypeList) {
		List<GmsGoodsType> gmsGoodsType = new ArrayList<>(goodsTypeList.size());
		for (Object item : goodsTypeList) {
			final GmsGoodsType goodsType = new GmsGoodsType();
			BeanUtils.copyProperties(item, goodsType);
			gmsGoodsType.add(goodsType);
		}
		boolean b = gmsGoodsTypeService.updateBatchById(gmsGoodsType);
		return new ApiResponse<Boolean>(b);
	}


	public void getChildren(GmsGoodsType father, List<GmsGoodsType> goods) {
		for (int i = 0; i < goods.size(); i++) {
			if (goods.get(i).getParentId() == father.getTypeId()) {
				father.getChildren().add(goods.get(i));
			}
		}
		//如果是遍历完了之后呢 没有子货物了说明找完了
		if (father.getChildren().size() == 0) {
			return;
		}
		//但是他得子货物还有子货物类型得话继续找
		for (GmsGoodsType father1 : father.getChildren()) {
			getChildren(father1, goods);
		}
	}

}
