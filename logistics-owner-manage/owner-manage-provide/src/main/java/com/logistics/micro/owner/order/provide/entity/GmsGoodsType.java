package com.logistics.micro.owner.order.provide.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

import java.util.ArrayList;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 货物分类表
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("GMS_GOODS_TYPE")
@ApiModel(value="GmsGoodsType对象", description="货物分类表")
public class GmsGoodsType implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "type_id", type = IdType.AUTO)
    @TableField(value = "type_id")
    private Integer typeId;

     @TableField(value = "type_name")
    @ApiModelProperty(value = "分类名称")
    private String typeName;

    @ApiModelProperty(value = "父id")
    private Integer parentId;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "修改时间")
    private Date modifyDate;

    @ApiModelProperty(value = "逻辑删除（0：否，1：是）")
    private Integer isDelete;

    @ApiModelProperty(value = "司机接单概率默认100")
    private Double acceptRate;
    
    @TableField(exist = false)
    List<GmsGoodsType> children=new ArrayList<>();
}
