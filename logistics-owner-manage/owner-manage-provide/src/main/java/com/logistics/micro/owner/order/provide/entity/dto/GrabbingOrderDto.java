package com.logistics.micro.owner.order.provide.entity.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用于抢单的dto
 * @author TBSWFGP
 * @creat 2021- 04-16-下午 13:49
 **/

@Data
public class GrabbingOrderDto  implements Serializable {
    /**

     * id

     */

    private Integer id;

    /**

     * 订单编号

     */

    private String orderCode;

    /**

     * 订单名称

     */

    private String orderName;

    /**

     * 司机姓名

     */

    private String driverName;

    /**

     * 司机编号

     */

    private String driverCode;

    /**

     * 车牌

     */

    private String carCode;

    /**

     * 订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）

     */

    private Integer state;

    /**

     * 订单开始时间

     */

    private Date beginDate;



}
