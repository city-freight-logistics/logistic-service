package com.logistics.micro.owner.order.provide.mapper;

import com.logistics.micro.owner.order.provide.entity.GmsGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 货物表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
public interface GmsGoodsMapper extends BaseMapper<GmsGoods> {

}
