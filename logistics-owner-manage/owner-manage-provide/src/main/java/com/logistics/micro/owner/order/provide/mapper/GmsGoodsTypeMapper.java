package com.logistics.micro.owner.order.provide.mapper;

import com.logistics.micro.owner.order.provide.entity.GmsGoodsType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 货物分类表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
public interface GmsGoodsTypeMapper extends BaseMapper<GmsGoodsType> {

}
