package com.logistics.micro.owner.order.provide.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.owner.manage.api.DemandOrder;

/**
 * @author TBSWFGP
 * @creat 2021- 04-14-下午 12:17
 **/
public interface DemandOrderService extends IService<DemandOrder> {

}
