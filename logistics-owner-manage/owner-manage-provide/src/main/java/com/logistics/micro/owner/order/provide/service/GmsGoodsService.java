package com.logistics.micro.owner.order.provide.service;

import com.logistics.micro.owner.order.provide.entity.GmsGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 货物表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
public interface GmsGoodsService extends IService<GmsGoods> {

}
