package com.logistics.micro.owner.order.provide.service;

import com.logistics.micro.owner.order.provide.entity.GmsGoodsType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 货物分类表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
public interface GmsGoodsTypeService extends IService<GmsGoodsType> {

}
