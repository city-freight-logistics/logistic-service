package com.logistics.micro.owner.order.provide.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.owner.manage.api.DemandOrder;
import com.logistics.micro.owner.order.provide.mapper.DemandOrderMapper;
import com.logistics.micro.owner.order.provide.service.DemandOrderService;
import org.springframework.stereotype.Service;

/**
 * @author TBSWFGP
 * @creat 2021- 04-14-下午 12:18
 **/
@Service
public class DemandOrderServiceImpl extends ServiceImpl<DemandOrderMapper, DemandOrder> implements DemandOrderService {
}
