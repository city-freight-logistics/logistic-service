package com.logistics.micro.owner.order.provide.service.impl;

import com.logistics.micro.owner.order.provide.entity.GmsGoodsType;
import com.logistics.micro.owner.order.provide.mapper.GmsGoodsTypeMapper;
import com.logistics.micro.owner.order.provide.service.GmsGoodsTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 货物分类表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-27
 */
@Service
public class GmsGoodsTypeServiceImpl extends ServiceImpl<GmsGoodsTypeMapper, GmsGoodsType> implements GmsGoodsTypeService {

}
