package com.logistics.owner.order.publish.api;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2021-04-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("OMS_DEMAND_ORDER")
@ApiModel(value="DemandOrder对象", description="货主发布拉货订单")
public class DemandOrder implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用车id")
    private Integer carId;

    @ApiModelProperty(value = "订单编号")
    private String orderCode;

    @ApiModelProperty(value = "订单名称")
    private String orderName;

    @ApiModelProperty(value = "订单业务")
    private String orderType;

    @ApiModelProperty(value = "货物编号")
    private String goodsId;

    @ApiModelProperty(value = "货物类型name")
    private String typeName;
    
    @ApiModelProperty(value = "订单发起人（发货人）")
    private String customerName;

    @ApiModelProperty(value = "发货位置经纬度")
    private  String latitude;
    @ApiModelProperty(value = "发货位置经纬度")
    private  String longitude;
    @TableField(exist = false)
    @ApiModelProperty(value = "收货位置经纬度")
    private  String toLatitude;
    @TableField(exist = false)
    @ApiModelProperty(value = "收货位置经纬度")
    private  String toLongitude;
    @ApiModelProperty(value = "收获与发货距离")
    private  String orderDistance;

    @ApiModelProperty(value = "发起人联系电话")
    private String customerTel;
    /**
     *
     * 找单表的code
     */
    @TableField(exist = false)
    private  String  findOrderCode;
    @ApiModelProperty(value = "收货人")
    private String consigneeName;
    /**
     * 用户id
     */
    private Integer userId;
    @ApiModelProperty(value = "收货人联系电话")
    private String consigneeTel;

    @ApiModelProperty(value = "用户身份")
    private String customerType;

    @ApiModelProperty(value = "订单执行人姓名")
    private String driverName;
    @ApiModelProperty(value = "订单执行人号码")
    private String driverCode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "订单开始日期")
    private Date beginDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "订单结束日期")
    private Date completeDate;

    @ApiModelProperty(value = "订单备注")
    private String orderRemarks;

    @ApiModelProperty(value = "订单价格")
    private Double freightAmount;

    @ApiModelProperty(value = "优惠券")
    private String coupon;

    @ApiModelProperty(value = "支付金额")
    private Double receiveAmount;

    @ApiModelProperty(value = "支付方式【1->支付宝；2->微信】")
    private Integer payType;

    @ApiModelProperty(value = "订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）")
    private Integer orderState;

    @ApiModelProperty(value = "订单评价")
    private String orderComment;
    /**
     * 找单表的id
     */
    @TableField(exist = false)
    private Integer orderId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private Date modifyDate;

    @ApiModelProperty(value = "逻辑删除/是否完成（0：否，1：是）")
    private Integer isDelete;

    @TableField(exist = false)
    @ApiModelProperty(value = "货物名称")
    private String goodsName;
    @TableField(exist = false)
    @ApiModelProperty(value = "货物类型id")
    private String typeId;
    @TableField(exist = false)
    @ApiModelProperty(value = "货物重量")
    private Double goodsWeight;

    public Date getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }
}
