package com.logistics.owner.order.publish.api;

import com.logistics.micro.common.model.ApiResponse;

/**
 * @author TBSWFGP
 * @creat 2021- 04-27-上午 10:44
 **/
public interface OwnerPublishService {
    /**
     * 通过订单编号查询订单
     * @param orderCode
     * @return
     */
    ApiResponse<?> getOrderOrderCode(String orderCode);

    /**
     * 找单信息状态
     * @param orderCode

     * @return
     */
    String getOrderOrderCodeStateFind(String orderCode);

    ApiResponse<?> order(DemandOrder demandOrder,Integer orderState);

    /**
     * 根据订单id修改订单状态
     * @return
     */
    ApiResponse<?> updateOrderState(String orderCode,Integer state);

}
