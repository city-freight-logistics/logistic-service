package com.logistics.micro.owner.order.provide.controller;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.message.api.OrderService;
import com.logistics.micro.owner.order.provide.dubboService.OwnerPublishDubboService;
import com.logistics.owner.order.publish.api.DemandOrder;
import com.logistics.micro.owner.order.provide.service.DemandOrderService;
import com.logistics.micro.owner.order.provide.service.GmsGoodsService;
import com.logistics.micro.owner.order.provide.service.TempOrderService;
import com.logistics.micro.owner.order.provide.utils.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-04-13
 */

@RestController
@RequestMapping("/demand-order")
@Api(tags = "生成订单模块")
public class DemandOrderController implements Serializable {
   @Reference
   private OrderService orderService;
   @Autowired
   private GmsGoodsService gmsGoodsService;
   @Autowired
   private DemandOrderService demandOrderService;
   @Autowired
   private RedisUtil redisUtil;
   @Autowired
   private   TempOrderService tempOrderService;
    @Autowired
    private OwnerPublishDubboService ownerPublishDubboService;
   /**
    * 货主预处理订单 下单 等待车主接单
    * @return
    */

   @ApiOperation(value = "用户拉货页面生成订单接口,参数在DemandOrder里面对应")
   @Transactional(rollbackFor = Exception.class)
   @PostMapping("/order")
   public ApiResponse<?> order(@RequestBody DemandOrder demandOrder){
      ApiResponse<?> response = ownerPublishDubboService.order(demandOrder,0);
      return response;
   }
   //根据距离和这个货车类型id获取金钱数量获取金钱数
   @GetMapping("/getMoney")
   public ApiResponse<Double> getMoney(Double orderDistance,Integer carId){
      System.out.println(orderDistance);
      Double money=0.0;
      if (carId==1){
         money= orderDistance>5?(30+Math.ceil(orderDistance-5)*2.5):30;
      }
      else if(carId==2){
         money= orderDistance>5?(55+Math.ceil(orderDistance-5)*3):55;
      } else if(carId==3){
         money= orderDistance>5?(65+Math.ceil(orderDistance-5)*3.5):65;
      } else if(carId==4){
         money= orderDistance>5?(65+Math.ceil(orderDistance-5)*4):65;
      }  else if(carId==5){
         money= orderDistance>5?(135+Math.ceil(orderDistance-5)*4.5):135;
      } else if(carId==6){
         money= orderDistance>5?(135+Math.ceil(orderDistance-5)*5):135;
      }  else if(carId==7){
         money= orderDistance>5?(250+Math.ceil(orderDistance-5)*5.5):250;
      }   else if(carId==8){
         money= orderDistance>5?(280+Math.ceil(orderDistance-5)*6):280;
      }  else if(carId==9){
         money= orderDistance>5?(350+Math.ceil(orderDistance-5)*7.5):350;
      }    else if(carId==10){
         money= orderDistance>5?(550+Math.ceil(orderDistance-5)*10):550;
      }   else if(carId==11){
         money= orderDistance>5?(1000+Math.ceil(orderDistance-5)*12):1000;
      }
      return  new ApiResponse<>(money);
   }

   /**
    * 评价订单
    * @param comment
    * @param id
    * @return
    */
   @GetMapping("/toComment")
   public ApiResponse<?> toComment(String comment,Integer id){
     if(!StringUtils.isNotBlank(comment)){
        return  new ApiResponse("评论不能为空");
     }
      DemandOrder order = new DemandOrder();
      order.setOrderState(8);
      order.setOrderComment(comment);
      boolean b = this.demandOrderService.update(order, new UpdateWrapper<DemandOrder>().eq("id", id));
      return new ApiResponse(b);
   }
}

