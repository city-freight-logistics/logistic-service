package com.logistics.micro.owner.order.provide.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户下单但还未接单表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-04-18
 */
@RestController
@RequestMapping("/temp-order")
public class TempOrderController {

}

