package com.logistics.micro.owner.order.provide.dubboService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.model.Header;
import com.logistics.micro.common.model.rabbit.RabbitMessage;
import com.logistics.micro.common.utils.DateUtils;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.message.api.OrderService;
import com.logistics.owner.order.publish.api.DemandOrder;
import com.logistics.micro.owner.order.provide.entity.GmsGoods;
import com.logistics.micro.owner.order.provide.entity.TempOrder;
import com.logistics.micro.owner.order.provide.entity.dto.DispatchOrderDto;
import com.logistics.micro.owner.order.provide.entity.dto.OrderDto;
import com.logistics.micro.owner.order.provide.mapper.DemandOrderMapper;
import com.logistics.micro.owner.order.provide.service.DemandOrderService;
import com.logistics.micro.owner.order.provide.service.GmsGoodsService;
import com.logistics.micro.owner.order.provide.service.TempOrderService;
import com.logistics.micro.owner.order.provide.utils.RedisUtil;
import com.logistics.owner.order.publish.api.OwnerPublishService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;

/**
 * @author TBSWFGP
 * @creat 2021- 04-27-上午 10:45
 **/
@Service
@Component
public class OwnerPublishDubboService extends ServiceImpl<DemandOrderMapper, DemandOrder> implements OwnerPublishService {
    @Reference
    private OrderService orderService;
    @Autowired
    private GmsGoodsService gmsGoodsService;
    @Autowired
    private DemandOrderService demandOrderService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private TempOrderService tempOrderService;

    @Override
    public ApiResponse<?> getOrderOrderCode(String orderCode) {

        DemandOrder order = this.baseMapper.selectOne(new QueryWrapper<DemandOrder>().eq("order_code", orderCode));
        return  new ApiResponse(order);
    }

    @Override
    public String getOrderOrderCodeStateFind(String orderCode) {
        DemandOrder order = this.baseMapper.selectOne(
                new QueryWrapper<DemandOrder>()
                        .eq("order_code", orderCode)
                        .eq("order_state", 0));
        return   GsonUtils.toJSON(order);
    }


    public ApiResponse<?> pickUpOrder(DemandOrder demandOrder){
        GmsGoods gmsGoods = new GmsGoods();
        gmsGoods.setGoodsName(demandOrder.getGoodsName());
        gmsGoods.setTypeId(demandOrder.getTypeId());
        gmsGoods.setGoodsWeight(demandOrder.getGoodsWeight());
        //存入货物信息表
        boolean save = gmsGoodsService.save(gmsGoods);
        if(!save){
            return new ApiResponse<>();
        }
        //随机生成一个订单号
        // Date类型转为指定格式的String类型
        String date = DateUtils.DateToString(new Date(), "yyyyMMddHHmmss");
        //在生成6个随机数字 加上data字符串作为订单号
        StringBuffer str=new StringBuffer();
        Random random=new Random();
        for (int i = 0; i < 6; i++) {
            str.append(random.nextInt(10));
        }

        str.append(date);
        //设置订单号
        demandOrder.setOrderCode(str.toString());
        //订单生成时间
        demandOrder.setCreateDate(new Date());
        //默认订单状态为0 下单状态
        demandOrder.setOrderState(0);
        //设置货物id
        demandOrder.setGoodsId(String.valueOf(gmsGoods.getGoodsId()));
        //保存订单到db
        boolean flag = demandOrderService.saveOrUpdate(demandOrder);
        //保存一个暂存表
        boolean b1 = tempOrderService.saveOrUpdate(new TempOrder(demandOrder));
        //存放订单到redis hash表
        OrderDto orderDto = new OrderDto(demandOrder);
        HashMap<String, Object> map = new HashMap<>();
        map.put(orderDto.getOrderCode(), GsonUtils.toJSON(orderDto));
        boolean b = redisUtil.hmset("order-temp", map);
        //消息推送
        RabbitMessage<DispatchOrderDto> rabbitMessage=new RabbitMessage<>();
        DispatchOrderDto dispatchOrderDto = new DispatchOrderDto(demandOrder);
        rabbitMessage.setBody(dispatchOrderDto);
        rabbitMessage.setDesc(dispatchOrderDto.getOrderName());
        orderService.providerSendOrderInfo(rabbitMessage);
        ApiResponse<Object> response = new ApiResponse<>();
        if(flag && b && b1){
            response.setHeader(new Header(HeaderStatus.SUCCESS));
        }
        return  response;
    }

    @Override
    public ApiResponse<?> order(DemandOrder demandOrder,Integer orderState){
        GmsGoods gmsGoods = new GmsGoods();
        gmsGoods.setGoodsName(demandOrder.getGoodsName());
        gmsGoods.setTypeId(demandOrder.getTypeId());
        gmsGoods.setGoodsWeight(demandOrder.getGoodsWeight());
        //存入货物信息表
        boolean save = gmsGoodsService.save(gmsGoods);
        if(!save){
            return new ApiResponse<>();
        }
        //判断当前是不是找单表找单表就不生成订单号了
        if(null==demandOrder.getOrderCode() || "".equals(demandOrder.getOrderCode())) {
            //随机生成一个订单号
            // Date类型转为指定格式的String类型
            String date = DateUtils.DateToString(new Date(), "yyyyMMddHHmmss");
            //在生成6个随机数字 加上data字符串作为订单号
            StringBuffer str=new StringBuffer();
            Random random=new Random();
            for (int i = 0; i < 6; i++) {
                str.append(random.nextInt(10));
            }
            str.append(date);
            //设置订单号
            demandOrder.setOrderCode(str.toString());
        }
        //订单生成时间
        demandOrder.setCreateDate(new Date());
        demandOrder.setBeginDate(new Date());
        demandOrder.setOrderState(orderState);
        //设置货物id
        demandOrder.setGoodsId(String.valueOf(gmsGoods.getGoodsId()));
        if (Objects.nonNull(demandOrderService.getOne(new QueryWrapper<DemandOrder>().eq("order_code", demandOrder.getOrderCode())))) {
            return new ApiResponse<>(HeaderStatus.SUCCESS);
        }
        //保存订单到db
        boolean flag = demandOrderService.saveOrUpdate(demandOrder);
        //保存一个暂存表
        boolean b1 = tempOrderService.saveOrUpdate(new TempOrder(demandOrder));
        //存放订单到redis hash表
        OrderDto orderDto = new OrderDto(demandOrder);
        HashMap<String, Object> map = new HashMap<>();
        map.put(orderDto.getOrderCode(), GsonUtils.toJSON(orderDto));
        boolean b = redisUtil.hmset("order-temp", map);
        //消息推送
        RabbitMessage<DispatchOrderDto> rabbitMessage=new RabbitMessage<>();
        DispatchOrderDto dispatchOrderDto = new DispatchOrderDto(demandOrder);
        rabbitMessage.setBody(dispatchOrderDto);
        rabbitMessage.setDesc(dispatchOrderDto.getOrderName());
        orderService.providerSendOrderInfo(rabbitMessage);
        ApiResponse<Object> response = new ApiResponse<>();
        if(flag && b && b1){
            response.setHeader(new Header(HeaderStatus.SUCCESS));
        }
        return  response;
    }

    @Override
    public ApiResponse<?> updateOrderState(String orderCode, Integer state) {

        DemandOrder order = new DemandOrder();
        order.setOrderState(state);
        UpdateWrapper<DemandOrder> wrapper = new UpdateWrapper<>();
        wrapper.eq("order_code", orderCode);
        boolean b = this.update(order, wrapper);
        return new ApiResponse<>(b);
    }


}
