package com.logistics.micro.owner.order.provide.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 货物表
 * </p>
 *
 * @author ${author}
 * @since 2021-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("GMS_GOODS")
@ApiModel(value="GmsGoods对象", description="货物表")
public class GmsGoods implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "goods_id", type = IdType.AUTO)
    private Integer goodsId;

    @ApiModelProperty(value = "货物名称")
    private String goodsName;

    @ApiModelProperty(value = "货物类型id")
    private String typeId;

    @ApiModelProperty(value = "货物重量")
    private Double goodsWeight;

    @ApiModelProperty(value = "货物体积")
    private String goodsVolume;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "修改时间")
    private Date modifyDate;

    @ApiModelProperty(value = "逻辑删除（0：否，1：是）")
    private Integer isDelete;

    @ApiModelProperty(value = "司机接单概率默认100")
    private Integer acceptRate;


}
