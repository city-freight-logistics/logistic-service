package com.logistics.micro.owner.order.provide.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.logistics.owner.order.publish.api.DemandOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户下单但还未接单表
 * </p>
 *
 * @author ${author}
 * @since 2021-04-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("OMS_TEMP_ORDER")
@ApiModel(value="TempOrder对象", description="用户下单但还未接单表")
public class TempOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String orderCode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private Date modifyDate;

    @ApiModelProperty(value = "是否完成（0：否，1：是）")
    private Integer isDelete;

    public TempOrder(DemandOrder demandOrder) {
        this.id=demandOrder.getId();
        this.isDelete=demandOrder.getIsDelete();
        this.modifyDate=demandOrder.getModifyDate();
        this.orderCode=demandOrder.getOrderCode();
        this.createDate=demandOrder.getCreateDate();

    }
}
