package com.logistics.micro.owner.order.provide.entity.dto;

import com.logistics.owner.order.publish.api.DemandOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 即将派发出去的订单信息
 *
 * @author xionghw0982
 * @date 2021-04-09 14:13
 */
@Data
public class DispatchOrderDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Integer id;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 订单名称
     */
    private String orderName;
    /**
     * 货物编号
     */
    private String goodsId;
    /**
     * 客户姓名
     */
    private String customerName;
    /**
     * 运费
     */
    private Double freightAmount;
    /**
     * 起点到终点的距离
     */
    private String distanceOrder;
    /**
     * 订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）
     */
    private Integer orderState;
    /**
     * 下单位置-纬度
     */
    private String latitude;
    /**
     * 下单位置-经度
     */
    private String longitude;

    /**
     * 订单开始时间
     */
    private String beginDate;

    @ApiModelProperty(value = "货物类型name")
    private String typeName;

    public DispatchOrderDto(DemandOrder demandOrder) {
      this.id=demandOrder.getId();
      this.customerName=demandOrder.getCustomerName();
      this.freightAmount=demandOrder.getFreightAmount();
      this.goodsId=demandOrder.getGoodsId();
      this.latitude= demandOrder.getLatitude();
      this.longitude= demandOrder.getLongitude();
      this.orderState=demandOrder.getOrderState();
      this.freightAmount=demandOrder.getFreightAmount();
      this.orderCode=demandOrder.getOrderCode();
      this.orderName=demandOrder.getOrderName();
      this.distanceOrder= demandOrder.getOrderDistance();
      this.typeName=demandOrder.getTypeName();
      this.beginDate=demandOrder.getBeginDate().toString();
    }
}
