package com.logistics.micro.owner.order.provide.mapper;

import com.logistics.owner.order.publish.api.DemandOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-04-13
 */
@Mapper
public interface DemandOrderMapper extends BaseMapper<DemandOrder> {

}
