package com.logistics.micro.owner.order.provide.mapper;

import com.logistics.micro.owner.order.provide.entity.TempOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户下单但还未接单表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-04-18
 */
public interface TempOrderMapper extends BaseMapper<TempOrder> {

}
