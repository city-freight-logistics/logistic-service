package com.logistics.micro.owner.order.provide.service;

import com.logistics.micro.owner.order.provide.entity.TempOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户下单但还未接单表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-18
 */
public interface TempOrderService extends IService<TempOrder> {

}
