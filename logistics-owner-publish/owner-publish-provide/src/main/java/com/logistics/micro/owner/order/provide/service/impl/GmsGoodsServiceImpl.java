package com.logistics.micro.owner.order.provide.service.impl;

import com.logistics.micro.owner.order.provide.entity.GmsGoods;
import com.logistics.micro.owner.order.provide.mapper.GmsGoodsMapper;
import com.logistics.micro.owner.order.provide.service.GmsGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 货物表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-28
 */
@Service
public class GmsGoodsServiceImpl extends ServiceImpl<GmsGoodsMapper, GmsGoods> implements GmsGoodsService {

}
