package com.logistics.micro.owner.order.provide.service.impl;

import com.logistics.micro.owner.order.provide.entity.TempOrder;
import com.logistics.micro.owner.order.provide.mapper.TempOrderMapper;
import com.logistics.micro.owner.order.provide.service.TempOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户下单但还未接单表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-04-18
 */
@Service
public class TempOrderServiceImpl extends ServiceImpl<TempOrderMapper, TempOrder> implements TempOrderService {

}
