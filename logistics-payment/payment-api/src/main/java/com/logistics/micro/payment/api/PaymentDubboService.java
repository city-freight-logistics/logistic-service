package com.logistics.micro.payment.api;

import java.util.Map;

import com.logistics.micro.common.model.ApiResponse;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/04/25 1:11
 */
public interface PaymentDubboService {


	/**
	 * 用户支付
	 *
	 * @param orderDto
	 * @return
	 */
	ApiResponse payByUser(String orderDto);

	/**
	 * 司机收款
	 *
	 * @param orderDto
	 * @return
	 */
	ApiResponse getByDriver(String orderDto);

	/**
	 * 退款
	 */
	ApiResponse refund(String orderDto);

}
