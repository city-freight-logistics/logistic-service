package com.logistics.micro.payment.provide;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/04/24 22:28
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.logistics.micro.payment.provide.dao")
public class PaymentBootstrap {
	public static void main(String[] args) {
		SpringApplication.run(PaymentBootstrap.class, args);
	}

}
