package com.logistics.micro.payment.provide.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.payment.provide.entity.Pay;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hj
 * @date 2021-04-24 00:34:54
 */
@Mapper
public interface PayDao extends BaseMapper<Pay> {

}
