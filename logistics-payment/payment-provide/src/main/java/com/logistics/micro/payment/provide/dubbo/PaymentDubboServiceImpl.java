package com.logistics.micro.payment.provide.dubbo;

import java.util.Map;

import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.order.api.DriverOrderService;
import com.logistics.micro.owner.manage.api.OwnerManageService;
import com.logistics.micro.payment.api.PaymentDubboService;
import com.logistics.micro.payment.provide.entity.Pay;
import com.logistics.micro.payment.provide.entity.dto.DemandOrderDto;
import com.logistics.micro.payment.provide.entity.dto.OrderDto;
import com.logistics.micro.payment.provide.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/04/25 1:10
 */
@Slf4j
@Service
public class PaymentDubboServiceImpl implements PaymentDubboService {

	@Reference
	DriverOrderService driverOrderService;

	@Reference
	OwnerManageService ownerManageService;
	@Autowired
	PayService payService;

	@Override
	public ApiResponse payByUser(String orderDto) {
		final OrderDto orderOwner= GsonUtils.fromJson(orderDto, OrderDto.class);
		System.out.println(orderOwner);
		log.info("orderOwner",orderOwner);
		if (orderOwner.getOrderState() == 1) {
			return new ApiResponse(orderOwner);
			//进行支付
		} else if (orderOwner.getOrderState() == 4) {
			payService.payByUser(orderOwner);
				return new ApiResponse(HeaderStatus.SUCCESS, "支付成功");
		}
		return new ApiResponse(HeaderStatus.ERROR, "支付失败");
	}

	@Override
	public ApiResponse getByDriver(String orderDto) {
		final OrderDto order = GsonUtils.fromJson(orderDto, OrderDto.class);
		ApiResponse driverOrder = driverOrderService.getOneByOrderCode(order.getOrderCode());
		Pay pay = payService.getOneByOrderCode(order.getOrderCode());
		OrderDto orderBody = (OrderDto) driverOrder.getBody();
		if (orderBody == null && pay == null) {
			return new ApiResponse<>("订单不存在");
		}
		if (orderBody.getOrderState() == 1) {
			return new ApiResponse(orderBody);
		} else if (orderBody.getOrderState() == 0 && pay.getStatus() == 0) {
			orderBody.setOrderState(5);
			orderBody.setReceiveAmount(pay.getMoney());
			pay.setPayeeBy(orderBody.getId());
			ApiResponse apiResponse = driverOrderService.updateOne(orderBody.toString());
			payService.getPyDriver(pay);
			if (apiResponse.getBody().equals("更新成功")) {
				return new ApiResponse(HeaderStatus.SUCCESS, "收款成功");
			}
		}
		return new ApiResponse(HeaderStatus.ERROR, "收款失败");
	}

	@Override
	public ApiResponse refund(String orderDto) {
		return null;
	}
}
