package com.logistics.micro.payment.provide.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * 支付实体
 *
 * @author ju.huang02@hand-chian.com 2021/05/09 0:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("MMS_PAY")
public class Pay implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	@Id
	private Integer id;
	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 订单名称
	 */
	private String orderName;
	/**
	 * 交易金额
	 */
	private String money;
	/**
	 * 支付状态，0未支付，1支付
	 */
	private Integer status;
	/**
	 * 付款人id
	 */
	private Integer payBy;
	/**
	 * 收款人id
	 */
	private Integer payeeBy;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 修改时间
	 */
	private Date modifyDate;
	/**
	 * 是否删除0：否，1：删除
	 */
	private Integer isDelete;

}
