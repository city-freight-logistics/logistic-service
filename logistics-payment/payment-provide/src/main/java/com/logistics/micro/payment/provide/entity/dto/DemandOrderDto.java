package com.logistics.micro.payment.provide.entity.dto;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/05/28 18:42
 */
@Data
public class DemandOrderDto {

	private static final long serialVersionUID = 1L;

	private Integer id;


	private String orderCode;


	private String orderName;


	private String orderType;


	private String goodsId;


	private String customerName;


	private  String latitude;


	private  String longitude;

	private  String orderDistance;

	private String customerTel;


	private String consigneeName;


	private String consigneeTel;


	private String customerType;


	private String driverCode;


	private Date beginDate;


	private Date completeDate;


	private String orderRemarks;


	private String freightAmount;


	private String coupon;


	private String receiveAmount;


	private Integer payType;

	// @ApiModelProperty(value = "订单状态（整型 0：已下单，1：已付款，2：受理中，3：运输中，4：处理完毕，5：已收款，6：已结束，7：待评价，8：已评价）")
	private Integer orderState;

	// @ApiModelProperty(value = "订单评价")
	private String orderComment;

	// @ApiModelProperty(value = "创建时间")
	private Date createDate;

	// @ApiModelProperty(value = "修改时间")
	private Date modifyDate;

	// @ApiModelProperty(value = "逻辑删除/是否完成（0：否，1：是）")
	private Integer isDelete;

}
