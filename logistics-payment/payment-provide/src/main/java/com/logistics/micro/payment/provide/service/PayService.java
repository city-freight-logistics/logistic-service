package com.logistics.micro.payment.provide.service;

import com.logistics.micro.payment.provide.entity.Pay;
import com.logistics.micro.payment.provide.entity.dto.DemandOrderDto;
import com.logistics.micro.payment.provide.entity.dto.OrderDto;


/**
 * @author hj
 * @date 2021-04-24 00:34:54
 */
public interface PayService {


	/**
	 * 用户支付
	 *
	 * @param orderOwner
	 */
	void payByUser(OrderDto orderOwner);

	/**
	 * 查询订单
	 *
	 * @param orderCode
	 * @return
	 */
	Pay getOneByOrderCode(String orderCode);

	/**
	 * 司机收款修改订单
	 *
	 * @param pay
	 */
	void getPyDriver(Pay pay);
}

