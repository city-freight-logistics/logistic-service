package com.logistics.micro.payment.provide.service.impl;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.payment.provide.dao.PayDao;
import com.logistics.micro.payment.provide.entity.Pay;
import com.logistics.micro.payment.provide.entity.dto.DemandOrderDto;
import com.logistics.micro.payment.provide.entity.dto.OrderDto;
import com.logistics.micro.payment.provide.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/05/09 0:32
 */
@Service
public class PayServiceImpl implements PayService {


	@Autowired
	private PayDao payDao;

	@Override
	public void payByUser(OrderDto orderOwner) {
		Pay pay = new Pay() {{
			setOrderCode(orderOwner.getOrderCode());
			setMoney(orderOwner.getFreightAmount());
			setOrderName(orderOwner.getOrderName());
			setPayBy(orderOwner.getId());
		}};
		payDao.insert(pay);
	}

	@Override
	public Pay getOneByOrderCode(String orderCode) {
		UpdateWrapper<Pay> wrapper = new UpdateWrapper<>();
		wrapper.eq("order_code", orderCode);
		Pay pay = payDao.selectOne(wrapper);
		return pay;
	}

	@Override
	public void getPyDriver(Pay pay) {
		pay.setStatus(1);
		payDao.updateById(pay);
	}
}
