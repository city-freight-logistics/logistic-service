package com.logistics.mircro.third.party.provide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xionghw0982
 * @version 1.0
 * @date 2021/4/1 21:52
 * @description
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ThirdPartyServiceBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(ThirdPartyServiceBootstrap.class,args);
    }
}
