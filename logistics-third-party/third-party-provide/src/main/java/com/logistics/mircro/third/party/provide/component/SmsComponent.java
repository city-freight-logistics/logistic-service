package com.logistics.mircro.third.party.provide.component;

import com.logistics.mircro.third.party.provide.util.HttpUtils;
import lombok.Data;
import org.apache.http.HttpResponse;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: HuangJu
 * @Description: 短信发送的实现类
 * @Date: 0:42 2021/4/8
 */
@Component
@Data
@ConfigurationProperties(prefix = "sms")
public class SmsComponent {

	private String tpl_id;
	private String host;
	private String path;
	private String appcode;

	public void sendSmsCode(String phone, String code) {
		String method = "POST";
		Map<String, String> headers = new HashMap<String, String>();
		//最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
		headers.put("Authorization", "APPCODE " + "51b3a7d7913042de8754ac7237a21c69");
		Map<String, String> querys = new HashMap<String, String>();
		querys.put("mobile", phone);
		querys.put("param", code);
		querys.put("tpl_id", "TP1711063");
		Map<String, String> bodys = new HashMap<String, String>();
		try {

			HttpResponse response = HttpUtils.doPost("http://dingxin.market.alicloudapi.com", "/dx/sendSms", method, headers, querys, bodys);
			System.out.println(response.toString());
			//获取response的body
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
