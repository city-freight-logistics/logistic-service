package com.logistics.mircro.third.party.provide.controller;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.mircro.third.party.provide.util.MinioUtil;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wuyue5
 * @version 1.0
 * @since 2021-05-26 21:07
 */
@RestController
@RequestMapping("provide/third-party")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class MinioController {

    private final MinioUtil minioUtil;

    @PostMapping("/minio/upload")
    public ApiResponse<?> upload(@RequestParam("file") MultipartFile file) {
        return new ApiResponse<>(minioUtil.upLoadMultipartFile(file));
    }
}
