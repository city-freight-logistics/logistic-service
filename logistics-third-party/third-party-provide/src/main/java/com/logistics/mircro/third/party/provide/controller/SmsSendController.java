package com.logistics.mircro.third.party.provide.controller;

import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.mircro.third.party.provide.component.SmsComponent;
import org.springframework.web.bind.annotation.*;

/**
 * description
 *
 * @author ju.huang02@hand-china.com 2021/04/12 19:07
 */
@RestController
@RequestMapping("/sms")
public class SmsSendController {

	private final SmsComponent smsComponent;

	public SmsSendController(SmsComponent smsComponent) {
		this.smsComponent = smsComponent;
	}

	/**
	 * 提供给其他服务调用
	 *
	 * @param phone
	 * @return
	 */
	@PostMapping("/sendCode")
	public ApiResponse sendCode(@RequestParam("phone") String phone, @RequestParam("code") String code) {
		smsComponent.sendSmsCode(phone, code);

		return new ApiResponse(HeaderStatus.SUCCESS);
	}

}
