package com.logistics.micro.user.api;

import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.user.dto.DriverAdminQueryDto;
import com.logistics.micro.user.dto.UserQueryListDto;

/**
 * 外部数据获取/操作相关接口
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 23:02
 */
public interface UserAdminService {

    /**
     * 条件查询获取
     *
     * @param queryListDto
     * @return
     */
    ApiResponse<?> queryUserList(UserQueryListDto queryListDto);

    /**
     * 管理端查询司机
     *
     * @param queryDto
     * @return
     */
    ApiResponse<?> getAdminDriver(DriverAdminQueryDto queryDto);

    /**
     * 司机审核接口
     *
     * @param driverAdminVo
     * @return
     */
    ApiResponse<?> examineDriver(String driverAdminVo);

    /**
     * 司机状态禁用解封接口
     *
     * @param driverAdminVo
     * @return
     */
    ApiResponse<?> disableOrRe(String driverAdminVo);

    /**
     * 根据司机编号更新车辆数
     *
     * @param driverCode
     * @return
     */
    Integer setVehicleNumByDriverCode(Integer carNum, String driverCode);

    Integer getNewUser();
}
