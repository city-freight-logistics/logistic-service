package com.logistics.micro.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/5 11:46
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DriverAdminQueryDto implements Serializable {
    private String page;
    private String limit;
    private String driverCode;
    private String driverName;
    private String driverState;
}
