package com.logistics.micro.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户列表页查询Dto
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 22:59
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserQueryListDto implements Serializable {
    private String page;
    private String limit;
    private String userId;
    private String userName;
    private String userType;
}
