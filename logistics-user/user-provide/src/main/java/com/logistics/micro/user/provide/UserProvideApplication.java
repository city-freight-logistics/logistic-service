package com.logistics.micro.user.provide;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @author: HuangJu
 * @Description:
 * @Date: 22:50 2021/4/7
 */
@EnableRedisHttpSession
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.logistics.micro.user.provide.dao")
public class UserProvideApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserProvideApplication.class,args);

    }
}
