package com.logistics.micro.user.provide.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

/**
 * description
 *
 * @author ju.huang02@hand-china.com 2021/04/19 22:55
 */
@Configuration
public class SessionConfig {

	@Bean
	public CookieSerializer cookieSerializer(){
		DefaultCookieSerializer defaultCookieSerializer = new DefaultCookieSerializer();

//		提高session域名
//		defaultCookieSerializer.setDomainName("");
		return defaultCookieSerializer;

	}

	public RedisSerializer<Object> springSessionDefaultRedisSerializer(){
		return new GenericJackson2JsonRedisSerializer();
	}

}
