package com.logistics.micro.user.provide.controller;

import java.sql.Wrapper;
import java.util.Arrays;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.user.provide.entity.dto.DriverDto;
import com.logistics.micro.user.provide.entity.pojo.DriverPO;
import com.logistics.micro.user.provide.entity.pojo.UserPO;
import com.logistics.micro.user.provide.service.DriverService;
import com.logistics.micro.user.provide.service.UserService;
import com.logistics.micro.user.provide.util.CodeUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/05/14 2:47
 */
@RestController
@RequestMapping("/provide/attestation")
public class DriverController {

	@Autowired
	private UserService userService;

	@Autowired
	private DriverService driverService;

	/**
	 * 提交司机验证申请
	 */
	@PostMapping("/save")
	public ApiResponse<String> save(@RequestBody DriverDto driverDto) {
		UserPO userPO = userService.isNumber(driverDto.getUserId());
		DriverPO driverPO = new DriverPO();
		BeanUtils.copyProperties(driverDto, driverPO);
		if (StringUtils.isEmpty(userPO.getIdNumber())) {
			return new ApiResponse<>(HeaderStatus.ERROR,"未实名制请先实名制");
		}
		if (driverPO.getIdNumber().equals(userPO.getIdNumber())) {
			driverPO.setCode(CodeUtil.code());
			final boolean save = driverService.save(driverPO);
			if (save) {
				return new ApiResponse<>("提交成功，等待管理员审批");
			}
		} else {
			return new ApiResponse<>(HeaderStatus.ERROR,"身份证和账号实名信息不一致");
		}
		return new ApiResponse<>(HeaderStatus.ERROR, "提交失败");
	}

	@PostMapping("/update")
	public ApiResponse<String> update(@RequestBody DriverDto driverDto) {
		ApiResponse apiResponse = driverService.updateInfo(driverDto);
		return apiResponse;
	}


		/**
	 * 信息
	 */
	@GetMapping("/info")
	public ApiResponse<DriverPO> info(String idNumber) {
		DriverPO driverPO = driverService.getOne(new QueryWrapper<DriverPO>().eq("id_number", idNumber).eq("is_delete",0));
		return new ApiResponse<>(HeaderStatus.SUCCESS,driverPO);
	}

	/**
	 * 删除
	 */
	@GetMapping("/delete/{id}")
	public ApiResponse<String> delete(@PathVariable("id") Integer id) {
		ApiResponse apiResponse = driverService.deleteById(id);
		return apiResponse;
	}
}
