package com.logistics.micro.user.provide.controller;

import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.user.provide.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * description
 *
 * @author ju.huang02@hand-china.com 2021/04/12 19:07
 */
@RestController
@RequestMapping("/sms")
public class SmsSendController {

	private final SmsComponent smsComponent;

	@Autowired
	public SmsSendController(SmsComponent smsComponent) {
		this.smsComponent = smsComponent;
	}

	/**
	 * 提供给其他服务调用
	 *
	 * @param phone
	 * @return
	 */
	@PostMapping("/sendCode")
	public Boolean sendCode(@RequestParam("phone") String phone, @RequestParam("code") String code) {
		smsComponent.sendSmsCode(phone, code);
		return true;
	}

}
