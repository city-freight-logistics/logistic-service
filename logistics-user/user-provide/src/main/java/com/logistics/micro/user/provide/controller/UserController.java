package com.logistics.micro.user.provide.controller;

import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.logistics.micro.common.constant.AuthServerConstant;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.JwtUtils;
import com.logistics.micro.user.provide.entity.dto.UpdatePasswordDto;
import com.logistics.micro.user.provide.entity.pojo.UserPO;
import com.logistics.micro.user.provide.entity.vo.UserRegistVo;
import com.logistics.micro.user.provide.service.UserService;
import com.mysql.cj.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.enums.HeaderStatus;


/**
 * @author hj
 * @date 2021-04-18 14:40:37
 */
@Slf4j
@RestController
@RequestMapping("/provide/user")
public class UserController {

	@Autowired
	private UserService userService;


	private final SmsSendController smsSendController;

	private final StringRedisTemplate redisTemplate;


	@Autowired
	public UserController(SmsSendController smsSendController, StringRedisTemplate redisTemplate) {
		this.smsSendController = smsSendController;
		this.redisTemplate = redisTemplate;
	}

	/**
	 * 修改密码
	 */
	@PostMapping("/updatePassword")
	public ApiResponse updatePassword(@RequestBody UpdatePasswordDto updatePasswordDto) {
		ApiResponse apiResponse = userService.updatePassword(updatePasswordDto);
		return apiResponse;
	}

	/**
	 * 发送注册验证码
	 *
	 * @param phone
	 * @return
	 */
	@GetMapping("/sendCode")
	public ApiResponse sendCode(@RequestParam("phone") String phone) {
		log.info("开始进行短信发送");
		//判断手机号是否已经注册过
		UserPO user = userService.getOne(new QueryWrapper<UserPO>().eq("phone", phone));
		if (!ObjectUtils.isEmpty(user)) {
			return new ApiResponse(HeaderStatus.ERROR, "该手机号已经被注册");
		}
		//接口防刷
		String redisCode = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone);
		if (!StringUtils.isNullOrEmpty(redisCode)) {
			long l = Long.parseLong(redisCode.split("_")[1]);
			Long min = 60000L;
			if (System.currentTimeMillis() - 1 < min) {
				//60秒内不能再发
				return new ApiResponse(HeaderStatus.SMS_CODE_EXCEPTION,"验证码频率过高");
			}
		}

		//2.验证码的再次验证。redis
		int c = (int) ((Math.random() * 9 + 1) * 10000);
		String code = String.valueOf(c);
		String code1 = code + "_" + System.currentTimeMillis();
		code = new String("code:") + code;
		//redis缓存验证码，防止同一个phone在60秒内再次发送验证码
		redisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone, code1, 5, TimeUnit.MINUTES);

		Boolean check = smsSendController.sendCode(phone, code);
		if (check == true) {
			return new ApiResponse(HeaderStatus.SUCCESS, "验证码发送成功");
		} else {
			return new ApiResponse(HeaderStatus.ERROR, "验证码发送失败");
		}
	}

	/**
	 * 注册功能
	 *
	 * @param user
	 * @return
	 */
	@PostMapping("/register")
	public ApiResponse regist(@RequestBody UserRegistVo user) {
		UserRegistVo vo = user;

		//1、校验验证码
		String code = vo.getCode();
		String s = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());
		if (!StringUtils.isNullOrEmpty(s)) {

			if (code.equals(s.split("_")[0])) {
				//删除验证码,令牌机制
				redisTemplate.delete(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());
				//验证通过。进行注册
				boolean save = userService.regist(vo);
				if (save) {
					return new ApiResponse<>(HeaderStatus.SUCCESS, "保存成功");
				}

				return new ApiResponse<>(HeaderStatus.ERROR, "保存失败");

			} else {
				HashMap<String, String> errors = new HashMap<>();
				errors.put("code", "验证码错误");
				return new ApiResponse(HeaderStatus.ERROR);
			}
		} else {
			HashMap<String, String> errors = new HashMap<>();
			errors.put("code", "验证码错误");
			return new ApiResponse(HeaderStatus.ERROR, "验证出错");
		}
	}

	/**
	 * 登录
	 *
	 * @param user
	 * @return
	 */
	@PostMapping("/login")
	public ApiResponse<String> list(@RequestBody UserRegistVo user) {
		ApiResponse apiResponse = userService.login(user.getPhone(), user.getPassword());
		return apiResponse;
	}

	/**
	 * 根据token获取用户信息
	 */
	@PostMapping("/getInfo")
	public ApiResponse<UserPO> getInfo(HttpServletRequest request) {
		String jwtToken = request.getHeader("token");
		String id = JwtUtils.getMemberIdByJwtToken(jwtToken);
		UserPO userPO = userService.selectById(id);
		return new ApiResponse<>(HeaderStatus.SUCCESS, userPO);
	}

	/**
	 * 发送验证码更新密码
	 */
	@GetMapping("/smsSend")
	public ApiResponse smsSend(@RequestParam("phone") String phone) {
		//接口防刷
		String redisCode = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_FORGET_CODE + phone);
		if (!StringUtils.isNullOrEmpty(redisCode)) {
			long l = Long.parseLong(redisCode.split("_")[1]);
			Long min = 60000L;
			if (System.currentTimeMillis() - 1 < min) {
				//60秒内不能再发
				return new ApiResponse(HeaderStatus.SMS_CODE_EXCEPTION);
			}
		}

		//2.验证码的再次验证。redis
		int c = (int) ((Math.random() * 9 + 1) * 10000);
		String code = String.valueOf(c);
		String code1 = code + "_" + System.currentTimeMillis();
		code = new String("code:") + code;
		//redis缓存验证码，防止同一个phone在60秒内再次发送验证码
		redisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_FORGET_CODE + phone, code1, 5, TimeUnit.MINUTES);
		Boolean check = smsSendController.sendCode(phone, code);
		if (check == true) {
			return new ApiResponse(HeaderStatus.SUCCESS, "验证码发送成功");
		} else {
			return new ApiResponse(HeaderStatus.ERROR, "验证码发送失败");
		}
	}

	/**
	 * 重置密码
	 */
	@PostMapping("/forget")
	public ApiResponse forget(@RequestBody UserRegistVo vo) {
		//1、校验验证码
		String code = vo.getCode();
		String s = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_FORGET_CODE + vo.getPhone());
		if (!StringUtils.isNullOrEmpty(s)) {

			if (code.equals(s.split("_")[0])) {
				//删除验证码,令牌机制
				redisTemplate.delete(AuthServerConstant.SMS_CODE_FORGET_CODE + vo.getPhone());
				//验证通过。进行注册
				boolean save = userService.forget(vo);
				if (save) {
					return new ApiResponse<>(HeaderStatus.SUCCESS, "保存成功");
				}
				return new ApiResponse<>(HeaderStatus.ERROR, "保存失败");

			} else {
				HashMap<String, String> errors = new HashMap<>();
				errors.put("code", "验证码错误");
				return new ApiResponse(HeaderStatus.ERROR);
			}
		} else {
			HashMap<String, String> errors = new HashMap<>();
			errors.put("code", "验证码错误");
			return new ApiResponse(HeaderStatus.ERROR, "验证出错");
		}
	}

	/**
	 * 更新个人信息
	 */
	@PostMapping("/save")
	public ApiResponse<String> save(@RequestBody UserPO userPO) {
		ApiResponse apiResponse = userService.saveById(userPO);
		return apiResponse;
	}

	/**
	 * 实名
	 */
	@PostMapping("update")
	public ApiResponse update(@RequestBody UserPO userPO) {

		ApiResponse apiResponse = userService.realName(userPO);
		return apiResponse;
	}

	/**
	 * 退出登录
	 */
	@GetMapping("/outLogin")
	public ApiResponse outLogin(HttpServletRequest request) {
		String jwtToken = request.getHeader("token");
		boolean checkToken = JwtUtils.checkToken(jwtToken);
		if (checkToken) {
			redisTemplate.delete(AuthServerConstant.JWT_TOKEN + jwtToken);
			return new ApiResponse(HeaderStatus.SUCCESS, "退出成功");
		}
		return new ApiResponse(HeaderStatus.ERROR, "退出失败");
	}
}
