package com.logistics.micro.user.provide.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.logistics.micro.user.dto.DriverAdminQueryDto;
import com.logistics.micro.user.provide.entity.pojo.DriverPO;
import com.logistics.micro.user.provide.entity.vo.DriverAdminVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 车辆信息表
 * 
 * @author hj
 * @date 2021-04-22 22:46:06
 */
@Mapper
public interface DriverDao extends BaseMapper<DriverPO> {

    IPage<DriverAdminVo> getAdminDriver(Page<?> page, @Param("query") DriverAdminQueryDto queryDto);

}
