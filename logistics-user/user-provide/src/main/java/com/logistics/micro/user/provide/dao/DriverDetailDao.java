package com.logistics.micro.user.provide.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.user.provide.entity.pojo.DriverDetailPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 
 * 
 * @author hj
 * @date 2021-04-25 02:05:37
 */
@Mapper
public interface DriverDetailDao extends BaseMapper<DriverDetailPO> {

    @Update("update CMS_DRIVER_DETAIL set car_number = #{carNum} where driver_code = #{driverCode}")
    int updateCarNumber(@Param("carNum") Integer carNum, @Param("driverCode") String driverCode);
	
}
