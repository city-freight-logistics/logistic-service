package com.logistics.micro.user.provide.dao;

import java.util.List;

import com.logistics.micro.user.provide.entity.dto.DriverDto;
import org.apache.ibatis.annotations.Mapper;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/04/25 2:49
 */
@Mapper
public interface DriverDtoDao {

	/**
	 * 查询所有需要认证的司机
	 *
	 * @return
	 */
	List<DriverDto> qurryAll();
}
