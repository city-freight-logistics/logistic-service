package com.logistics.micro.user.provide.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.user.provide.entity.pojo.UserPO;
import com.logistics.micro.user.provide.entity.vo.UserRegistVo;
import org.apache.ibatis.annotations.*;

/**
 * @author hj
 * @date 2021-04-18 14:40:37
 */
@Mapper
public interface UserDao extends BaseMapper<UserPO> {


	/**
	 * 查询手机号是否唯一
	 *
	 * @param phone
	 */
	@Select("select count(*) from UMS_USER where phone = #{phone}")
	Integer selectByPhone(String phone);

	/**
	 * 登录
	 *
	 * @param phone
	 */
	@Select("select * from UMS_USER where phone= #{phone}")
	UserPO login(@Param("phone") String phone);


	@Update("update UMS_USER set driver_code= #{driverCode},role = 'driver' where id_number= #{idNumber}")
	Integer updateByIdNubmer(@Param("idNumber") String idNumber, @Param("driverCode")String driverCode);

	@Select("SELECT " +
			"	COUNT( * ) " +
			"FROM " +
			"	UMS_USER " +
			"WHERE " +
			"	DATE_FORMAT( create_date, '%m-%d' ) = DATE_FORMAT( #{date}, '%m-%d' ) ")
	int newUserCount(@Param("date") String date);
}
