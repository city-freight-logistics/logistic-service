package com.logistics.micro.user.provide.dubboService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.DateUtils;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.user.api.UserAdminService;
import com.logistics.micro.user.dto.DriverAdminQueryDto;
import com.logistics.micro.user.dto.UserQueryListDto;
import com.logistics.micro.user.provide.dao.DriverDetailDao;
import com.logistics.micro.user.provide.dao.UserDao;
import com.logistics.micro.user.provide.entity.pojo.UserPO;
import com.logistics.micro.user.provide.service.DriverService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/2 23:03
 */
@Service
@Slf4j
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class UserAdminServiceImpl extends ServiceImpl<UserDao, UserPO> implements UserAdminService {

    private final DriverService driverService;
    private final DriverDetailDao driverDetailDao;
    private final UserDao userDao;

    @Override
    public ApiResponse<?> queryUserList(UserQueryListDto queryListDto) {
        try {
            if (Objects.isNull(queryListDto)) {
                return new ApiResponse<>(HeaderStatus.INPUT_DATA_NULL);
            }
            Map<String, Object> params = GsonUtils.strToMap(GsonUtils.toJSON(queryListDto));
            IPage<UserPO> page = getPageFromDB(params);
            final PageUtils pageUtils = new PageUtils(page);
            if (pageUtils.getTotalCount() == 0){
                return new ApiResponse<>(HeaderStatus.PAGE_CONTEXT_NULL_INFO);
            }
            return new ApiResponse<>(GsonUtils.beanToMap(pageUtils));
        } catch (Exception e) {
            log.info("UserAdminServiceImpl -> queryUserList() ret={} exception={}",queryListDto.toString(), e.getMessage());
            return new ApiResponse<>(HeaderStatus.EXCPEITON);
        }
    }

    @Override
    public ApiResponse<?> getAdminDriver(DriverAdminQueryDto queryDto) {
        return driverService.getAdminDriver(queryDto);
    }

    @Override
    public ApiResponse<?> examineDriver(String driverAdminVo) {
        return driverService.examineDriver(driverAdminVo);
    }

    @Override
    public ApiResponse<?> disableOrRe(String driverAdminVo) {
        return driverService.disableOrRe(driverAdminVo);
    }

    @Override
    public Integer setVehicleNumByDriverCode(Integer carNum, String driverCode) {
        return driverDetailDao.updateCarNumber(carNum, driverCode);
    }

    @Override
    public Integer getNewUser() {
        return userDao.newUserCount(DateUtils.DateToString(new Date()));
    }

    private IPage<UserPO> getPageFromDB(Map<String, Object> params) {
        // 没有命中，查db
        log.info("组装订单分页条件");
        final QueryWrapper<UserPO> condition = new QueryWrapper<>();
        if (!Objects.isNull(params.get("userId"))) {
            condition.eq("user_id", params.get("userId"));
        }
        if (!Objects.isNull(params.get("user_name"))) {
            condition.eq("handle_state", params.get("userName"));
        }
        if (!Objects.isNull(params.get("userType"))) {
            if ("0".equals(params.get("userType").toString())) {
                condition.eq("driver_code", params.get("userType"));
            } else {
                condition.ne("driver_code", 0);
            }
        }
        return this.page(
                new Query<UserPO>().getPage(params),
                condition
        );
    }
}
