package com.logistics.micro.user.provide.entity.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/04/25 2:07
 */
@Data
public class DriverDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 认证编号
	 */
	private String code;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 身份证号
	 */
	private String idNumber;
	/**
	 * 驾证照片
	 */
	private String carPicture;
	/**
	 * 审核状态：0：等待审批，1：审批通过，2：驳回
	 */
	private Integer state;
	/**
	 * 驳回原因
	 */
	private String reason;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 修改时间
	 */
	private Date modifyDate;
	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Integer isDelete;
	/**
	 * 里程数
	 */
	private Double mileage;
	/**
	 * 订单数
	 */
	private Long orderNumber;
	/**
	 * 车辆数量
	 */
	private Integer carNumber;
}
