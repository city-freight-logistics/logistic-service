package com.logistics.micro.user.provide.entity.dto;

import lombok.Data;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/05/28 20:39
 */
@Data
public class UpdatePasswordDto {

	private Integer userId;
	private String password;
	private String newPassword;

}
