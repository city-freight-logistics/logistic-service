package com.logistics.micro.user.provide.entity.pojo;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * 
 * @author hj
 * @date 2021-04-25 02:05:37
 */
@Data
@TableName("CMS_DRIVER_DETAIL")
public class DriverDetailPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 里程数
	 */
	private Double mileage;
	/**
	 * 订单数
	 */
	private Long orderNumber;
	/**
	 * 车辆数量
	 */
	private Integer carNumber;

}
