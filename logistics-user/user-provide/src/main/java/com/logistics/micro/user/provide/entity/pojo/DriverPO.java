package com.logistics.micro.user.provide.entity.pojo;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 车辆信息表
 * 
 * @author hj
 * @date 2021-04-25 02:10:37
 */
@Data
@TableName("CMS_DRIVER")
public class DriverPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(value = "id",type = IdType.AUTO)
	private Integer id;
	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 认证编号
	 */
	private String code;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 身份证号
	 */
	private String idNumber;
	/**
	 * 驾证照片
	 */
	private String carPicture;
	/**
	 * 审核状态：0：等待审批，1：审批通过，2：驳回
	 */
	private Integer state;
	/**
	 * 驳回原因
	 */
	private String reason;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 修改时间
	 */
	private Date modifyDate;
	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Integer isDelete;

}
