package com.logistics.micro.user.provide.entity.pojo;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author hj
 * @date 2021-04-18 14:40:37
 */
@Data
@TableName("UMS_USER")
public class UserPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id
	 */
	@TableId(value = "user_id", type = IdType.AUTO)
	private Integer userId;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 最后修改时间
	 */
	private Date modifyDate;
	/**
	 * 创建人
	 */
	private String createby;
	/**
	 * 头像
	 */
	private String userPicture;
	/**
	 * 密码
	 */
	private String userPassword;
	/**
	 * 昵称
	 */
	private String nickName;
	/**
	 * 生日
	 */
	private String birthday;
	/**
	 * 性别
	 */
	private Integer gender;
	/**
	 * 真实姓名
	 */
	private String name;
	/**
	 * 身份证号
	 */
	private String idNumber;
	/**
	 * 电话
	 */
	private String phone;

	/**
	 * 司机编号
	 */
	private String driverCode;
	/**
	 * 角色
	 */
	private String role;
	/**
	 * 会员积分
	 */
	private String vipScore;
	/**
	 * 常用地址
	 */
	private String userAddress;
	/**
	 * 逻辑删除0，1
	 */
	private Integer isDetele;

}
