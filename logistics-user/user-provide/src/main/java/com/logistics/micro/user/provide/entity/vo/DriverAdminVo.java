package com.logistics.micro.user.provide.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 管理端返回
 * @author Yue Wu
 * @version 1.0
 * @since 2021/5/5 11:42
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DriverAdminVo implements Serializable {
    private Integer id;
    private String driverCode;
    /**
     * 认证编号
     */
    private String code;
    private String name;
    private String idNumber;
    private String carPicture;
    /**
    private String carPicture;
    /**
     * 审核状态：0：等待审批，1：审批通过，2：驳回
     */
    private Integer state;
    private String reason;
    /**
     * 里程数
     */
    private Double mileage;
    /**
     * 订单数
     */
    private Long orderNumber;
    /**
     * 车辆数量
     */
    private Integer carNumber;
}
