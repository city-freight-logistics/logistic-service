package com.logistics.micro.user.provide.entity.vo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.logistics.micro.user.provide.entity.pojo.UserPO;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * description
 *
 * @author ju.huang02@hand-china.com 2021/04/13 16:21
 */
@Data
public class UserRegistVo extends UserPO {

	@NotEmpty(message="手机不能为空")
	@Pattern(regexp = "^1([3-9])[0-9]{9}$",message = "手机号格式不正确")
	private String phone;
	@NotEmpty(message = "密码不能为空")
	@Length(min=6,max=18,message = "密码长度6-18位字符")
	private String password;
	@NotEmpty(message = "验证码不能为空")
	private String code;


}
