package com.logistics.micro.user.provide.service;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.user.provide.entity.pojo.DriverDetailPO;

/**
 * 
 *
 * @author hj
 * @date 2021-04-25 02:05:37
 */
public interface DriverDetailService extends IService<DriverDetailPO> {

    PageUtils queryPage(Map<String, Object> params);
}

