package com.logistics.micro.user.provide.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.user.dto.DriverAdminQueryDto;
import com.logistics.micro.user.provide.entity.dto.DriverDto;
import com.logistics.micro.user.provide.entity.pojo.DriverPO;

/**
 * 车辆信息表
 *
 * @author hj
 * @date 2021-04-22 22:46:06
 */
public interface DriverService extends IService<DriverPO> {

	/**
	 * 分页
	 *
	 * @param params
	 * @return
	 */
	PageUtils queryPage(Map<String, Object> params);

	List<DriverDto> qurryAll();

	ApiResponse<?> getAdminDriver(DriverAdminQueryDto queryDto);

	/**
	 * 司机审核接口
	 *
	 * @param driverAdminVo
	 * @return
	 */
	ApiResponse<?> examineDriver(String driverAdminVo);

	/**
	 * 司机状态禁用解封接口
	 *
	 * @param driverAdminVo
	 * @return
	 */
	ApiResponse<?> disableOrRe(String driverAdminVo);

	/**
	 * 修改认证信息
	 * @param driverDto
	 * @return
	 */
	ApiResponse updateInfo(DriverDto driverDto);

	ApiResponse deleteById(Integer id);
}

