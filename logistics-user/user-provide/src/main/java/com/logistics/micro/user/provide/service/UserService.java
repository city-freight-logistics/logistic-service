package com.logistics.micro.user.provide.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.user.provide.entity.dto.UpdatePasswordDto;
import com.logistics.micro.user.provide.entity.pojo.UserPO;
import com.logistics.micro.user.provide.entity.vo.UserRegistVo;

import java.util.Map;

/**
 * @author hj
 * @date 2021-04-18 14:40:37
 */
public interface UserService extends IService<UserPO> {

	PageUtils queryPage(Map<String, Object> params);

	/**
	 * 注册
	 *
	 * @param vo
	 * @return
	 */
	boolean regist(UserRegistVo vo);

	/**
	 * 判断手机号唯一性
	 *
	 * @param phone
	 * @return
	 */
	boolean checkPhoneUnique(String phone);

	/**
	 * 登录
	 *
	 * @param phone
	 * @param password
	 * @return
	 */
	ApiResponse login(String phone, String password);

	/**
	 * 根据id查询用户
	 * @param id
	 * @return
	 */
	UserPO selectById(String id);


	/**
	 * 根据身份证修改用户司机编号
	 * @param idNumber
	 * @param driverCode
	 * @return
	 */
	boolean updateByIdNubmer(String idNumber, String driverCode);

	/**
	 * 是否使命制
	 * @param id
	 * @return
	 */
	UserPO isNumber(String id);

	/**
	 * 忘记密码进行重置
	 * @param vo
	 * @return
	 */
	boolean forget(UserRegistVo vo);

	/**实名认证
	 *
	 * @param userPO
	 * @return
	 */
	ApiResponse realName(UserPO userPO);

	/**
	 * 更新个人信息
	 * @param userPO
	 * @return
	 */
	ApiResponse saveById(UserPO userPO);

	/**
	 * 修改功能
	 * @param updatePasswordDto
	 * @return
	 */
	ApiResponse updatePassword(UpdatePasswordDto updatePasswordDto);
}

