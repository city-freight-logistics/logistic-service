package com.logistics.micro.user.provide.service.impl;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.user.provide.dao.DriverDetailDao;
import com.logistics.micro.user.provide.entity.pojo.DriverDetailPO;
import com.logistics.micro.user.provide.service.DriverDetailService;
import org.springframework.stereotype.Service;


@Service("driverDetailService")
public class DriverDetailServiceImpl extends ServiceImpl<DriverDetailDao, DriverDetailPO> implements DriverDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DriverDetailPO> page = this.page(
                new Query<DriverDetailPO>().getPage(params),
                new QueryWrapper<DriverDetailPO>()
        );

        return new PageUtils(page);
    }

}