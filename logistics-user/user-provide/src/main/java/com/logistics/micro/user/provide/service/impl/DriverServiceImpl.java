package com.logistics.micro.user.provide.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import com.logistics.micro.user.dto.DriverAdminQueryDto;
import com.logistics.micro.user.provide.dao.DriverDao;
import com.logistics.micro.user.provide.dao.DriverDtoDao;
import com.logistics.micro.user.provide.entity.dto.DriverDto;
import com.logistics.micro.user.provide.entity.pojo.DriverDetailPO;
import com.logistics.micro.user.provide.entity.pojo.DriverPO;
import com.logistics.micro.user.provide.entity.pojo.UserPO;
import com.logistics.micro.user.provide.entity.vo.DriverAdminVo;
import com.logistics.micro.user.provide.service.DriverDetailService;
import com.logistics.micro.user.provide.service.DriverService;
import com.logistics.micro.user.provide.service.UserService;
import com.logistics.micro.user.provide.util.CodeUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


@Service("driverService")
@Slf4j
public class DriverServiceImpl extends ServiceImpl<DriverDao, DriverPO> implements DriverService {

	@Autowired
	private DriverDtoDao driverDtoDao;
	@Autowired
	private DriverService driverService;
	@Autowired
	private DriverDetailService driverDetailService;
	@Autowired
	private UserService userService;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		IPage<DriverPO> page = this.page(
				new Query<DriverPO>().getPage(params),
				new QueryWrapper<DriverPO>()
		);

		return new PageUtils(page);
	}

	@Override
	public List<DriverDto> qurryAll() {
		List<DriverDto> driverDtos = driverDtoDao.qurryAll();
		return driverDtos;
	}

	@Override
	public ApiResponse<?> getAdminDriver(DriverAdminQueryDto queryDto) {
		try {
			if (Objects.isNull(queryDto)) {
				return new ApiResponse<>(HeaderStatus.INPUT_DATA_NULL);
			}
			Map<String, Object> params = GsonUtils.strToMap(GsonUtils.toJSON(queryDto));
//            IPage<HandlePo> page = getPageFromDB(params);
			IPage<DriverAdminVo> query = this.baseMapper.getAdminDriver((Page<?>) new Query<DriverAdminVo>().getPage(params), queryDto);
			final PageUtils pageUtils = new PageUtils(query);
			if (pageUtils.getTotalCount() == 0) {
				return new ApiResponse<>(HeaderStatus.PAGE_CONTEXT_NULL_INFO);
			}
			return new ApiResponse<>(GsonUtils.beanToMap(pageUtils));
		} catch (Exception e) {
			log.info("HandleServiceImpl -> queryHandleList() ret={} exception={}", queryDto.toString(), e.getMessage());
			return new ApiResponse<>(HeaderStatus.EXCPEITON);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ApiResponse<?> examineDriver(String driverAdminVo) {
		try {
			log.info("DriverServiceImpl -> examineDriver start");
			DriverAdminVo adminVo = GsonUtils.fromJson(driverAdminVo, DriverAdminVo.class);
			DriverPO driverPO1 = this.getOne(new QueryWrapper<DriverPO>().eq("id_number", adminVo.getIdNumber()));
			log.info("DriverServiceImpl -> examineDriver ret: reqState={}, dbState={}", adminVo.getState(), driverPO1.getState() == 0);
			if (driverPO1.getState().equals(adminVo.getState())) {
				log.info("DriverServiceImpl -> examineDriver SUCCESS_IDEMPOTENT");
				return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
			}
			if (adminVo.getState() == 1 && driverPO1.getState() == 0) {
				log.info("DriverServiceImpl -> examineDriver pass examineDriver");
				final DriverPO driverPO = GsonUtils.fromJson(driverAdminVo, DriverPO.class);
				driverPO.setId(driverPO1.getId());
				String idNumber = driverPO.getIdNumber();
				DriverDetailPO driverDetailPO = new DriverDetailPO();
				Integer state = driverPO.getState();
				String driverCode = "";
				DriverPO check = driverService.getById(driverPO1.getId());
				if (check.getState() == 0) {
					if (state == 1) {
						driverCode = CodeUtil.driverCode();
						driverPO.setDriverCode(driverCode);
						driverDetailPO.setDriverCode(driverCode);
					}
					final boolean update = driverService.updateById(driverPO);
					if (update) {
						driverDetailPO.setDriverCode(driverCode);
						driverDetailService.save(driverDetailPO);
						userService.updateByIdNubmer(idNumber, driverCode);
						return new ApiResponse<>("更新成功");
					}
					return new ApiResponse<>(HeaderStatus.ERROR, "更新失败");
				} else {
					return new ApiResponse<>(HeaderStatus.SUCCESS, "已经更新");
				}
			}
			log.info("DriverServiceImpl -> examineDriver reject examineDriver");
			boolean update = this.update(new UpdateWrapper<DriverPO>()
					.set("state", adminVo.getState())
					.set("reason", adminVo.getReason())
					.eq("state", 0)
					.eq("id_number", adminVo.getIdNumber()));
			return update ? new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT) : new ApiResponse<>(HeaderStatus.QUERY_UPDATE_STATUS_EXCEPTION);
		} catch (Exception e) {
			log.error("DriverServiceImpl -> ret={} exception={}", driverAdminVo, e.getMessage());
			return new ApiResponse<>(HeaderStatus.ERROR);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ApiResponse<?> disableOrRe(String driverAdminVo) {
		DriverAdminVo adminVo = GsonUtils.fromJson(driverAdminVo, DriverAdminVo.class);
		DriverPO driverPO = this.getOne(new QueryWrapper<DriverPO>().eq("id_number", adminVo.getIdNumber()));
		if (driverPO.getState().equals(adminVo.getState())) {
			return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
		}
		this.update(new UpdateWrapper<DriverPO>()
				.set("state", adminVo.getState())
				.eq("id_number", adminVo.getIdNumber()));
		return new ApiResponse<>(HeaderStatus.SUCCESS_IDEMPOTENT);
	}

	@Override
	public ApiResponse updateInfo(DriverDto driverDto) {
		UserPO userPO = userService.isNumber(driverDto.getUserId());
		DriverPO driver = driverService.getOne(new QueryWrapper<DriverPO>().eq("id_number", driverDto.getIdNumber()));
		if (ObjectUtils.isEmpty(driver) || ObjectUtils.isEmpty(userPO)) {
			return new ApiResponse(HeaderStatus.ERROR,"认证信息或者用户不存在");
		}
		if (StringUtils.isEmpty(userPO.getIdNumber())) {
			return new ApiResponse<>(HeaderStatus.ERROR,"未实名制请先实名制");
		}
		if (driverDto.getIdNumber().equals(userPO.getIdNumber()) && driver.getState() == 2) {
			boolean update = this.update(new UpdateWrapper<DriverPO>().set("name", driverDto.getName())
					.set("id_number", driverDto.getIdNumber()).set("car_picture", driverDto.getCarPicture()).set("state", 0).eq("code", driverDto.getCode()));
			if (update) {
				return new ApiResponse<>("重新提交成功，等待管理员审批");
			}
		} else {
			return new ApiResponse<>(HeaderStatus.ERROR,"身份证和账号实名信息不一致");
		}
		return new ApiResponse<>(HeaderStatus.ERROR, "提交失败");
	}

	@Override
	public ApiResponse deleteById(Integer id) {
		DriverPO driver = this.getById(id);
		if (driver.getState() == 1) {
			this.removeById(id);
			UserPO user = userService.getOne(new QueryWrapper<UserPO>().eq("id_number", driver.getIdNumber()));
			user.setRole("customer");
			userService.updateById(user);
			return new ApiResponse(HeaderStatus.SUCCESS, "取消认证成功");
		}
		return new ApiResponse(HeaderStatus.ERROR, "取消认证失败");
	}

}