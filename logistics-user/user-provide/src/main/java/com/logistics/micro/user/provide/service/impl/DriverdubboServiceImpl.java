package com.logistics.micro.user.provide.service.impl;

import java.util.List;
import java.util.Map;

import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.GsonUtils;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.user.provide.entity.dto.DriverDto;
import com.logistics.micro.user.provide.entity.pojo.DriverDetailPO;
import com.logistics.micro.user.provide.entity.pojo.DriverPO;
import com.logistics.micro.user.provide.service.DriverDetailService;
import com.logistics.micro.user.provide.service.DriverService;
import com.logistics.micro.user.provide.service.UserService;
import com.logistics.micro.user.provide.util.CodeUtil;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * description
 *
 * @author ju.huang02@hand-china.com 2021/04/22 23:06
 */
@org.springframework.stereotype.Service
public class DriverdubboServiceImpl {

	@Autowired
	private DriverService driverService;
	@Autowired
	private DriverDetailService driverDetailService;
	@Autowired
	private UserService userService;

	/***
	 * @Parms 分页查询认证信息
	 * @return
	 * @Description: 分页查询认证信息
	 */
	public Map<String, Object> queryPage(Map<String, Object> params) {
		List<DriverDto> driverDtos = driverService.qurryAll();
		PageUtils pageUtils = new PageUtils();
		pageUtils.setList(driverDtos);
		final Map<String, Object> map = GsonUtils.beanToMap(pageUtils);
		return map;
	}


	/**
	 * 修改信息
	 *
	 * @param driver
	 * @return
	 */
	public ApiResponse<String> update(String driver) {
		final DriverPO driverPO = GsonUtils.fromJson(driver, DriverPO.class);
		String idNumber = driverPO.getIdNumber();
		DriverDetailPO driverDetailPO = new DriverDetailPO();
		Integer state = driverPO.getState();
		String driverCode = "";
		DriverPO check = driverService.getById(driverPO.getId());
		if (check.getState() == 0) {
			if (state == 1) {
				driverCode = CodeUtil.driverCode();
				driverPO.setDriverCode(driverCode);
				driverDetailPO.setDriverCode(driverCode);
			}
			final boolean update = driverService.updateById(driverPO);
			if (update) {
				driverDetailPO.setDriverCode(driverCode);
				driverDetailService.save(driverDetailPO);
				userService.updateByIdNubmer(idNumber, driverCode);
				return new ApiResponse<>("更新成功");
			}
			return new ApiResponse<>(HeaderStatus.ERROR, "更新失败");
		} else {
			return new ApiResponse<>(HeaderStatus.SUCCESS, "已经更新");
		}
	}

}
