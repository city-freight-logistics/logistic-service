package com.logistics.micro.user.provide.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.logistics.micro.common.constant.AuthServerConstant;
import com.logistics.micro.common.enums.HeaderStatus;
import com.logistics.micro.common.model.ApiResponse;
import com.logistics.micro.common.utils.JwtUtils;
import com.logistics.micro.user.provide.dao.UserDao;
import com.logistics.micro.user.provide.entity.dto.UpdatePasswordDto;
import com.logistics.micro.user.provide.entity.pojo.UserPO;
import com.logistics.micro.user.provide.entity.vo.UserRegistVo;
import com.logistics.micro.user.provide.service.UserService;
import com.logistics.micro.user.provide.util.Md5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.common.utils.PageUtils;
import com.logistics.micro.common.utils.Query;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserPO> implements UserService {

	@Autowired
	UserDao userDao;
	@Autowired
	StringRedisTemplate redisTemplate;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		IPage<UserPO> page = this.page(
				new Query<UserPO>().getPage(params),
				new QueryWrapper<UserPO>()
		);

		return new PageUtils(page);
	}

	@Override
	public boolean regist(UserRegistVo vo) {
		//查看手机号唯一性
		boolean check = checkPhoneUnique(vo.getPhone());
		if (check) {
			String password = vo.getPassword();
			String md5 = Md5Util.md5(password);
			UserPO user = new UserPO();
			user.setNickName(vo.getPhone());
			user.setUserPassword(md5);
			user.setPhone(vo.getPhone());
			int insert = userDao.insert(user);
			return insert == 1;
		} else {
			return false;
		}

	}

	/**
	 * 判断手机唯一性
	 *
	 * @param phone
	 * @return
	 */
	@Override
	public boolean checkPhoneUnique(String phone) {
		Integer line = userDao.selectByPhone(phone);
		if (line >= 1) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public ApiResponse login(String phone, String password) {
		UserPO login = userDao.login(phone);
		if (!ObjectUtils.isEmpty(login)) {
			String userPassword = login.getUserPassword();
			//查看密码是否正确
			Boolean check = Md5Util.md5(password, userPassword);
			if (check) {
				//登录成功
				String jwtToken = JwtUtils.getJwtToken(login.getUserId());
				redisTemplate.opsForValue().set(AuthServerConstant.JWT_TOKEN + jwtToken, jwtToken, 365, TimeUnit.DAYS);
				return new ApiResponse(HeaderStatus.SUCCESS, jwtToken);
			} else {
				//密码错误
				return new ApiResponse(HeaderStatus.PASSWORD_ERROR, "密码错误请重新输入");
			}
		} else {
			//用户不存在
			return new ApiResponse(HeaderStatus.NO_USER_PHONE, "手机号不存在");
		}
	}

	@Override
	public UserPO selectById(String id) {
		return userDao.selectById(id);
	}


	@Override
	public boolean updateByIdNubmer(String idNumber, String driverCode) {
		Integer line = userDao.updateByIdNubmer(idNumber, driverCode);
		return line == 1;
	}

	@Override
	public UserPO isNumber(String id) {
		return userDao.selectById(id);
	}

	@Override
	public boolean forget(UserRegistVo vo) {
		String password = vo.getPassword();
		String md5 = Md5Util.md5(password);
		UserPO user = new UserPO();
		user.setUserPassword(md5);
		int update = userDao.update(user, new UpdateWrapper<UserPO>() {{
			eq("phone", vo.getPhone());
		}});
		return update == 1;
	}

	@Override
	public ApiResponse realName(UserPO userPO) {
		UserPO user = this.selectById(String.valueOf(userPO.getUserId()));
		if (StringUtils.isEmpty(user.getIdNumber())) {
			boolean update = this.updateById(userPO);
			if (update) {
				return new ApiResponse(HeaderStatus.SUCCESS, "认证成功");
			} else {
				return new ApiResponse(HeaderStatus.ERROR, "认证失败，请稍后认证");
			}
		} else {
			return new ApiResponse(HeaderStatus.ERROR, "已经实名认证，不能再认证");
		}
	}

	@Override
	public ApiResponse saveById(UserPO userPO) {
		UserPO user = this.selectById(String.valueOf(userPO.getUserId()));
		if (!ObjectUtils.isEmpty(user)) {
			boolean update = this.updateById(userPO);
			if (update) {
				return new ApiResponse(HeaderStatus.SUCCESS, "修改成功");
			} else {
				return new ApiResponse(HeaderStatus.ERROR, "修改失败");
			}
		} else {
			return new ApiResponse(HeaderStatus.ERROR, "用户不存在");
		}
	}

	@Override
	public ApiResponse updatePassword(UpdatePasswordDto updatePasswordDto) {
		UserPO userPO = this.selectById(String.valueOf(updatePasswordDto.getUserId()));
		Boolean check = Md5Util.md5(updatePasswordDto.getPassword(), userPO.getUserPassword());
		if(check){
			boolean update = this.update(new UpdateWrapper<UserPO>().set("user_password", updatePasswordDto.getNewPassword())
					.eq("user_id", updatePasswordDto.getUserId()));
			if (update){
				return new ApiResponse(HeaderStatus.SUCCESS,"修改成功");
			}
			return new ApiResponse(HeaderStatus.ERROR,"修改失败");
		}
		return new ApiResponse(HeaderStatus.ERROR,"原密码错误");
	}

}