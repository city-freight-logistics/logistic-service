package com.logistics.micro.user.provide.util;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * description
 *
 * @author ju.huang02@hand-chian.com 2021/04/25 23:37
 */
public class CodeUtil {

	private static Integer driverNum = 0;
	private static Integer num = 0;
	private static String dateFirst = null;
	private static Integer endCode = 10000;
	private static Integer numEnd = 1000001;

	public static String code() {
		num += 1;
		String codeHeader = "DA";
		SimpleDateFormat time = new SimpleDateFormat("yyyyMMdd");
		if (num == 1) {
			dateFirst = time.format(new Date());
		}
		Date date = new Date();
		String dateTime = time.format(date);
		if (dateTime.compareTo(dateFirst) > 0) {
			num = 0;
			endCode = 10000;
		}
		String codeEnd = String.valueOf(endCode).replaceAll("^(1+)", "");
		String codeMiddle = time.format(date);
		String code = codeHeader + codeMiddle + codeEnd;
		endCode += 1;
		return code;
	}

	public static String driverCode() {
		driverNum += 1;
		String codeHeader = "DN";
		SimpleDateFormat time = new SimpleDateFormat("yyyyMMdd");
		if (driverNum == 1) {
			dateFirst = time.format(new Date());
		}
		Date date = new Date();
		String dateTime = time.format(date);
		if (dateTime.compareTo(dateFirst) > 0) {
			driverNum = 0;
			numEnd = 1000001;
		}
		String codeEnd = String.valueOf(numEnd).replaceAll("^(1+)", "");
		String driverCode = codeHeader + dateTime + codeEnd;
		numEnd += 1;
		return driverCode;
	}

}
