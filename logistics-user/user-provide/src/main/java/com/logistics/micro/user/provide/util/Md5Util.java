package com.logistics.micro.user.provide.util;

/**
 * description
 *
 * @author ju.huang02@hand-china.com 2021/04/15 15:51
 */

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by Miracle Luna on 2019/11/18
 */
public class Md5Util {

	static BCryptPasswordEncoder passwordEncoder=  new BCryptPasswordEncoder();
	/**
	 * 将数据进行 MD5 加密，并以16进制字符串格式输出
	 *
	 * @param data
	 * @return
	 */
	public static String md5(String data) {

		String encode=  passwordEncoder.encode(data);
		return encode;
	}

	/**
	 * 对比密码是否正确
	 * @param password
	 * @param data
	 * @return
	 */
	public static Boolean md5(String password,String data){

		boolean matches = passwordEncoder.matches(password, data);
		return matches;
	}

}